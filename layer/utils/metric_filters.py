from django.core import exceptions as django_exceptions

_filter_map = {}
''' keeps filter functions (should be defined in this module). Is filled at the bottom
'''

def _fill_filter_map():
    """ all functions in this module which names are of form 'filter_*' are kept as metric filters """
    for name, func in globals().items():
        if name.startswith('filter_'):
            _filter_map[name[7:]] = func

def get_filter(name, no_exp=False):
    if name in _filter_map:
        return _filter_map[name]
    elif no_exp:
        return None
    raise django_exceptions.ValidationError("Not found metric filter '%s'" % name)

def get_filter_choices():
    flt_list = [('','-no filter-')]
    flt_list.extend( (f,f) for f in _filter_map.keys() )
    return flt_list
# ---------------------------------------------------------------

def multiply(pairs, multiply_by, round_digits=0):
    for pair in pairs:
        pair[1] = round(pair[1] * multiply_by, round_digits)
    return pairs

def divide(pairs, divide_by, round_digits=0):
    for pair in pairs:
        pair[1] = round(pair[1] / divide_by, round_digits)
    return pairs

# each filter function should accept the round_digits arg (=0 by default)
def filter_kbytes(pairs, round_digits=0):
    return divide(pairs, 1024, round_digits)

def filter_mbytes(pairs, round_digits=0):
    return divide(pairs, 1024*1024, round_digits)

def filter_kilo(pairs, round_digits=0):
    return divide(pairs, 1000, round_digits)

def filter_mega(pairs, round_digits=0):
    return divide(pairs, 1000*1000, round_digits)

def filter_shrink(pairs, round_digits=0, stride=10):
    return pairs[::stride]

# ---------------------------------------------------------------
_fill_filter_map()
