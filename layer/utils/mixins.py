import json
import datetime

from django import http
from . import metric_filters


class JSONResponseMixin(object):
    def render_to_response(self, context):
        """Returns a JSON response containing 'context' as payload"""
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        """Construct an `HttpResponse` object."""
        return http.HttpResponse(content,
                                 content_type='application/json',
                                 **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object"""
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else obj
        return json.dumps(context, default=dthandler)


class GraphDataResponseMixin(JSONResponseMixin):
    """ Transforms data in format [metric_id, [[timestamp,value], ...]] to JSON response
        Applies metric filters if requested in URL parameters
        Adds comments to HTTP headers
    """
    def render_to_response(self, data):
        pairs = []
        for pair in data[1]: # transform to list of lists - QuarySet.values_list() returns tuples
            pairs.append([ pair[0].isoformat(), pair[1] ])

        kw_round_digits = {}
        for rd in self.request.GET.getlist('round_digits'):
            try:
                kw_round_digits['round_digits'] = int(rd)
            except:
                pass
        filters = self.request.GET.getlist('filter') # URL with "filter=shrink&filter=kbytes" leads to QueryDict with {'filter' : ['shrink','kbytes'] }
        for flt_name in filters:
            filter_func = metric_filters.get_filter(flt_name)
            pairs = filter_func(pairs, **kw_round_digits)

        for param in self.request.GET.getlist('multiply_by'):
            try:
                multiply_by = float(param)
            except:
                continue
            pairs = metric_filters.multiply(pairs, multiply_by, **kw_round_digits)

        content = json.dumps([data[0], pairs])
        res = self.get_json_response(content)
        for h in ['server', 'metric', 'run']:
            key = 'target_'+ h
            if key in self.context:
                res['X-measurement-'+ h] = self.context[key]
        return res

