from __future__ import absolute_import

from celery import Celery

from django.conf import settings


celery_app = Celery('layer')

celery_app.config_from_object(settings)
celery_app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)