/**
 * Created bynikolaygolub on 28/12/13.
 */


// Constants
var timestamp_key = "timestamp";

function build_line_timestamp_amchart(settings, data, place) {
    // Initiate chart and data provider
    var chart = new AmCharts.AmSerialChart();
    chart.dataProvider = data;
    chart.pathToImages = "/static/js/amcharts/images/";
    chart.categoryField = timestamp_key;

    // base options
    chart.color = "#373737";
    chart.addTitle(settings.title,19,'#373737');
    chart.autoMarginOffset = 5;
    chart.marginTop = -5;
    chart.fontFamily = "Tahoma";
    chart.fontSize = 13;

    // CURSOR
    var chartCursor = new AmCharts.ChartCursor();
    chartCursor.cursorPosition = "mouse";
    chartCursor.categoryBalloonDateFormat = "DD MMM HH:NN:SS";
    chart.balloon.enabled = true;
    chartCursor.categoryBalloonColor = "#F8981D";
    chartCursor.categoryBalloonAlpha = 0.6;
    chartCursor.color = "#000000";
    chart.addChartCursor(chartCursor);
    
    // BALLOON
    var balloon = chart.balloon;
    balloon.animationDuration = 0.14;
    balloon.borderThickness = 1;
    balloon.cornerRadius = 5;
    balloon.fontSize = 10;
    balloon.pointerWidth = 8;
    balloon.shadowAlpha = 0.25;

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.parseDates = true;
    categoryAxis.fontSize = 13;
    categoryAxis.titleFontSize = 15;
    categoryAxis.minPeriod = "fff";
    categoryAxis.title = settings.x_label;


    // value
    var valueAxis = new AmCharts.ValueAxis();
    valueAxis.dashLength = 1;
    valueAxis.fontSize = 13;
    valueAxis.titleFontSize = 17;
    valueAxis.title = settings.y_label;
    valueAxis.minimum = 0;
    if (settings.maximum != null) {
        valueAxis.maximum = settings.maximum;
    }
    chart.addValueAxis(valueAxis);

    var graphs = settings.graphs;

    for (var i = 0; i < graphs.length; i++) {
        var graph = new AmCharts.AmGraph();
        graph.title = graphs[i].title;
        graph.id = graphs[i].title;
        graph.valueField = graphs[i].match_key;
        graph.lineThickness = 1;
        graph.markerType = "circle";
        //graph.connect = false;
        //graph.lineColor = graphs[i].color;
        chart.addGraph(graph);
    }

    // SCROLLBAR
    var chartScrollbar = new AmCharts.ChartScrollbar();
    chart.addChartScrollbar(chartScrollbar);
    chartScrollbar.graph = graph.id;
    chartScrollbar.graphType = "line";
    chartScrollbar.offset = 1;
    chartScrollbar.scrollbarHeight = 30;
    chartScrollbar.maximum = settings.maximum;
    chartScrollbar.minimum = 0;


    var legend = new AmCharts.AmLegend();
    chart.addLegend(legend);
    legend.valueText = "";
    legend.autoMargins = false;
    legend.marginLeft = 60;
    legend.marginBottom = 5;
    legend.verticalGap = 0;
    legend.spacing = 50;
    legend.fontSize = 13;
    // Add download button
    chart.exportConfig = {menuItems: [{
        textAlign: 'center',
        onclick: function () {},
        icon: '/static/img/export.png',
        iconTitle: 'Save chart as an image',
        items: [{
            title: 'JPG',
            format: 'jpg'
        }, {
            title: 'PNG',
            format: 'png'
        }, {
            title: 'SVG',
            format: 'svg'
        }]
    }]};
    // WRITE
    chart.write(place);
}

function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}


function build_timestamp_data_provider(data_dictionary) {
    /*
     Creates data provider for amcharts from standard list of data.
     Converts list like this:
     [
     data_key,
     [["2013-12-28T21:00:00+00:00", 41.5858],
     ["2013-12-28T20:59:00+00:00", 41.5901],
     ["2013-12-28T20:58:00+00:00", 41.6476]]
     ]
     into something like this:
     [
     {data_key: 41.6476, timestamp: 2013-12-28T20:58:00+00:00 },
     {data_key: 41.5901, timestamp: 2013-12-28T21:59:00+00:00 },
     {data_key: 41.5858, timestamp: 2013-12-28T21:00:00+00:00 },
     ]
     where timestamp becomes js timestamp object and data_key is usually the id from settings
     for particular line on graph

     */
    var dataProvider = [];
    for (var data_key in data_dictionary) {
        for (var i = 0; i < data_dictionary[data_key].length; i++) {
            var dataElement = {};
            dataElement[timestamp_key] = new Date(data_dictionary[data_key][i][0]);
            dataElement[data_key] = data_dictionary[data_key][i][1];
            dataProvider.push(dataElement)
        }
    }
    return sortByKey(dataProvider, timestamp_key);
}


function initiate_amchart(graph_body_div) {
    var a_chart = $(graph_body_div).find('ul.moduleList li.selected a');
    console.log(a_chart);
    var chart_placeholder = $(graph_body_div).find('div.graphImg.cropable div.chart-placeholder')[0];
    var spinner = new Spinner(standard_spinner_opts).spin(chart_placeholder);
    var chart_type = a_chart.attr('type');
    var settings_url = a_chart.attr('settings');
    // get dictionary with url to specific charts
    var data_dict = JSON.parse(a_chart.attr('data'));

    // get data from server-side
    var deferreds = [];

    var data_dictionary = {};
    var settings = {};
    for (var key in data_dict) {
        var data_url = data_dict[key];
        deferreds.push(
            $.getJSON(data_url, function (data) {
                /* this stuff should return list with 2 elements:
                 first is metric_id, which is match key in settings
                 and second is list of tuples ('timestamp', 'value')
                 */
                data_dictionary[data[0]] = data[1];
            })
        );

    }
    deferreds.push(
        $.getJSON(settings_url, function (data) {
            // data is json formatted settings
            settings = data;
        })
    );

    // When all functions are complete we can start build chart
    $.when.apply($, deferreds).done(function () {
        var dataProvider = build_timestamp_data_provider(data_dictionary);
        build_line_timestamp_amchart(settings, dataProvider, chart_placeholder);
    })

}


function initiate_amcharts() {
    $('div.graph div.graph-body').each(
        function () {
            initiate_amchart(this);
        });
}

$(document).ready(function () {
    initiate_amcharts();
});

$(document).ready(function () {
    $("div.graph div.graph-body ul.moduleList li").click(function (event) {
        $(this).addClass('selected').siblings().removeClass('selected');
        var graph_body_div = $(this).parent().parent().first();
        initiate_amchart(graph_body_div);
        event.preventDefault();
    });
});