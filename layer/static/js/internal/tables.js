/**
 * Created with PyCharm.
 * User: nikolay.golub
 * Date: 9/23/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */


function get_table_content(table_url, place) {
    // set spinner
    var spinner = new Spinner(standard_spinner_opts).spin(place);
    $.ajax({url: table_url,
        datatype: 'text/html',
        success: function (table_data) {
            write_table(place, table_data, table_url);
        }
    });
}

function write_table(place, table, new_url) {
    // write table data
    $(place).html(table);
    // replace old urls with actual urls
    $(place).find('table thead tr th a, table thead tr td a').each(
            function () {
                old_url_string = this.href;
                old_url_query = old_url_string.substring(old_url_string.indexOf("?"));
                updated_href = new_url.split("?")[0] + old_url_query;
                $(this).prop('href', updated_href);
            });
    // bind action for table urls clicking
    $(place).find('table thead tr th a, table thead tr td a, ul li a').on('click', function (e) {
        e.preventDefault();
        get_table_content(this.href, place);
    });
}

$(document).ready(function () {
    $('div.content div.testResults div.contentTable').each(
            function () {
                var table_url = $(this).find('a').attr('href');
                var table_div = this;
                get_table_content(table_url, table_div);
            });
});
