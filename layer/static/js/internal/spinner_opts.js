/**
 * Created by ngolub on 1/9/14.
 */
var standard_spinner_opts = {
    // example can be viewed here http://fgnass.github.io/spin.js/
  lines: 9, // The number of lines to draw
  length: 7, // The length of each line
  width: 3, // The line thickness
  radius: 6, // The radius of the inner circle
  corners: 0, // Corner roundness (0..1)
  rotate: 13, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1.1, // Rounds per second
  trail: 50, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: true, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: 'auto', // Top position relative to parent in px
  left: 'auto' // Left position relative to parent in px
};