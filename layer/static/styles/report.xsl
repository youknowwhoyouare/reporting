<?xml version="1.0" encoding="UTF-8"?>
<!--
    Document   : report.xsl
    Created on : 2 Апрель 2010 г., 20:35
    Author     : tarashevsky
-->

<xsl:stylesheet
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
>

<xsl:output method="html" encoding="utf-8" indent="no"
                omit-xml-declaration="yes" media-type="text/html"
                cdata-section-elements="script style"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
/>

    <!-- Keys for grouping -->
    <xsl:key name="key-role" match="role" use="@name" />

    <!-- Variables -->
    <xsl:variable name="project">
        <xsl:value-of select="//meta[@name='project']/@content" />
    </xsl:variable>

    <xsl:variable name="appType">
        <xsl:value-of select="//meta[@name='appType']/@content" />
    </xsl:variable>

    <xsl:variable name="testName">
        <xsl:value-of select="//meta[@name='testName']/@content" />
    </xsl:variable>

    <xsl:variable name="testId">
        <xsl:value-of select="//meta[@name='id']/@content" />
    </xsl:variable>

    <xsl:variable name="branch">
        <xsl:value-of select="//meta[@name='branch']/@content" />
    </xsl:variable>

    <xsl:variable name="release">
        <xsl:value-of select="//meta[@name='release']/@content" />
    </xsl:variable>

    <xsl:variable name="sow">
        <xsl:value-of select="//meta[@name='sow']/@content" />
    </xsl:variable>

    <xsl:variable name="sowLink">
        <xsl:value-of select="//meta[@name='sowLink']/@content" />
    </xsl:variable>

    <xsl:variable name="tagslist">
        <xsl:value-of select="//meta[@name='tags']/@content" />
    </xsl:variable>

    <xsl:variable name="resolution">
        <xsl:call-template name="lowercase">
            <xsl:with-param name="string" select="//meta[@name='resolution']/@content" />
        </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="testDate">
        <xsl:value-of select="//meta[@name='testDate']/@content" />
    </xsl:variable>

    <xsl:variable name="testTime">
        <xsl:value-of select="//meta[@name='testTime']/@content" />
    </xsl:variable>

    <xsl:variable name="idLink">
        <xsl:value-of select="//meta[@name='idLink']/@content" />
    </xsl:variable>

    <!-- Templates -->
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=8" />
            <title>
                <xsl:value-of select="$testId" />
                <xsl:text> TITLE</xsl:text>
            </title>
            <xsl:copy-of select="//metas/meta" />

            <link rel="shortcut icon" href="{$siteUrl}favicon.ico" />
            <link rel="stylesheet" type="text/css" href="{$siteUrl}styles/main.css" media="all" ></link>
            <link rel="stylesheet" type="text/css" href="{$siteUrl}styles/thickbox.css" media="all" ></link>
            <script type="text/javascript" src="{$siteUrl}js/jquery-1.4.min.js"></script>
            <script type="text/javascript" src="{$siteUrl}js/thickbox.js"></script>
            <script type="text/javascript" src="{$siteUrl}js/script.js"></script>
            <script type="text/javascript" src="{$siteUrl}js/swfobject.js"></script>
            <script type="text/javascript" src="{$siteUrl}js/amchart/amcharts.js"></script>
            <script type="text/javascript" src="{$siteUrl}js/amchart/amfallback.js"></script>
        </head>
        <body class="noJS" style="">
            <xsl:choose>
                <xsl:when test="contains(',passed,completed,failed,', concat(',', $resolution, ','))" >
                    <div class="wrap test {$resolution}">
                        <xsl:call-template name="header" />
                        <xsl:call-template name="content" />
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <div class="wrap test undefined">
                        <xsl:call-template name="header" />
                        <xsl:call-template name="content" />
                    </div>
                </xsl:otherwise>
            </xsl:choose>
          
           <xsl:call-template name="footer"/>
        </body>
        </html>
    </xsl:template>

    <xsl:template name="header">
        <div class="header">
            <div class="ticketInfo">
                <xsl:call-template name="ticketinfo"/>
            </div>
            <h1><xsl:value-of select="$testName" /> Report</h1>
            <xsl:call-template name="breadcrumbs"/>
            <div class="relativeArticles">
                <a class="seeAlsoLink" href="javascript:void(0);">See Also</a>
                <iframe style="display:none;" frameborder="0" src="{$siteUrl}report/shortreport/{$project}/{$testId}">
                </iframe>
            </div>
        </div>
       
    </xsl:template>

    <xsl:template name="breadcrumbs">
        <div class="breadcrumbs">
            <a href="{$siteUrl}">Main Field</a>
            <xsl:text> › </xsl:text>
            <a href="{$siteUrl}{$project}/">
                <xsl:value-of select="$project" />
            </a>
            <xsl:text> › </xsl:text>
            <a href="{$siteUrl}{$project}/{$appType}/">
                <xsl:value-of select="$appType" />
            </a>
            <xsl:text> › </xsl:text>
            <strong>
                <xsl:value-of select="$testName" />
            </strong>
	</div>
    </xsl:template>

    <xsl:template name="ticketinfo">
        <p>
            <span class="tag">
                <xsl:value-of select="$branch" />
            </span>
            <span class="code">
                <xsl:choose>
                    <xsl:when test="$idLink!=''">
                        <a href="{$idLink}">
                            <xsl:value-of select="$testId" />
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                         <xsl:value-of select="$testId" />
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </p>
        <p>
            <span class="branch">
                <xsl:value-of select="$release" />
            </span>
        <xsl:if test="$sow!=''">
            <span class="sow">
                <xsl:text>SOW: </xsl:text>
                <xsl:choose>
                    <xsl:when test="$sowLink!=''">
                        <a href="{$sowLink}">
                            <xsl:value-of select="$sow" />
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                         <xsl:value-of select="$sow" />
                    </xsl:otherwise>
                </xsl:choose>
            </span>
        </xsl:if>
        </p>
    </xsl:template>


    <xsl:template name="content">
        <div class="content">
            <xsl:call-template name="aside" />
            <xsl:call-template name="testContent" />
            <xsl:call-template name="testResults" />
        </div>
    </xsl:template>

    <xsl:template name="aside">
        <div class="aside">
            <xsl:call-template name="tags" />
            <xsl:call-template name="resolution" />
            <xsl:call-template name="toc" />
            <xsl:call-template name="quicklinks" />
            <xsl:call-template name="participants" />
            <!--<xsl:call-template name="actions" />-->
        </div>
    </xsl:template>

    <xsl:template name="tags">
    <div class="tags">
        <h2>Tags:</h2>
        <ul>
            <xsl:call-template name="split-string">
                <xsl:with-param name="list"><xsl:value-of select="$tagslist"/></xsl:with-param>
                <xsl:with-param name="delimiter">,</xsl:with-param>
                <xsl:with-param name="action">tagoutput</xsl:with-param>
            </xsl:call-template>
        </ul>
    </div>
    </xsl:template>

    <xsl:template name="resolution">
        <div class="resolution">
            <h2>
                <xsl:text>Test status: </xsl:text>
                    <strong>
                        <xsl:call-template name="first-upper">
                            <xsl:with-param name="string"><xsl:value-of select="$resolution"/></xsl:with-param>
                        </xsl:call-template>
                     </strong>
            </h2>
            <div class="date">
                <p>
                    <xsl:text>Test date: </xsl:text>
                    <xsl:value-of select="$testDate" />
                </p>
                <xsl:if test="$testTime">
                    <p>
                        <xsl:text>Test time: </xsl:text>
                        <xsl:value-of select="$testTime" />
                    </p>
                </xsl:if>
            </div>
            <xsl:apply-templates select="//additional_info/*" />
        </div>
    </xsl:template>

    <xsl:template name="toc">
        <xsl:if test="(//widget[@link]) and (//report_body[@order!=''])">

        <div class="toc">
            <h2>Table of contents</h2>
            <ul>
            <xsl:for-each select="//report_body[@order!='']">
                <xsl:variable name="order">
                    <xsl:value-of select="concat(',',@order,',')" />
                </xsl:variable>
                <xsl:call-template name="split-string">
                    <xsl:with-param name="list"><xsl:value-of select="concat(',',@order,',')" /></xsl:with-param>
                    <xsl:with-param name="delimiter">,</xsl:with-param>
                    <xsl:with-param name="action">toclink</xsl:with-param>
                </xsl:call-template>
                <!--<xsl:for-each select="widget[@link]">
                <xsl:if test="contains($order, concat(',', @id, ','))" >
                    <li>
                        <a>
                            <xsl:attribute name="href">#<xsl:value-of select="translate(translate(@link, ' ', '_'), '/.', '')" /></xsl:attribute>
                            <xsl:value-of select="@link" />
                        </a>
                    </li>
                </xsl:if>
                </xsl:for-each>-->

            </xsl:for-each>
            </ul>
        </div>
        </xsl:if>

    </xsl:template>

    <xsl:template name="quicklinks">
    <xsl:if test="//links/link">
        <div class="toc">
            <xsl:choose>
                <xsl:when test="//links/@title">
                    <h2><xsl:value-of select="//links/@title" /></h2>
                </xsl:when>
                <xsl:otherwise>
                    <h2>Quick Links</h2>
                </xsl:otherwise>
            </xsl:choose>
            <ul>
                <xsl:for-each select="//links/link">
                    <li>
                        <a>
                            <xsl:attribute name="href">
                                <xsl:choose>
                                    <xsl:when test="@target">
                                        <xsl:if test="starts-with(@target, '/')">
                                            <xsl:text>../../</xsl:text>
                                            <xsl:value-of select="$selfDir" />
                                        </xsl:if>
                                        <xsl:if test="not(starts-with(@target, 'http'))">
                                            <xsl:text>/</xsl:text>
                                        </xsl:if>
                                        <xsl:value-of select="@target" />
                                    </xsl:when>
                                    <xsl:otherwise />
                                </xsl:choose>
                            </xsl:attribute>
                            <xsl:attribute name="target">
                                <xsl:if test="starts-with(@target, 'http')">
                                    <xsl:text>_blank</xsl:text>
                                </xsl:if>
                            </xsl:attribute>
                            <xsl:value-of select="." />
                        </a>
                    </li>
                 </xsl:for-each>
            </ul>
         </div>
    </xsl:if>
    </xsl:template>

    <xsl:template name="participants">
        <xsl:variable name="role-names" select="//roles
                                                /role[generate-id(.)
                                                    = generate-id(key('key-role', @name))]
                                                /@name"
        />
        
        <div class="participants">
            <h2>Test Participants</h2>
            <xsl:for-each select="$role-names">
                <dl>
                    <dt>
                        <xsl:call-template name="first-upper">
                            <xsl:with-param name="string"><xsl:value-of select="." /></xsl:with-param>
                        </xsl:call-template>
                    </dt>
                    <xsl:for-each select="key('key-role', .)">
                        <dd><xsl:apply-templates select="." /></dd>
                    </xsl:for-each>
                </dl>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template name="actions">
        <div class="actions">
            <h2>Report Actions</h2>
            <p>
                <!-- TODO: make link generation -->
                <a class="doc" href="javascript:void(0);"><i></i>Save to Word</a>
                <a class="pdf" href="javascript:void(0);"><i></i>Save to PDF</a>
            </p>
        </div>
    </xsl:template>

    <!--- Content Part -->
    <xsl:template name="testContent">
        <div class="testContent">
            <xsl:call-template name="conc" />
            <xsl:call-template name="testlog" />
            <xsl:call-template name="env" />
        </div>
    </xsl:template>

    <xsl:template name="conc">
        <div class="conc">
            <h2>Test conclusions</h2>
            <xsl:if test="not(contains(',passed,completed,failed,aborted,', concat(',', $resolution, ',')))" >
                <p><strong>Disclaimer: The test in currently in progress. Information placed on this page currently cannot be concerned as final and thus Performance Team has NO responsibility in decisions made based on it.</strong></p>
            </xsl:if>
            <xsl:apply-templates select="//conclusions" />
        </div>
    </xsl:template>

    <xsl:template name="testlog">
        <xsl:if test="//test_log">
        <div class="testLog">
            <h3>Test log</h3>
            <ul>
                <xsl:apply-templates select="//test_log" />
            </ul>
        </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="env">
        <xsl:if test="(//environment) or (//test_conditions)">
            <div class="environment">
                <h2>Environment</h2>
                <xsl:if test="//environment">
                    <h3>Hardware and OS</h3>
                    <dl>
                        <xsl:apply-templates select="//environment" />
                    </dl>
                </xsl:if>
                <xsl:if test="//test_conditions">
                <h3>Initial test parameters and test flow</h3>
                <dl>
                    <xsl:apply-templates select="//test_conditions" />
                </dl>
                </xsl:if>
             </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="testResults">
        <xsl:for-each select="//report_body">
            <div class="testResults">
                <xsl:if test="@title">
                    <h2><xsl:value-of select="@title" /></h2>
                </xsl:if>
                <xsl:if test="not(@title)">
                    <h2>Test Results</h2>
                </xsl:if>
                <xsl:call-template name="split-string">
                    <xsl:with-param name="list"><xsl:value-of select="@order" /></xsl:with-param>
                    <xsl:with-param name="delimiter">,</xsl:with-param>
                    <xsl:with-param name="action">widget</xsl:with-param>
                </xsl:call-template>
            </div>
        </xsl:for-each>
        <!--<xsl:call-template name="actions" />-->
    </xsl:template>


<!-- templates for elements -->
    <xsl:template match="role">
        <xsl:choose>
            <xsl:when test="@mail">
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>mailto:</xsl:text>
                        <xsl:value-of select="@mail" />
                    </xsl:attribute>
                    <xsl:text>@</xsl:text>
                    <xsl:value-of select="." />
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="." />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="paragraph">
        <p class="{@css}">
             <xsl:apply-templates />
        </p>
    </xsl:template>

    <xsl:template match="br">
       <br />
    </xsl:template>

    <xsl:template match="additional_info/record">
        <p class="{@css}">
            <xsl:value-of select="." />
        </p>
    </xsl:template>

    <xsl:template match="record">
        <li>
            <xsl:if test="@time">
                <span class="date"><xsl:value-of select="@time" /></span>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:apply-templates />
        </li>
    </xsl:template>

    <xsl:template match="item">
         <xsl:choose>
            <xsl:when test="((@description='') or not(@description)) and (.='')">
                <dt class="empty"></dt>
            </xsl:when>
            <xsl:otherwise>
                <dt><xsl:value-of select="@description" />:</dt>
                <dd><xsl:apply-templates /></dd>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="intro">
        <p class="{@css}">
            <xsl:value-of select="." />
        </p>
    </xsl:template>

    <xsl:template match="log">
        <div class="request">
            <h4><xsl:value-of select="@name" /></h4>
            <xsl:if test="@comment">
                <p><xsl:value-of select="@comment" /></p>
            </xsl:if>
            <xsl:if test="record">
                <ul>
                <xsl:apply-templates />
                </ul>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="incidents">
        <xsl:if test="@title">
            <h3><xsl:value-of select="@title" /></h3>
        </xsl:if>
        <xsl:if test="not(@title)">
            <h3>Incidents Report</h3>
        </xsl:if>
        <ul>
            <xsl:apply-templates />
        </ul>
    </xsl:template>

    <xsl:template match="incident">
        <li>
            <xsl:if test="@active='true'"><xsl:attribute name="class">active</xsl:attribute></xsl:if>
            <h4><i></i><xsl:value-of select="@name" /></h4>
            <ul>
                <xsl:apply-templates />
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="comments">
        <xsl:apply-templates />
    </xsl:template>

    <xsl:template match="table">
        <xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="graph">
        <div class="graph-heading">
            <xsl:if test="../@title">
                <h3><xsl:value-of select="../@title" /></h3>
            </xsl:if>
            <ul class="switcher">
                <xsl:for-each select="tab" >
                    <li>
                        <xsl:if test="position() = last()">
                            <xsl:attribute name="class">selected</xsl:attribute>
                        </xsl:if>
                        <a href="#"><xsl:value-of select="@name" /></a>
                    </li>
                </xsl:for-each>
            </ul>
        </div>
        <div class="graph-body">
            <ul class="moduleList">
                <xsl:for-each select="tab[1]/image">
                    <xsl:variable name="pos"><xsl:value-of select="position()"/></xsl:variable>
                    <li>
                        <xsl:if test="$pos = 1">
                            <xsl:attribute name="class">selected</xsl:attribute>
                        </xsl:if>

                        <a href="#">
                            <xsl:attribute name="paths">
                                <xsl:text>({'paths' :[</xsl:text>
                                <xsl:for-each select="../../tab/image[position() = $pos]">
                                    <xsl:text>{</xsl:text>
                                    <xsl:value-of select="position() - 1" />
                                    <xsl:text>: '</xsl:text>
                                    <xsl:choose>
                                        <xsl:when test="starts-with(@path, '/')">
                                            <xsl:text>../../</xsl:text>
                                            <xsl:value-of select="$selfDir" />
                                        </xsl:when>
                                        <xsl:when test="(@path!='') and not(starts-with(@path, '/')) and not(contains(@path, '.xml'))">
                                             <xsl:text>/</xsl:text>
                                        </xsl:when>
                                    </xsl:choose>
                                    <xsl:value-of select="@path" />
                                    <xsl:text>'}</xsl:text>

                                    <xsl:if test="position() != last()">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                </xsl:for-each>

                                <xsl:text>]})</xsl:text>
                            </xsl:attribute>
                            <!--<xsl:if test="@settings">
                                <xsl:attribute name="settings">
                                    <xsl:value-of select="@settings" />
                                </xsl:attribute>
                            </xsl:if>-->
							<xsl:attribute name="settings">
								<xsl:text>({'settings' :[</xsl:text>
                                <xsl:for-each select="../../tab/image[position() = $pos]">
                                    <xsl:text>{</xsl:text>
                                    <xsl:value-of select="position() - 1" />
                                    <xsl:text>: '</xsl:text>
                                    <xsl:choose>
                                        <xsl:when test="starts-with(@settings, '/')">
                                            <xsl:text>../../</xsl:text>
                                            <xsl:value-of select="$selfDir" />
                                        </xsl:when>
                                        <xsl:when test="(@settings!='') and not(starts-with(@settings, '/')) and not(contains(@settings, '.xml'))">
                                             <xsl:text>/</xsl:text>
                                        </xsl:when>
                                    </xsl:choose>
                                    <xsl:value-of select="@settings" />
                                    <xsl:text>'}</xsl:text>

                                    <xsl:if test="position() != last()">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                </xsl:for-each>

                                <xsl:text>]})</xsl:text>
							</xsl:attribute>
                            <xsl:attribute name="types">
                                <xsl:text>({'types' :[</xsl:text>
                                <xsl:for-each select="../../tab/image[position() = $pos]">
                                    <xsl:text>{</xsl:text>
                                     <xsl:value-of select="position() - 1" />
                                     <xsl:text>: '</xsl:text>
                                        <xsl:value-of select="@type" />
                                     <xsl:text>'}</xsl:text>

                                    <xsl:if test="position() != last()">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                </xsl:for-each>
                                <xsl:text>]})</xsl:text>
                            </xsl:attribute>
                            <xsl:value-of select="@name" />
                         </a>
                    </li>
                </xsl:for-each>
            </ul>
            <div class="graphImg">
                <xsl:if test="../@cropable='true'">
                    <xsl:attribute name="class">graphImg cropable</xsl:attribute>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="tab[last()]/image[1]/@path">
                        <xsl:choose>
                            <xsl:when test="not(tab[last()]/image[1]/@type) and not(contains(tab[last()]/image[1]/@path, '.xml'))">
                                <img>
                                    <xsl:attribute name="src">
                                        <xsl:choose>
                                            <xsl:when test="starts-with(tab[last()]/image[1]/@path, '/')">
                                                <xsl:text>../../</xsl:text>
                                                <xsl:value-of select="$selfDir" />
                                            </xsl:when>
                                            <xsl:when test="(tab[last()]/image[1]/@path!='') and not(starts-with(tab[last()]/image[1]/@path, '/'))  and not(contains(tab[last()]/image[1]/@path, '.xml'))">
                                                 <xsl:text>/</xsl:text>
                                            </xsl:when>
                                        </xsl:choose>
                                        <xsl:value-of select="tab[last()]/image[1]/@path" />
                                    </xsl:attribute>
                                </img>
                           </xsl:when>
                           <xsl:otherwise>
                              <p class="embedFlash" data="{tab[last()]/image[1]/@path}">
                                    <xsl:if test="tab[last()]/image[1]/@settings">
                                        <xsl:attribute name="settings">
                                            <xsl:value-of select="tab[last()]/image[1]/@settings" />
                                        </xsl:attribute>
                                    </xsl:if>
                                    <xsl:if test="tab[last()]/image[1]/@type">
                                        <xsl:attribute name="type">
                                            <xsl:value-of select="tab[last()]/image[1]/@type" />
                                        </xsl:attribute>
                                    </xsl:if>
                                    <xsl:text>Please install flash-player</xsl:text>
                                </p>
                           </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise><p class="notAvailable">N/A</p></xsl:otherwise>
                </xsl:choose>
            </div>

        </div>

    </xsl:template>

    <xsl:template match="stage">
        <div class="level ">
            <xsl:attribute name="class">
                <xsl:text>level</xsl:text>
                <xsl:if test="@active"> active</xsl:if>
                <xsl:if test="position() = last()"> last</xsl:if>
            </xsl:attribute>
           
            
        <xsl:variable name="path">
            <xsl:choose>
                <xsl:when test="@path">
                    <xsl:if test="starts-with(@path, '/')">
                        <xsl:text>../</xsl:text>
                        <xsl:value-of select="$selfDir" />
                    </xsl:if>
                    <xsl:if test="starts-with(@path, 'filedata')">
                        <xsl:text>../</xsl:text>
                    </xsl:if>
                    <xsl:value-of select="@path" />
                </xsl:when>
                <xsl:otherwise />
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="name" select="@name" />
        
            <h4><i></i><xsl:value-of select="@name" /></h4>
            <ul>
                <xsl:for-each select="image">
                    <xsl:variable name="self" select="generate-id(.)" />
                    <xsl:variable name="short_name" select="@short_name" />
                    <xsl:variable name="mask" select="@mask" />

                    <xsl:variable name="data">
                        return {cmpData: {
                                    imgPath: '',
                                    defaultImg: '<xsl:value-of select="concat($path, @filename)" />',
                                    modules: '<xsl:value-of select="@full_name" />',
                                    originalText: '<xsl:value-of select="$name"/>'
                                },
                        selects: [[{text: 'choose stage'}
                            <xsl:for-each select="../../stage/image[(@short_name = $short_name) and not(@compare='skip')]" >
                                <xsl:if test='generate-id(.) != $self'>
                                <xsl:variable name="extpath">
                                    <xsl:choose>
                                        <xsl:when test="../@path">
                                            <xsl:if test="starts-with(../@path, '/')">
                                                <xsl:text>../</xsl:text>
                                                <xsl:value-of select="$selfDir" />
                                            </xsl:if>
                                            <xsl:value-of select="../@path" />
                                        </xsl:when>
                                        <xsl:otherwise />
                                    </xsl:choose>
                                </xsl:variable>
                                , {text: '<xsl:value-of select="../@name" />',
                                  data: ['<xsl:value-of select="concat($extpath, @mask)" />','<xsl:value-of select="../@name" />']
                                  <xsl:if test="position() = 1">
                                      ,defaultOption: false
                                  </xsl:if>
                                  }
                                 </xsl:if>
                            </xsl:for-each>
                                    ],
                            [{text: 'choose test'}
                            <xsl:for-each select="../compare_external">
                                <xsl:variable name="extpath">
                                    <xsl:choose>
                                        <xsl:when test="@path">
                                            <xsl:if test="starts-with(@path, '/')">
                                                <xsl:text>../</xsl:text>
                                                <xsl:value-of select="$selfDir" />
                                            </xsl:if>
                                            <xsl:if test="starts-with(@path, 'filedata')">
                                                <xsl:text>../</xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="@path" />
                                        </xsl:when>
                                        <xsl:otherwise />
                                    </xsl:choose>
                                </xsl:variable>
                                <xsl:choose>
                                    <xsl:when test="@naming='same'">
                                        <xsl:if test="not(contains(concat(skip, ','),concat($short_name, ',')))">
                                            , {text: '<xsl:value-of select="@name" />',
                                               data: ['<xsl:value-of select="concat($extpath, $mask)" />','<xsl:value-of select="@name" />']
                                              }
                                         </xsl:if>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:if test="image[(@short_name = $short_name)]">
                                            , {text: '<xsl:value-of select="@name" />',
                                               data: ['<xsl:value-of select="concat($extpath, image[@short_name = $short_name]/@mask)" />','<xsl:value-of select="@name" />']
                                              }
                                        </xsl:if>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                                    ]]}
                    </xsl:variable>
                    <li>
                            <a onblur="{$data}" class="img thickbox passData" href="{$siteUrl}assets/graph.html?keepThis=true&amp;TB_iframe=true&amp;height=700&amp;width=900">
                                <img>
                                    <xsl:attribute name="src">
                                        <xsl:text>../</xsl:text>
                                        <xsl:value-of select="concat($path, @thumb)" />
                                    </xsl:attribute>
                                </img>
                             </a>
                            <a onblur="{$data}" class="thickbox passData" href="{$siteUrl}assets/graph.html?keepThis=true&amp;TB_iframe=true&amp;height=700&amp;width=900"><xsl:value-of select="@short_name" /></a>
                            <i></i>
                    </li>
                </xsl:for-each>

            </ul>
        </div>
    </xsl:template>

    <xsl:template match="zones">
        <xsl:variable name="top-offset" select="@offset" />
        <xsl:variable name="height" select="@lenght" />
        <div class="imageMap-body">
            <img alt="">
                <xsl:attribute name="src">
                    <xsl:if test="starts-with(../image/@path, '/')">
                        <xsl:text>../../</xsl:text>
                        <xsl:value-of select="$selfDir" />
                    </xsl:if>
                    <xsl:value-of select="../image/@path" />
                </xsl:attribute>
            </img>
            <xsl:for-each select="zone">
                <div class="map" style="width: {@lenght}px; left: {@offset}px; top: {$top-offset}px; height: {$height}px;">
                    <xsl:variable name="link"><xsl:value-of select="@link" /></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="starts-with(@link, 'http')"><a href="{@link}"></a></xsl:when>
                        <xsl:when test="@link"><a href="#{translate(translate(@link, ' ', '_'),  '/.', '')}"></a></xsl:when>
                        <xsl:otherwise><a href="javascript:void(0);"></a></xsl:otherwise>
                    </xsl:choose>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:template match="widget">
    </xsl:template>

    <xsl:template match="widget[@type='Logs'] | widget[@type='Text']">
        <div class="results {@css}">
            <xsl:call-template name="widget-head" />
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="widget[@type='Table']">
        <div>
            <xsl:attribute name="class">
                <xsl:text>contentTable </xsl:text>
                <xsl:if test="@realsize = 'true'"> autoWidth </xsl:if>
                <xsl:value-of select="@css" />
            </xsl:attribute>
            <xsl:call-template name="widget-head" />
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="widget[@type='Distributions']">
        <div class="diagrams {@css}">
            <xsl:call-template name="widget-head" />
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="widget[@type='Incidents']">
        <div class="incidents {@css}">
            <xsl:if test="@multiselect='true'"><xsl:attribute name="class">incidents multiSelect</xsl:attribute></xsl:if>
            <xsl:call-template name="widget-head" />
            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="widget[@type='Graphs']">
        <div class="graph {@css}">
            <xsl:attribute name="id">
                <xsl:value-of select="concat(@type, @id)" />
            </xsl:attribute>

            <xsl:if test="@link">
                <a><xsl:attribute name="name"><xsl:value-of select="translate(translate(@link, ' ', '_'),  '/.', '')" /></xsl:attribute></a>
            </xsl:if>

            <xsl:apply-templates />
        </div>
    </xsl:template>

    <xsl:template match="widget[@type='mappedimage']">
        <div class="imageMap {@css}">
            <xsl:call-template name="widget-head" />
            <xsl:apply-templates />


        </div>
    </xsl:template>

    <xsl:template name="widget-head">
        <xsl:param name="head" select="."/>
            
            <xsl:attribute name="id">
                <xsl:value-of select="concat($head/@type, $head/@id)" />
            </xsl:attribute>
           
            <xsl:if test="@link">
                <a><xsl:attribute name="name"><xsl:value-of select="translate(translate(@link, ' ', '_'),  '/.', '')" /></xsl:attribute></a>
            </xsl:if>
            
            <xsl:if test="@title">
                <h3><xsl:value-of select="$head/@title" /></h3>
            </xsl:if>
    </xsl:template>

    <xsl:template match="a">
        <xsl:copy-of select="." />
    </xsl:template>

    <xsl:template match="code">
        <span>
            <xsl:apply-templates select="*|text()" mode="copyall" />
        </span>
    </xsl:template>

    <xsl:template match="*|text()" mode="copyall">
        <xsl:copy>
            <xsl:apply-templates select="*|text()" mode="copyall" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="sup">
        <xsl:copy-of select="." />
    </xsl:template>

    <xsl:template match="footnote">
        <div class="note">
            <dl>
                <xsl:apply-templates />
            </dl>
        </div>
    </xsl:template>
    
    <xsl:template match="dt">
        <xsl:copy-of select="." />
    </xsl:template>
    
    <xsl:template match="note">
        <dd>
            <xsl:apply-templates />
        </dd>
    </xsl:template>

    <xsl:template match="span">
        <span class="{@css}">
            <xsl:apply-templates />
        </span>
    </xsl:template>

<!-- Functions -->

<!-- Split $list string with delimiter and apply $action -->
<xsl:template name="split-string">
    <xsl:param name="list" />
    <xsl:param name="delimiter" />
    <xsl:param name="action" />
    <xsl:variable name="newlist">
        <xsl:choose>
            <xsl:when test="contains($list, $delimiter)"><xsl:value-of select="normalize-space($list)" /></xsl:when>

            <xsl:otherwise><xsl:value-of select="concat(normalize-space($list), $delimiter)"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="first" select="substring-before($newlist, $delimiter)" />
    <xsl:variable name="remaining" select="substring-after($newlist, $delimiter)" />
        
    <xsl:choose>
        <xsl:when test="$action='tagoutput'">
            <xsl:call-template name="tagoutput">
                <xsl:with-param name="var"><xsl:value-of select="$first" /></xsl:with-param>
                <xsl:with-param name="notlast"><xsl:value-of select="$remaining" /></xsl:with-param>
            </xsl:call-template>
        </xsl:when>

        <xsl:when test="$action='widget'">
            <xsl:apply-templates select="//widget[@id=$first]" />
        </xsl:when>

        <xsl:when test="$action='toclink'">
            <xsl:if test="//widget[@id=$first]">
                <xsl:variable name="tocwidget" select="//widget[@id=$first]" />
                <xsl:if test="$tocwidget/@link" >
                    <li>
                        <a>
                            <xsl:attribute name="href">#<xsl:value-of select="translate(translate($tocwidget/@link, ' ', '_'), '/.', '')" /></xsl:attribute>
                            <xsl:value-of select="$tocwidget/@link" />
                        </a>
                    </li>
                </xsl:if>
            </xsl:if>
        </xsl:when>
    </xsl:choose>

    <xsl:if test="$remaining">
        <xsl:call-template name="split-string">
            <xsl:with-param name="list" select="$remaining" />
            <xsl:with-param name="delimiter"><xsl:value-of select="$delimiter"/></xsl:with-param>
            <xsl:with-param name="action" select="$action" />
         </xsl:call-template>
     </xsl:if>
  </xsl:template>

  <!-- Upper case first char -->
  <xsl:template name="first-upper">
    <xsl:param name="string" />

    <xsl:value-of select="translate(substring($string, 1,1),
                                'abcdefghijklmnopqrstuvwxyz',
                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                          )" />
    <xsl:value-of select="substring($string, 2)" />
  </xsl:template>

  <xsl:template name="lowercase">
    <xsl:param name="string" />

    <xsl:value-of select="translate($string,
                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                'abcdefghijklmnopqrstuvwxyz'
                          )" />
  </xsl:template>


  <xsl:template name="tagoutput">
        <xsl:param name="var" />
        <xsl:param name="notlast" />
        <li>
            <a href="{$siteUrl}{$project}/tag/{$var}">
                <xsl:value-of select="$var" />
            </a>
            <xsl:if test="$notlast!=''">, </xsl:if>
            </li>
    </xsl:template>
</xsl:stylesheet>
