from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next "admin" lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
admin.site.index_template     = "admin/layer_index.html"
admin.site.app_index_template = "admin/layer_app_index.html"


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'layer.views.home', name='home'),
    # url(r'^layer/', include('layer.foo.urls')),

    url(r'^accounts/login/$',
        'django.contrib.auth.views.login',
        {'template_name': 'login.html'}),

    url(r'^' + settings.MEDIA_URL + r'(?P<path>.*)$',
        'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/',
        include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/',
        include(admin.site.urls)),

    url(r'^_measurements/',
        include('measurements.urls', namespace="measurements")),
    url(r'^_env/',
        include('environments.urls', namespace="environments")),

    url(r'^', include('reports.urls', namespace='reports'))
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


