from collections import defaultdict
from measurements import models as meas_models
import django_tables2 as tables
from django_tables2.columns import library
from django_tables2_reports.tables import TableReport
from django.utils.safestring import mark_safe

class SimpleStatTable(tables.Table):
    Metric = tables.Column(accessor='entity')
    Qty = tables.Column(accessor='qty')
    Min = tables.Column(accessor='min')
    Median = tables.Column(accessor='median')
    P95 = tables.Column(accessor='95p')
    Max = tables.Column(accessor='max')

    class Meta:
        attrs = {'class': 'scaled horizontalComparison'}
        order_by = ('-median',)
        template = '_single_run_table.html'
        empty_text = 'No data presented'

class Statistics(TableReport):
    Metric = tables.Column(accessor='entity')
    Qty = tables.Column(accessor='qty')
    Min = tables.Column(accessor='min')
    Median = tables.Column(accessor='median')
    P95 = tables.Column(accessor='95p')
    Max = tables.Column(accessor='max')

    class Meta:
        attrs = {'class': 'scaled horizontalComparison'}
        order_by = ('-median',)
        template = '_simple_table.html'
        empty_text = 'No data presented'


@library.register
class TestLinkURLColumn(tables.Column):
    """
    Adds URL to TestLink; for columns where cell values start with testlink case id, eg 'PRF-403 Messages: Send Internal Message'
    """
    def render(self, value):
        tc_id = value.partition(' ')[0]
        tc_prj = tc_id.partition('-')[0]
        html = "<a href='http://testlink.dins.ru/linkto.php?tprojectPrefix=%s&item=testcase&id=%s'>%s</a>" % (tc_prj, tc_id, value)
        return mark_safe(html)


class SLAViolationsTable(tables.Table):
    TestCase = TestLinkURLColumn(accessor='tc_name')
    Title = tables.Column(accessor='tc_title',verbose_name='TestCase title')
    Component = tables.Column(accessor='component')
    Median = tables.Column(accessor='median')
    SLA = tables.Column(accessor='sla',verbose_name='SLA, ms')
    Priority = tables.Column(accessor='priority')

    class Meta:
        attrs = {'class': 'scaled horizontalComparison'}
        order_by = ('-median',)
        template = '_simple_table.html'
        empty_text = 'No data presented'


class SkippedTestCasesTable(tables.Table):
    TestCase = TestLinkURLColumn(accessor='tc_name')
    Title = tables.Column(accessor='tc_title')
    Component = tables.Column(accessor='component')
    Priority = tables.Column(accessor='priority')

    class Meta:
        attrs = {'class': 'scaled horizontalComparison'}
        order_by = ('-component',)
        template = '_simple_table.html'
        empty_text = 'No data presented'


class CoverageTable(tables.Table):
    name = tables.Column(accessor="name",verbose_name='Component')
    total_cases = tables.Column(accessor="total_cases")
    new_cases = tables.Column(accessor="new_cases")
    applicable_cases = tables.Column(accessor="applicable_cases")
    executed_cases = tables.Column(accessor="executed_cases")
    deprecated_cases = tables.Column(accessor="deprecated_cases")

    class Meta:
        order_by = ('name',)
        exclude = ('id', 'description', 'app')
        template = '_simple_table.html'


class CSECoverageTable(tables.Table):
    name = tables.Column(accessor="name",verbose_name="Area")
    high_priority_total = tables.Column(accessor='high_priority_total')
    normal_priority_total = tables.Column(accessor='normal_priority_total')
    high_priority_tested = tables.Column(accessor='high_priority_tested')
    normal_priority_tested = tables.Column(accessor='normal_priority_tested')
    p1_offenders = tables.Column(accessor='p1_offenders')
    p2_offenders = tables.Column(accessor='p2_offenders')
    applicable_cases = tables.Column(accessor='applicable_cases')
    deprecated_cases = tables.Column(accessor='deprecated_cases')

    class Meta:
        order_by = ('name',)
        exclude = ('id', 'description', 'app')
        template = '_simple_table.html'


class CSENewTestCasesTable(tables.Table):
    title = tables.Column(verbose_name="TestCase")
    description = tables.Column()
    result = tables.Column()
    sla = tables.Column()
    priority = tables.Column()
    component = tables.Column()

    class Meta:
        order_by = ('title',)
        model = meas_models.TestCase
        exclude = ('id', 'external_key', 'version', 'app')
        template = '_simple_table.html'
        empty_text = 'No data presented'


class LoadTable(tables.Table):
    component = tables.Column()
    rate = tables.Column(verbose_name='Rate, RPS')

    class Meta:
        order_by = ('component',)

    def render_rate(self, value):
        return round(value, 1)


def get_merged_table(labels, tables_dicts, key_column_name, merged_columns_order):

    # key column is always first column in merged_columns order

    # Build class for merged column
    #
    attrs = {}

    # General options
    class Meta:
        attrs = {'class': 'scaled horizontalComparison'}
        order_by = (key_column_name,)
        template = '_merged_table.html'
        empty_text = 'No data presented'
    attrs['Meta'] = Meta
    attrs[key_column_name] = tables.Column()
    for column in merged_columns_order:
        for label in labels:
            attrs[get_merged_key(column, label)] = tables.Column(verbose_name=label)
    merged_table = type('MergedTable', (tables.Table,), attrs)
    merged_columns = []
    for merged_column_name in merged_columns_order:
        column = tables.Column(verbose_name=merged_column_name,
                               attrs={"th": {"rowspan": len(labels)}})
        merged_columns.append(column)
    merged_table.merged_columns = merged_columns

    # Merge data for table
    data = merge_table_dicts(labels, tables_dicts, key_column_name)
    return merged_table(data)


def merge_table_dicts(labels, tables, key_column):
    merged_tables = []
    for table, label in zip(tables, labels):
        new_table = []
        for row in table:
            new_row = {}
            for key, value in list(row.items()):
                if key == key_column:
                    new_row[key] = value
                else:
                    new_row[get_merged_key(key, label)] = value
            new_table.append(new_row)
        merged_tables.append(new_table)
    d = defaultdict(dict)
    for l in merged_tables:
        for elem in l:
            d[elem[key_column]].update(elem)
    resulted_table = d.values()
    return resulted_table


def get_merged_key(old_key, label):
    return old_key + '_' + label.replace(' ', '').replace('.', '')
