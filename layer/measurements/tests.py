# Standard library
from io import BytesIO
from datetime import datetime, date
import time
import random
from unittest import skip

# Django modules
from django.db.models.loading import get_model
from django.db import transaction
from django.contrib.auth.models import User
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.core import exceptions as django_exceptions

# 3rd party modules
import pytz
from mock import MagicMock

# My modules
from . import tasks
from . import models
from apps import models as apps_models
from environments import models as env_models
from external_data import testcases
from external_data.servermonitoring.zabbix_accessor import ZabbixAccessor


class FakeFile(object):
    def __init__(self, name, data):
        self.name = name
        self.file = data

    def startswith(self, sep):
        if self.name[0] == sep:
            return True
        else:
            return False

    def endswith(self, sep):
        if self.name[-1] == sep:
            return True
        else:
            return False

    def __str__(self):
        return str(self.name)

    def __repr__(self):
        return str(self.name)

    def chunks(self):
        return self.file.readlines()


class BaseTest(TestCase):

    task_timeout = 30

    def setUp(self):
        with transaction.atomic():
            # Preparing database with test data
            # Apps section
            self.test_app = apps_models.App.objects.create(name="TestApp")
            self.test_component = apps_models.Component.objects.create(name="TestComponent",
                                                                       app=self.test_app)
            self.test_branch = apps_models.Branch.objects.create(name="TestBranch",
                                                                 date_created=date(2013, 6, 10))
            self.test_build = apps_models.Build.objects.create(version='1.2.3',
                                                               build_date=date(2013, 6, 15),
                                                               branch=self.test_branch,
                                                               app=self.test_app)
            self.test_release = apps_models.Release.objects.create(name='TestRelase',
                                                                   create_date=date(2013, 6, 16),
                                                                   main_build=self.test_build)
            # Environments section
            self.test_env = env_models.HardwareEnvironment.objects.create(name='TestHardwareEnvironment')
            # Testcase section
            self.test_case = models.TestCase.objects.create(external_key='PM-123',
                                                            title='Click here',
                                                            sla=1,
                                                            priority=1,
                                                            version=1)
            # Measurements section
            self.test_measure_type = models.ClientMeasurementType.objects \
                .create(name="Automatic")
            self.utc_timezone = pytz.timezone('UTC')
            self.test_run = models.Run.objects.create(name="Test Run",
                                                      tested_release=self.test_release,
                                                      timestamp_start=datetime(2013, 6, 20, 12).replace(
                                                          tzinfo=self.utc_timezone),
                                                      timestamp_end=datetime(2013, 6, 20, 18).replace(
                                                          tzinfo=self.utc_timezone),
                                                      environment=self.test_env)
            self.test_login = 'django'
            #self.test_password = make_password('django')
            self.test_password = 'django'
            self.test_user = User.objects.create_user(username=self.test_login,
                                                      password=self.test_password)

    def _generate_random_measurements_data(self, run, metrics_list, qty):
        """ Run should be within one month and one year
        """
        return_codes = ('OK', 'ERROR')
        rnd = random.SystemRandom()
        year = run.timestamp_start.year
        month = run.timestamp_start.month
        day_start = run.timestamp_start.day
        hour_start = run.timestamp_start.hour
        minute_start = run.timestamp_start.minute
        day_end = run.timestamp_end.day
        hour_end = run.timestamp_end.hour
        for x in range(qty):
            day = rnd.randint(day_start + 1, day_end - 1) if day_end - day_start >= 4 else day_start
            hour = rnd.randint(hour_start + 1, hour_end - 1) if hour_end - hour_start >= 4 else hour_start
            minute = rnd.randint(minute_start, 59)
            second = rnd.randint(1, 59)
            ms = rnd.randint(1, 500)
            timestamp = datetime(year,
                                 month,
                                 day,
                                 hour,
                                 minute,
                                 second,
                                 ms).replace(tzinfo=self.utc_timezone)
            metric = rnd.choice(metrics_list)
            value = rnd.random()
            account = str(rnd.randrange(1000000, 9000000))
            return_code = rnd.choice(return_codes)
            yield models.ClientMeasurement(timestamp=timestamp,
                                           run=run,
                                           value=value,
                                           account=account,
                                           metric=metric,
                                           return_code=return_code)

    def generate_rand_client_measurements(self, run, metrics_list, qty=30):
        with transaction.atomic():
            for measurement in self._generate_random_measurements_data(run, metrics_list, qty):
                measurement.save()


class SimpleTest(BaseTest):

    def test_get_tested_metrics(self):
        metrics = []
        with transaction.atomic():
            for metric_name in ('TestMetric1', 'TestMetric2', 'TestMetric3', 'TestMetric4'):
                metric = models.ClientMetric.objects.create(name=metric_name,
                                                            component=self.test_component,
                                                            measure_type=self.test_measure_type)
                metrics.append(metric)
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        tested_metrics = self.test_run.get_tested_client_metrics()
        original_names = [x.name for x in metrics]
        created_names = [x.name for x in tested_metrics]
        self.assertEqual(sorted(original_names), sorted(created_names))


class PlannedLoadTest(BaseTest):

    def setUp(self):
        super(PlannedLoadTest, self).setUp()
        self.metric1 = models.ClientMetric.objects.create(name='Metric1',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)
        self.metric2 = models.ClientMetric.objects.create(name='Metric2',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)

    def test_flag_creation(self):
        self.test_flag = models.Flag.objects.create(name='Planned Load')
        flag = 'Planned Load'
        created_flag = models.Flag.objects.last()
        self.assertEqual(flag, created_flag.name)
        created_flag.delete()


class ImportersTest(BaseTest):

    def _get_metrics_file(self):
        metrics = [b'Metric1', b'Metric2', b'Metric3']
        data = BytesIO(b'\n'.join(metrics))
        fake_file = FakeFile('dummy.txt', data)
        return fake_file

    def test_metric_simple_csv_ok(self):
        fake_file = self._get_metrics_file()
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'measure_type': self.test_measure_type,
                        'is_cse': False}
        upload_task = tasks.MetricsSimpleCsv(models.ClientMetric, cleaned_data)
        upload_task.task.get(self.task_timeout)
        created_metrics = models.ClientMetric.objects.all()
        created_metrics_names = [x.name.encode('utf-8') for x in created_metrics]
        self.assertEqual([b'Metric1', b'Metric2', b'Metric3'], created_metrics_names)
        created_metrics.delete()

    def test_metric_simple_csv_errors(self):
        fake_file = self._get_metrics_file()
        cleaned_data = {'data_file': fake_file,
                        'measure_type': self.test_measure_type}
        self.assertRaises(django_exceptions.ValidationError,
                          tasks.MetricsSimpleCsv,
                          models.ClientMetric,
                          cleaned_data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component}
        self.assertRaises(django_exceptions.ValidationError,
                          tasks.MetricsSimpleCsv,
                          models.ClientMetric,
                          cleaned_data)

    def test_client_measurements_simple_csv_ok(self):
        with transaction.atomic():
            metric = models.ClientMetric.objects.create(component=self.test_component,
                                                        name='Test Metric',
                                                        measure_type=self.test_measure_type)
        measurements = [b'2013-06-20 13:25:03;35.5',
                        b'2013-06-20 13:27:03.2;45.5',
                        b'2013-06-20 14:25:03.3;65.3']
        data = BytesIO(b'\n'.join(measurements))
        fake_file = FakeFile('dummy.csv', data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'metric': metric,
                        'run': self.test_run,
                        'timezone': 'UTC',
                        'account': ''}
        upload_task = tasks.MeasurementsSimpleCsv(cleaned_data, models.ClientMeasurement)
        # Wait task completion
        upload_task.task.get(self.task_timeout)
        created_measurements = models.ClientMeasurement.objects.all()
        self.assertEqual(len(created_measurements), len(measurements))
        earliest = created_measurements.order_by('timestamp')[0]
        self.assertEqual(earliest.value, 35.5)
        latest = created_measurements.order_by('-timestamp')[0]
        self.assertEqual(latest.value, 65.3)
        created_measurements.delete()
        metric.delete()

    def test_client_measurements_simple_csv_errors(self):
        with transaction.atomic():
            metric = models.ClientMetric.objects.create(component=self.test_component,
                                                        name='Test Metric',
                                                        measure_type=self.test_measure_type)
        measurements = [b'2013-05-20 13:25:03.1;35.5',
                        b'2013-06-20 13:27:03;45.5',
                        b'2013-07-20 14:25:03.3;65.3']
        data = BytesIO(b'\n'.join(measurements))
        fake_file = FakeFile('dummy.csv', data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'metric': metric,
                        'run': self.test_run,
                        'timezone': 'UTC',
                        'account': ''}
        upload_task = tasks.MeasurementsSimpleCsv(cleaned_data, models.ClientMeasurement)
        upload_task.task.get(self.task_timeout)
        created_measurements = models.ClientMeasurement.objects.all()
        self.assertEqual(len(created_measurements), 1)
        created_measurements.delete()
        models.ClientMetric.objects.all().delete()

    def test_client_measurements_bulk_csv_ok(self):
        metrics_names = ['Metric1', 'Metric2']
        for metric_name in metrics_names:
            with transaction.atomic():
                models.ClientMetric.objects.create(component=self.test_component,
                                                   name=metric_name,
                                                   measure_type=self.test_measure_type)
        measurements = [b'2013-06-20 13:01:01.1;Metric1;15.5',
                        b'2013-06-20 13:01:01.2;Metric2;17.5',
                        b'2013-06-20 13:02:02.3;Metric1;10.5',
                        b'2013-06-20 13:02:02;Metric2;27.3',
                        b'2013-06-20 13:03:03.1;Metric2;18.5']
        data = BytesIO(b'\n'.join(measurements))
        fake_file = FakeFile('dummy.csv', data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'run': self.test_run,
                        'account': '',
                        'timezone': 'UTC'}
        upload_task = tasks.ClientMeasurementsBulkCsv(cleaned_data,
                                            models.ClientMeasurement)
        upload_task.task.get(self.task_timeout)
        created_measurements = models.ClientMeasurement.objects.all()
        self.assertEqual(len(measurements), created_measurements.count())
        earliest = created_measurements.filter(metric__name='Metric1') \
            .order_by('timestamp')[0]
        self.assertEqual(earliest.value, 15.5)
        latest = created_measurements.filter(metric__name='Metric2') \
            .order_by('-timestamp')[0]
        self.assertEqual(latest.value, 18.5)
        created_measurements.delete()
        models.ClientMetric.objects.all().delete()

    def test_client_measurements_bulk_csv_errors(self):
        metrics_names = ['Metric1', 'Metric2']
        for metric_name in metrics_names:
            with transaction.atomic():
                models.ClientMetric.objects.create(component=self.test_component,
                                                   name=metric_name,
                                                   measure_type=self.test_measure_type)
        measurements = [b'2013-05-20 13:01:01.1;Metric1;15.5',
                        b'2013-06-20 13:01:01.2;Metric2;17.5',
                        b'2013-06-20 13:02:02;Metric1;10.5',
                        b'2013-06-20 13:02:02.4;Metric2;27.3',
                        b'2013-07-20 13:03:03.5;Metric2;18.5']
        data = BytesIO(b'\n'.join(measurements))
        fake_file = FakeFile('dummy.csv', data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'run': self.test_run,
                        'account': ''}
        upload_task = tasks.ClientMeasurementsBulkCsv(cleaned_data,
                                            models.ClientMeasurement)
        upload_task.task.get(self.task_timeout)
        created_measurements = models.ClientMeasurement.objects.all()
        self.assertEqual(len(created_measurements), 3)
        created_measurements.delete()

    def test_server_measurements_bulk_csv_ok(self):
        metrics_names = ['Metric1', 'Metric2']
        for metric_name in metrics_names:
            with transaction.atomic():
                models.ServerMetric.objects.create(component=self.test_component,
                                                   name=metric_name)
        server_name = 'server1'
        with transaction.atomic():
            server = env_models.Server.objects.create(name=server_name,
                                                      environment=self.test_env)
        measurements = [b'2013-06-20 13:01:01.1;Metric1;15.5',
                        b'2013-06-20 13:01:01.2;Metric2;17.5',
                        b'2013-06-20 13:02:02.3;Metric1;10.5',
                        b'2013-06-20 13:02:02;Metric2;27.3',
                        b'2013-06-20 13:03:03.1;Metric2;18.5']
        data = BytesIO(b'\n'.join(measurements))
        fake_file = FakeFile('dummy.csv', data)
        cleaned_data = {'data_file': fake_file,
                        'component': self.test_component,
                        'run': self.test_run,
                        'server': server,
                        'account': '',
                        'timezone': 'UTC'}
        upload_task = tasks.ServerMeasurementsBulkCsv(cleaned_data)
        upload_task.task.get(self.task_timeout)
        created_measurements = models.ServerMeasurement.objects.filter(server__name=server_name)
        self.assertEqual(len(measurements), created_measurements.count())
        created_measurements.delete()


class TableViewTest(BaseTest):

    def setUp(self):
        super(TableViewTest, self).setUp()
        self.metric1 = models.ClientMetric.objects.create(name='Metric1',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)
        self.metric2 = models.ClientMetric.objects.create(name='Metric2',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)

    def test_simple_table_view(self):
        with transaction.atomic():
            additional_release = apps_models.Release.objects.create(name='AdditionalTestReleaseGroup',
                                                                         main_build=self.test_build,
                                                                         create_date=date(2013, 7, 18))
            additional_run = models.Run.objects.create(name="Additional Run",
                                                       tested_release=additional_release,
                                                       environment=self.test_env,
                                                       timestamp_start=datetime(2013, 7, 20, 12).replace(
                                                           tzinfo=self.utc_timezone),
                                                       timestamp_end=datetime(2013, 7, 20, 18).replace(
                                                           tzinfo=self.utc_timezone))
        metrics = (self.metric1, self.metric2)
        self.generate_rand_client_measurements(self.test_run, metrics)
        self.generate_rand_client_measurements(additional_run, metrics)
        table_url = reverse('measurements:client_measurements_table',
                            kwargs={'app_name': self.test_component.name,
                                    'run_id': self.test_run.id})
        response = self.client.get(table_url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(b'Metric1' in response.content)

    def test_load_profile_table_view_smoke(self):
        metrics = []
        with transaction.atomic():
            for metric_name in ('TestMetric1', 'TestMetric2', 'TestMetric3', 'TestMetric4'):
                metric = models.ClientMetric.objects.create(name=metric_name,
                                                            component=self.test_component,
                                                            measure_type=self.test_measure_type)
                metrics.append(metric)
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        load_profile_table_url = reverse('measurements:load_profile_table', kwargs={'run_id': self.test_run.id})
        response = self.client.get(load_profile_table_url)
        self.assertEqual(response.status_code, 200)

    @skip
    def test_client_measurements_stat_table_perf_smoke(self):
        with transaction.atomic():
            test_run = models.Run.objects.create(name="Fat Run",
                                                 tested_release=self.test_release,
                                                 timestamp_start=datetime(2013, 6, 1).replace(
                                                     tzinfo=self.utc_timezone),
                                                 timestamp_end=datetime(2013, 6, 30, 22).replace(tzinfo=self.utc_timezone),
                                                 environment=self.test_env)
        metrics = []
        with transaction.atomic():
            for i in range(90):
                metric = models.ClientMetric.objects.create(name='FatMetric' + str(i),
                                                            component=self.test_component,
                                                            measure_type=self.test_measure_type)
                metrics.append(metric)
        self.generate_rand_client_measurements(test_run, metrics, qty=100000)

        stat_table_url = reverse('measurements:client_measurements_table',
                                 kwargs={'app_name': self.test_component.name,
                                         'run_id': self.test_run.id})
        start = time.time()
        response = self.client.get(stat_table_url)
        response_time = time.time() - start
        self.assertEqual(response.status_code, 200)


class StandardViewTest(BaseTest):

    def setUp(self):
        super(StandardViewTest, self).setUp()
        self.metric1 = models.ClientMetric.objects.create(name='Metric1',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)
        self.metric2 = models.ClientMetric.objects.create(name='Metric2',
                                                          component=self.test_component,
                                                          measure_type=self.test_measure_type)

    def test_client_measurements_json_view(self):
        self.generate_rand_client_measurements(self.test_run, (self.metric1, self.metric2))
        results_url = reverse('measurements:client_side_json_results',
                              kwargs={
                                  'metric_name': self.metric1.name,
                                  'run_id': self.test_run.id
                              })
        response = self.client.get(results_url)
        self.assertEqual(response.status_code, 200)


class UploadTest(BaseTest):

    def setUp(self):
        super(UploadTest, self).setUp()
        self.initialize_cse_report_file()
        with transaction.atomic():
            self.cse_run = models.Run.objects.create(name="CSE Run",
                                                     tested_release=self.test_release,
                                                     timestamp_start=datetime(2013, 10, 17, 10).replace(
                                                         tzinfo=self.utc_timezone),
                                                     timestamp_end=datetime(2013, 10, 17, 20).replace(
                                                         tzinfo=self.utc_timezone),
                                                     environment=self.test_env)
        self.client.login(username=self.test_login, password=self.test_password)

    def generate_fat_data(self, run, measurements_number):
        metrics_names = ['Metric' + str(i) for i in range(100)]
        metrics = []
        for metric_name in metrics_names:
            with transaction.atomic():
                metric = models.ClientMetric.objects.create(component=self.test_component,
                                                            name=metric_name,
                                                            measure_type=self.test_measure_type)
                metrics.append(metric)
        data = []
        datetime_format = '%Y-%m-%d %H:%M:%S.%f'
        for measurement in self._generate_random_measurements_data(run, metrics, measurements_number):
            timestamp = measurement.timestamp.strftime(datetime_format).encode('utf-8')
            metric = measurement.metric.name.encode('utf-8')
            return_code = measurement.return_code.encode('utf-8')
            value = str(measurement.value).encode('utf-8')
            account = str(measurement.account).encode('utf-8')
            entry = b';'.join((timestamp, metric, value, return_code, account)) + b';'
            data.append(entry)
            del measurement
        return data

    def initialize_cse_report_file(self):
        text_sample = b"""date","time","TC","scenario","action","measurement item","version","tool","cycle","priority","value","SLA","keyword","test type","environment","account number","area"
"10/17/2013","15:18:26","PERF-SW-0060","PHONE_SYSTEM_processMainNumber","Settings, Phone System, Company number  ->  Main company number "," Pane upload","5.16.000.73","muineles","1","0","1.047","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:18:43","PERF-SW-0060","PHONE_SYSTEM_processMainNumber","Settings, Phone System, Company number  ->  Main company number "," Pane upload","5.16.000.73","muineles","2","0","0.593","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:00","PERF-SW-0060","PHONE_SYSTEM_processMainNumber","Settings, Phone System, Company number  ->  Main company number "," Pane upload","5.16.000.73","muineles","3","0","0.594","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:18","PERF-SW-0063-A","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number ->  Direct extension number "," Acknowledgement","5.16.000.73","muineles","1","0","0.219","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:18","PERF-SW-0063","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number ->  Direct extension number "," Pane download","5.16.000.73","muineles","1","0","2.922","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:21","PERF-SW-0064-A","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number, Direct extension number ->   Save "," Acknowledgement","5.16.000.73","muineles","1","0","0.203","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:21","PERF-SW-0064","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number, Direct extension number  ->   Save "," Page download","5.16.000.73","muineles","1","0","15.469","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:54","PERF-SW-0063-A","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number ->  Direct extension number "," Acknowledgement","5.16.000.73","muineles","2","0","0.109","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:54","PERF-SW-0063","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number ->  Direct extension number "," Pane download","5.16.000.73","muineles","2","0","2.047","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:57","PERF-SW-0064-A","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number, Direct extension number ->   Save "," Acknowledgement","5.16.000.73","muineles","2","0","0.218","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:19:57","PERF-SW-0064","PHONE_SYSTEM_processExtensionNumber","Settings, Phone System, Company number, Direct extension number  ->   Save "," Page download","5.16.000.73","muineles","2","0","15.187","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:20:28","PERF-SW-0061","PHONE_SYSTEM_processFaxNumber","Settings, Phone System, Company number  ->  Fax "," Pane upload","5.16.000.73","muineles","1","0","0.234","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:20:29","PERF-SW-0062-A","PHONE_SYSTEM_processFaxNumber","Settings, Phone System, Company number, Fax  ->  Save "," Acknowledgement","5.16.000.73","muineles","1","0","2.313","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:20:29","PERF-SW-0062","PHONE_SYSTEM_processFaxNumber","Settings, Phone System, Company number, Fax  ->  Save "," Page upload","5.16.000.73","muineles","1","0","16.5","0","wordkey","auto","SS POD2","12072061234","Web"
"10/17/2013","15:21:02","PERF-SW-0061","PHONE_SYSTEM_processFaxNumber","Settings, Phone System, Company number  ->  Fax "," Pane upload","5.16.000.73","muineles","2","0","0.297","0","wordkey","auto","SS POD2","12072061234","Web"""
        self.cse_report_file = BytesIO(text_sample)
        self.cse_report_file.name = 'intermediate_results.csv'

    def test_simple_metrics_upload(self):
        metrics_list = [b'Metric1', b'Metric2', b'Metric3']
        data_file = BytesIO(b'\n'.join(metrics_list))
        data_file.name = 'dummy.csv'
        # NB: faking for server side checks
        simple_upload_url = reverse('measurements:upload_client_metrics_simple')
        response = self.client.post(simple_upload_url, {'data_file': data_file,
                                                        'component': self.test_component.id,
                                                        'measure_type': self.test_measure_type.id})
        self.assertEqual(response.status_code, 200)
        new_metrics = models.ClientMetric.objects.all()
        self.assertEqual(new_metrics.count(), len(metrics_list))

    def test_cse_metrics_upload(self):
        with transaction.atomic():
            additional_test_type = models.ClientMeasurementType.objects.create(name='Automatic Selenium',
                                                                               tool='muineles')
            additional_component = apps_models.Component.objects.create(name='Web',
                                                                        app=self.test_app)
        advanced_upload_url = reverse('measurements:upload_client_metrics_cse')
        response = self.client.post(advanced_upload_url, {'data_file': self.cse_report_file,
                                                          'component': additional_component.id,
                                                          'measure_type': additional_test_type.id,
                                                          'uploader': 'CSEMetricsReportBasedUploader'})

        self.assertEqual(response.status_code, 200)
        time.sleep(10)
        new_cse_metrics = models.ClientMetric.objects.filter(is_cse=True)
        # Metric name is TC, so all uniq
        self.assertEqual(new_cse_metrics.count(), 8)

    def test_get_metrics_from_zabbix(self):
        metrics = ['Metric1', 'Metric2', 'Metric3']
        ZabbixAccessor.__init__ = MagicMock(return_value=None)
        ZabbixAccessor.get_hosts_metric_names = MagicMock(return_value=metrics)

        zabbix_upload_url = reverse('measurements:upload_server_metrics_zabbix')
        response = self.client.post(zabbix_upload_url, {'metrics': metrics})
        self.assertEqual(response.status_code, 200)

        available_metrics = list(models.ServerMetric.objects.all().values_list('name', flat=True))
        self.assertEqual(sorted(metrics), sorted(available_metrics))

    def test_cse_measurements_upload(self):
        # run test for create metrics in this db
        self.test_cse_metrics_upload()
        self.initialize_cse_report_file()
        cse_results_report_upload_url = reverse('measurements:upload_client_measurements')
        response = self.client.post(cse_results_report_upload_url, {'data_file': self.cse_report_file,
                                                                    'run': self.cse_run.id,
                                                                    'timezone': 'UTC',
                                                                    'uploader': 'CSEMeasurementsReportBasedUploader'})
        self.assertEqual(response.status_code, 200)
        new_cse_measurements = models.ClientMeasurement.objects.filter(run=self.cse_run)
        self.assertEqual(new_cse_measurements.count(), 15)

    @skip
    def test_client_measurements_fat_ok(self):
        with transaction.atomic():
            test_run = models.Run.objects.create(name="Fat Run",
                                                 tested_release=self.test_release,
                                                 timestamp_start=datetime(2013, 6, 10).replace(
                                                     tzinfo=self.utc_timezone),
                                                 timestamp_end=datetime(2013, 6, 30, 22).replace(tzinfo=self.utc_timezone),
                                                 environment=self.test_env)
        data_length = 10000
        data = self.generate_fat_data(test_run, data_length)
        data_file = BytesIO(b'\n'.join(data))
        data_file.name = 'dummy.csv'

        bulk_upload_url = reverse('measurements:upload_client_measurements')
        response = self.client.post(bulk_upload_url, {'data_file': data_file,
                                                      'component': self.test_component.id,
                                                      'run': test_run.id,
                                                      'timezone': 'UTC',
                                                      'uploader': 'ClientMeasurementsBulkCsv'})
        self.assertEqual(response.status_code, 200)
        new_measurements = models.ClientMeasurement.objects.all()
        self.assertEqual(new_measurements.count(), data_length)

    @skip
    def test_client_measurements_fat_err(self):
        test_run = models.Run.objects.create(name="Fat Run",
                                             tested_release=self.test_release,
                                             timestamp_start=datetime(2013, 6, 10).replace(
                                                 tzinfo=self.utc_timezone),
                                             timestamp_end=datetime(2013, 6, 30, 22).replace(tzinfo=self.utc_timezone),
                                             environment=self.test_env)
        data_length = 10000
        data = self.generate_fat_data(test_run, data_length)
        # add extra measurements outside the run and with duplicates
        errors_data = [
            # Outside the run, lower bound
            b'2013-06-01 12:25:48;Metric1;12.5;OK;',
            # Outside the run, upper bound
            b'2013-07-01 12:25:48;Metric1;12.5;OK;',
            # Duplicate
            b'2013-06-11 09:47:12;Metric2;14.5;OK;',
            b'2013-06-11 09:47:12;Metric2;14.5;OK;',
            # Wrong datetime format
            b'2013:06:11 09:47:12;Metric2;14.5;OK;',
            # Non existed metric
            b'2013-06-11 09:47:12;Non_Exists;14.5;OK;',
            # Non float value
            b'2013-06-11 09:47:12;Metric1;ERROR;OK;',
        ]

        test_data = data + errors_data
        data_file = BytesIO(b'\n'.join(test_data))
        data_file.name = 'dummy.csv'

        bulk_upload_url = reverse('measurements:upload_client_measurements')
        response = self.client.post(bulk_upload_url, {'data_file': data_file,
                                                      'component': self.test_component.id,
                                                      'run': test_run.id,
                                                      'timezone': 'UTC',
                                                      'uploader': 'ClientMeasurementsBulkCsv'})
        self.assertEqual(response.status_code, 200)
        new_measurements = models.ClientMeasurement.objects.all()
        self.assertTrue(new_measurements.count() >= data_length)  # + 1 is a first part of duplicate
        self.assertTrue(new_measurements.count() < len(test_data))


class TestCaseTest(BaseTest):

    def setUp(self):
        super(TestCaseTest, self).setUp()
        with transaction.atomic():
            for i in range(6):
                models.ClientMetric.objects.create(name='Metric' + str(i),
                                                   component=self.test_component,
                                                   measure_type=self.test_measure_type)
        self.prefix = 'PRF'
        # Patching a part of connector
        testcases.testlink_accessor.TestLinkDBAccessor.__init__ = MagicMock(return_value=None)
        metrics = models.ClientMetric.objects.all()

        def mock_get_applicable_cases(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics):
                tc = TestCaseModel(external_key='PRF-666' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1,
                                   component=self.test_component)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_applicable_cases = MagicMock(
            side_effect=mock_get_applicable_cases)

        def mock_new_cases(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics[:2]):
                tc = TestCaseModel(external_key='PRF-666' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_new_cases = MagicMock(side_effect=mock_new_cases)

        def mock_get_deprecated(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics[5:]):
                tc = TestCaseModel(external_key='PRF-555' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_deprecated_cases = MagicMock(side_effect=mock_get_deprecated)

    def test_fill_run_measurements_simple(self):
        cases_count_before = models.TestCase.objects.all().count()
        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics)
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        measurements = models.ClientMeasurement.objects.filter(run=self.test_run)
        executed_cases_count = measurements.values('test_case').distinct().count()
        executed_metrics_count = measurements.values('metric').distinct().count()
        cases_count_after = models.TestCase.objects.all().count()

        self.assertEqual(executed_cases_count, executed_metrics_count)
        self.assertGreater(cases_count_after, cases_count_before)

    def test_get_sla_violations_cases(self):
        median = 4000
        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        metric1, metric2 = metrics[:2]
        measurements1 = models.ClientMeasurement.objects.filter(run=self.test_run,
                                                                metric=metric1)
        measurements2 = models.ClientMeasurement.objects.filter(run=self.test_run,
                                                                metric=metric2)

        for measuremnt in measurements1:
            measuremnt.value = median + 100
            measuremnt.save()

        for measuremnt in measurements2:
            measuremnt.value = median + 100
            measuremnt.save()
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        violate_cases = self.test_run.get_sla_violations_cases()
        self.assertEqual(len(violate_cases), 2)

    def test_get_sla_violations_stat(self):
        force_median = 4000
        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        metric1, metric2 = metrics[:2]
        measurements1 = models.ClientMeasurement.objects.filter(run=self.test_run,
                                                                metric=metric1)
        measurements2 = models.ClientMeasurement.objects.filter(run=self.test_run,
                                                                metric=metric2)
        for measuremnt in measurements1:
            measuremnt.value = force_median + 100
            measuremnt.save()

        for measuremnt in measurements2:
            measuremnt.value = force_median + 100
            measuremnt.save()
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        stat = self.test_run.get_sla_violations_stat()
        self.assertEqual(len(stat), 2)

    def test_get_coverage_stat_smoke(self):
        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        stat = self.test_run.get_coverage_stat_by_app()
        sample_stat = stat[0]
        self.assertIsNotNone(sample_stat)


class TestCaseTestAdvanced(BaseTest):

    def setUp(self):
        super(TestCaseTestAdvanced, self).setUp()
        with transaction.atomic():
            for i in range(6):
                models.ClientMetric.objects.create(name='Metric' + str(i),
                                                   component=self.test_component,
                                                   measure_type=self.test_measure_type)

    def test_get_coveragte_stat_0_new(self):
        # Patch testlink connector
        testcases.testlink_accessor.TestLinkDBAccessor.__init__ = MagicMock(return_value=None)
        self.prefix = 'PRF'
        testcases.testlink_accessor.TestLinkDBAccessor.get_new_cases = MagicMock(return_value=[])

        def mock_get_applicable_cases(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics):
                tc = TestCaseModel(external_key='PRF-666' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_applicable_cases = MagicMock(
            side_effect=mock_get_applicable_cases)

        def mock_get_deprecated(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics[5:]):
                tc = TestCaseModel(external_key='PRF-555' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_deprecated_cases = MagicMock(side_effect=mock_get_deprecated)

        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        stat = self.test_run.get_coverage_stat_by_app()
        sample_stat = stat[0]
        self.assertIsNotNone(sample_stat)

    def test_get_coveragte_stat_0_deprecated(self):
        # Patch testlink connector
        testcases.testlink_accessor.TestLinkDBAccessor.__init__ = MagicMock(return_value=None)
        self.prefix = 'PRF'
        testcases.testlink_accessor.TestLinkDBAccessor.get_deprecated_cases = MagicMock(return_value=[])

        def mock_get_applicable_cases(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics):
                tc = TestCaseModel(external_key='PRF-666' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_applicable_cases = MagicMock(
            side_effect=mock_get_applicable_cases)

        def mock_new_cases(release_short_name, start_range, end_range):
            TestCaseModel = get_model('measurements', 'TestCase')
            res = []
            for i, metric in enumerate(metrics[:2]):
                tc = TestCaseModel(external_key='PRF-666' + str(i),
                                   version=1,
                                   title='Sample case ' + str(i),
                                   sla=3000,
                                   priority=1)
                tc.key_field = metric.name
                tc.parameters = None
                res.append(tc)
            return res
        testcases.testlink_accessor.TestLinkDBAccessor.get_new_cases = MagicMock(side_effect=mock_new_cases)

        metrics = models.ClientMetric.objects.all()
        self.generate_rand_client_measurements(self.test_run, metrics, qty=100)
        task = tasks.fill_cases_for_measurements.delay(self.test_run)
        task.get(3600)
        stat = self.test_run.get_coverage_stat_by_app()
        sample_stat = stat[0]
        self.assertIsNotNone(sample_stat)


class IssuesTest(BaseTest):
    def setUp(self):
        super(IssuesTest, self).setUp()
        issue = models.Issue.objects.create(jira_key='ENV-123',
                                            title='Something goes wrong')
        self.test_run.issue_set.add(issue)
        self.assertEqual(self.test_run.get_run_issues().count(), 1)

        sla_violation_issue = models.Issue.objects.create(jira_key='RLZ-123',
                                                          title='slowpoke is so slow',
                                                          testcase=self.test_case)
        self.test_run.issue_set.add(sla_violation_issue)
        self.assertEqual(self.test_run.get_run_issues().count(), 2)
        self.assertEqual(self.test_case.get_testcase_issues().count(), 1)