from django import forms
from django.forms.models import BaseInlineFormSet
from django.core.exceptions import ValidationError

from django.contrib import admin
from dbparti.admin import PartitionableAdmin

from . import models


class ClientMetricAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'unit', 'component', 'measure_type')
    list_filter = ('component', 'measure_type')
    search_fields = ('name', 'measure_type__name', 'measure_type__description')

    class Meta:
        model = models.ClientMetric


class MetricTransformInline(admin.TabularInline):
    model = models.MetricTransform
    fields = ('page_id', 'unit', 'filter', 'multiply', 'round_digits')


class ServerMetricInline(admin.TabularInline):
    model = models.ServerMetric
    fields = ('name', 'unit', 'component', 'description')


class ServerMetricAdmin(admin.ModelAdmin):
    class Meta:
        model = models.ServerMetric

    list_select_related = True
    inlines = [ MetricTransformInline ]
    fields = (('name', 'unit'), ('family_ref', 'family_unit'),
              'description', 'test_case', 'component' )
    list_display = ('name', 'unit', 'component', 'family_ref')
    list_filter = ('component', 'family_ref')
    search_fields = ('name',)
    readonly_fields = ('family_unit',)

    def family_unit(self, instance):
        return instance.family_ref.unit if instance.family_ref else ''
    family_unit.short_description = "Family unit"


class MetricFamilyAdmin(admin.ModelAdmin):
    list_select_related = True
    inlines = [ MetricTransformInline, ServerMetricInline ]
    fields = ('name', 'unit',  'description')
    list_display = ('name', 'unit', 'description')
    search_fields = ('name', 'description')

    class Meta:
        model = models.MetricFamily

'''
class MetricTransformAdminForm(forms.ModelForm):
    def clean_metric(self):
        metric_key = self.cleaned_data['metric']
        try:
            kw_query = {'pk': int(metric_key)}
        except:
            kw_query = {'family': metric_key}
        m_model = models.ClientMetric if self.cleaned_data['is_client'] else models.ServerMetric
        for metric in m_model.objects.filter(**kw_query):
            units = metric.unit
            return metric_key
        raise ValidationError("Not found metric with id or family '%s'" % metric_key)
        return metric_key
'''
class MetricTransformAdmin(admin.ModelAdmin):
    class Meta:
        model = models.MetricTransform

    #    form = MetricTransformAdminForm
    list_select_related = False
    fields = ('page_id', ('metric_family', 'family_unit'), ('server_metric', 'server_metric_unit'),
              ('unit', 'round_digits'), 'filter', 'multiply' )
    list_display = ('page_id', 'metric_family', 'server_metric', 'unit')
    list_filter =  ('page_id', 'metric_family')
    search_fields = ('page_id', 'metric_family', 'server_metric')
    readonly_fields = ('family_unit', 'server_metric_unit')

    def family_unit(self, instance):
        return instance.metric_family.unit if instance.metric_family else ''
    # short_description functions like a model field's verbose_name
    family_unit.short_description = "Unit"

    def server_metric_unit(self, instance):
        return instance.server_metric.get_unit() if instance.server_metric else ''
    # short_description functions like a model field's verbose_name
    server_metric_unit.short_description = "Unit"



class ServerMeasurementAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ['server', 'metric', 'timestamp', 'value']
    list_filter = ['run', 'metric__family_ref', 'metric', 'timestamp',
                   'server__role', 'server']

    class Meta:
        model = models.ServerMeasurement


class ClientMeasurementAdmin(PartitionableAdmin):
    partition_show = 'all'
    list_select_related = True
    list_display = ['metric', 'timestamp', 'value']

    # TODO: fix, produces a lot of SQL queries
    list_filter = ['metric__component']

    class Meta:
        model = models.ClientMeasurement


class TestCaseAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ['external_key', 'title', 'component']
    list_filter = ('component',)
    search_fields = ('external_key',)

    class Meta:
        model = models.TestCase


class RunAdmin(admin.ModelAdmin):
    list_display = ['name', 'tested_release', 'environment']
    list_filter = ('tested_release',)
    search_fields = ('name',)

    class Meta:
        model = models.Run

admin.site.register(models.ClientMeasurementType)
admin.site.register(models.ClientMetric, ClientMetricAdmin)
admin.site.register(models.ClientMeasurement, ClientMeasurementAdmin)
admin.site.register(models.ServerMetric, ServerMetricAdmin)
admin.site.register(models.ServerMeasurement, ServerMeasurementAdmin)
admin.site.register(models.MetricTransform, MetricTransformAdmin)
admin.site.register(models.MetricFamily, MetricFamilyAdmin)
admin.site.register(models.Run,RunAdmin)
admin.site.register(models.TestCase,TestCaseAdmin)
admin.site.register(models.Flag)