from django.db import models
from django.core import cache as dj_cache
from numpy import median, average, percentile
import re
import logging
import time
logger = logging.getLogger(__name__)


# MANAGERS
class MeasureStatManager(models.Manager):
    def get_stat(self, grouper='metric__name', **kwargs):
        if type(kwargs['run']) is str:
            key = (grouper+kwargs['run']+re.sub(r'[^a-zA-Z0-9]' ,'', str(kwargs)))
        else:
            key = (grouper+str(kwargs['run'].id)+re.sub(r'[^a-zA-Z0-9]' ,'', str(kwargs)))
        cache = dj_cache.get_cache('default')
        cached_result = cache.get(key)
        if cached_result is None:
            statistics = []
            start_time = time.time()

            grouped_entites = self.get_query_set() \
                .filter(**kwargs).order_by(grouper)\
                .values_list(grouper, flat=True) \
                .distinct()

            for grouped_entity in grouped_entites:
                kwargs[grouper] = grouped_entity
                values = self.get_query_set() \
                    .filter(**kwargs) \
                    .values_list('value', flat=True)
                values = tuple(values)
                if values:
                    cur_stat = {
                        'entity': grouped_entity,
                        'qty': len(values),
                        'min': min(values),
                        'avg': average(values),
                        'max': max(values),
                        'median': median(values),
                        '95p': percentile(values, 95)
                    }
                    statistics.append(cur_stat)
            response_time = time.time() - start_time
            logger.info('Statistics has been calculated for {time} seconds'.format(time=response_time))
            cache.set(key,statistics)
            return statistics
        else:
            return cached_result

class CSETestCasesManager(models.Manager):
    def get_queryset(self):
        return super(CSETestCasesManager, self).get_queryset().filter(clientmetric__is_cse=True)


class RealRunsManager(models.Manager):
    def get_queryset(self):
        return super(RealRunsManager, self).get_queryset().exclude(run__flags__name='Planned Load')
