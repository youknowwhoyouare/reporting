# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetricTransform'
        db.create_table('measurements_metrictransform', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page_id', self.gf('django.db.models.fields.CharField')(max_length=120, default='_default_')),
            ('is_client', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('metric', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('units', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('filter', self.gf('django.db.models.fields.CharField')(max_length=120, default='')),
            ('divide_by', self.gf('django.db.models.fields.FloatField')(blank=True, null=True)),
            ('round_digits', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('measurements', ['MetricTransform'])

        # Adding unique constraint on 'MetricTransform', fields ['page_id', 'is_client', 'metric']
        db.create_unique('measurements_metrictransform', ['page_id', 'is_client', 'metric'])

        """ Was applied with not-saved 0023_auto__del_field_issue_testcase__add_field_issue_test_case.py   ------------------------------------
        # Deleting field 'Issue.testcase'
        db.delete_column('measurements_issue', 'testcase_id')

        # Adding field 'Issue.test_case'
        db.add_column('measurements_issue', 'test_case',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, to=orm['measurements.TestCase']),
                      keep_default=False)
        """


    def backwards(self, orm):
        # Removing unique constraint on 'MetricTransform', fields ['page_id', 'is_client', 'metric']
        db.delete_unique('measurements_metrictransform', ['page_id', 'is_client', 'metric'])

        # Deleting model 'MetricTransform'
        db.delete_table('measurements_metrictransform')

        """ Was applied with not-saved 0023_auto__del_field_issue_testcase__add_field_issue_test_case.py   ------------------------------------
        """
        # Adding field 'Issue.testcase'
        db.add_column('measurements_issue', 'testcase',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, null=True, to=orm['measurements.TestCase']),
                      keep_default=False)

        # Deleting field 'Issue.test_case'
        db.delete_column('measurements_issue', 'test_case_id')


    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['apps.Vendor']"})
        },
        'apps.branch': {
            'Meta': {'object_name': 'Branch', 'ordering': "['-date_created']"},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'object_name': 'Build', 'unique_together': "(('version', 'app'),)", 'ordering': "['-build_date', 'version']"},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Build']", 'symmetrical': 'False'})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.server': {
            'Meta': {'object_name': 'Server', 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)", 'ordering': "['name']"},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_components': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'null': 'True'})
        },
        'measurements.account': {
            'Meta': {'object_name': 'Account', 'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)"},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'object_name': 'ClientMeasurement', 'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)"},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['environments.ClientEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '500', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'on_delete': 'models.SET_NULL', 'null': 'True', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '300', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'tool': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'null': 'True'})
        },
        'measurements.clientmetric': {
            'Meta': {'object_name': 'ClientMetric', 'unique_together': "(('component', 'name', 'measure_type'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.TestCase']", 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'})
        },
        'measurements.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '12'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Run']", 'symmetrical': 'False'}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.TestCase']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.metrictransform': {
            'Meta': {'object_name': 'MetricTransform', 'unique_together': "(('page_id', 'is_client', 'metric'),)"},
            'divide_by': ('django.db.models.fields.FloatField', [], {'blank': 'True', 'null': 'True'}),
            'filter': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_client': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'metric': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'page_id': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "'_default_'"}),
            'round_digits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'units': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'measurements.run': {
            'Meta': {'object_name': 'Run', 'ordering': "['-timestamp_end']"},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.Flag']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'object_name': 'ServerMeasurement', 'unique_together': "(('metric', 'server', 'timestamp', 'run'),)"},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '500', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'on_delete': 'models.SET_NULL', 'null': 'True', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'object_name': 'ServerMetric', 'unique_together': "(('component', 'name'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.TestCase']", 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"})
        },
        'measurements.testcase': {
            'Meta': {'object_name': 'TestCase', 'unique_together': "(('external_key', 'version'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']"}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']