# Stadard library
from django.http import HttpResponse
import logging
logger = logging.getLogger(__name__)

# Django modules
from django.core import cache as dj_cache
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views import generic as generic_views
from django.core.cache import cache
from django.core import exceptions as  django_exceptions
from django.db import IntegrityError, DatabaseError
from django.views.decorators.cache import cache_page
# 3rd party modules
from django_tables2 import RequestConfig
from django_tables2_reports.views import ReportTableView
from django_tables2_reports.config import RequestConfigReport as RequestReportConfig
from django_tables2_reports.utils import create_report_http_response
import xlwt
# My modules

from utils import mixins
from . import models
from . import forms
from . import tasks

from . import tables
from environments import models as env_models
from apps import models as apps_models
from reports import models as report_models

# Static views
class MeasurementsManageView(generic_views.TemplateView):
    template_name = 'measurements.html'


# Uploaders
class BaseUploadView(generic_views.FormView):
    template_name = 'upload.html'
    models_to_upload = None   # Should be defined in child class
    uploaders = None          # Same here

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        uploader = getattr(tasks, form.cleaned_data['uploader'])
        upload_results = {'uploaded': 0,
                          'failed': 0}
        custom_errors = None
        try:
            the_uploader = uploader(form.cleaned_data, self.models_to_upload)
            upload_results = the_uploader.get_result_info()
        except django_exceptions.ValidationError as e:
            custom_errors = e
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results,
                                   'custom_errors': custom_errors},
                                  context_instance=RequestContext(self.request))

    def get_form(self, form_class):
        if self.request.POST:
            # get bound form
            return self.form_class(self.uploaders,
                                   self.request.POST,
                                   self.request.FILES)
        else:
            return self.form_class(self.uploaders)


# Metric uploaders
class BaseMetricsUploadView(generic_views.FormView):
    template_name = 'upload.html'
    models_to_upload = None  # should be defined in child classes

    def form_valid(self, form):
        the_importer = tasks.MetricsSimpleCsv(self.models_to_upload,
                                                  form.cleaned_data)
        upload_results = the_importer.get_result_info()
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results},
                                  context_instance=RequestContext(self.request))


class ClientMetricsUploadView(BaseMetricsUploadView):
    form_class = forms.ClientMetricsSimpleUploadForm
    models_to_upload = models.ClientMetric


class ServerMetricsUploadView(BaseMetricsUploadView):
    form_class = forms.ServerMetricsSimpleUploadForm
    models_to_upload = models.ServerMetric


class ServerMetricsZabbixGetView(generic_views.FormView):
    template_name = 'upload.html'
    form_class = forms.ServerMetricsZabbixGetForm

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        upload_results = {'uploaded': 0,
                          'failed': 0,
                          'err_lines': []}
        for metric_name in form.cleaned_data['metrics']:
            try:
                if form.cleaned_data['component']:
                    models.ServerMetric.objects.create(name=metric_name,
                                                       app=form.cleaned_data['component'])
                else:
                    models.ServerMetric.objects.create(name=metric_name)
                upload_results['uploaded'] += 1
            except (IntegrityError, DatabaseError):
                upload_results['failed'] += 1
                upload_results['err_lines'].append(metric_name)
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results},
                                  context_instance=RequestContext(self.request))


class CSEMetricsUploadView(BaseUploadView):
    #! Note the different base class
    models_to_upload = models.ClientMetric
    form_class = forms.CSEMetricsUploadForm
    uploaders = (('CSEMetricsReportBasedUploader', tasks.CSEMetricsReportBasedUploader.__doc__),)


# Measurements uploaders
class ClientMeasurementsUploadViewView(BaseUploadView):
    form_class = forms.ClientMeasurementsUploadForm
    models_to_upload = models.ClientMeasurement
    uploaders = (('MeasurementsSimpleCsv', tasks.MeasurementsSimpleCsv.__doc__),
                 ('ClientMeasurementsBulkCsv', tasks.ClientMeasurementsBulkCsv.__doc__,),
                 ('CSEMeasurementsReportBasedUploader', tasks.CSEMeasurementsReportBasedUploader.__doc__),)


class ServerMeasurementsUploadViewView(BaseUploadView):
    form_class = forms.ServerMeasurementsUploadForm
    models_to_upload = models.ServerMeasurement
    uploaders = (('tasks.MeasurementsSimpleCsv', tasks.MeasurementsSimpleCsv.__doc__),
                 ('tasks.ServerMeasurementsBulkCsv', tasks.ServerMeasurementsBulkCsv.__doc__))


class ServerMeasurementsZabbixGetView(generic_views.FormView):
    template_name = 'upload.html'
    form_class = forms.ServerMeasurementsZabbixGetForm

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        upload_task = tasks.import_server_measurements_zabbix.delay(form.cleaned_data['run'],
                                                                    form.cleaned_data['metrics'],
                                                                    form.cleaned_data['servers'])
        upload_results = {'task': upload_task}
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results},
                                  context_instance=RequestContext(self.request))


class ClientMeasurementsAppReportTableView(ReportTableView):
    table_class = tables.Statistics
    model = models.ClientMeasurement
    template_name = '_table.html'
    context_object_name = 'table'
    def get_context_data(self, **kwargs):
        context = super(ClientMeasurementsAppReportTableView, self).get_context_data(**kwargs)
        context['title'] = self.kwargs['app_name'] + ' results table'

        return context

    def get_table(self):
        table = None

        component = apps_models.Component.objects.get(name=self.kwargs['app_name'])
        measurements_stat = self.model.analyzes.get_stat(run=self.kwargs['run_id'],metric__is_cse=False,
                                                         metric__component=component.id,return_code='OK')
        table = tables.Statistics(measurements_stat)
        self.table_to_report = RequestReportConfig(self.request).configure(table)
        return table


# Results views
class ClientMeasurementsAppTableView(generic_views.DetailView):
    model = models.ClientMeasurement
    template_name = '_table.html'
    context_object_name = 'table'
    addt_runs_param_name = 'additional_runs'

    def get_context_data(self, **kwargs):
        context = super(ClientMeasurementsAppTableView, self).get_context_data(**kwargs)
        context['title'] = self.kwargs['app_name'] + ' results table'
        context['component']= self.kwargs['app_name']
        context['run']= self.kwargs['run_id']
        return context

    def get_object(self, queryset=None):
        table = None
        if self.addt_runs_param_name in self.request.GET:
            # Looking for additional runs
            # Maximum number of runs in the table is 5
            max_addt_runs = 5
            try:
                raw_list = self.request.GET[self.addt_runs_param_name].split(',')
                if len(set(raw_list)) <= 1:
                    self.get_simple_table()
                additional_runs_id = [int(x) for x in raw_list[:max_addt_runs]]
                run_ids = [int(self.kwargs['run_id'])] + additional_runs_id
                table_datas = []
                labels = []
                for run_id in run_ids:
                    try:
                        run = models.Run.objects.get(id=run_id)
                    except django_exceptions.ObjectDoesNotExist:
                        continue
                    labels.append(run.__unicode__())
                    data = self.model.analyzes.get_stat(run=run,metric__is_cse=False,return_code='OK',
                                                        metric__component__name=self.kwargs['app_name'])
                    table_datas.append(data)
                if len(table_datas) > 1:
                    key_column = 'entity'
                    merged_columns_order = ('qty', 'min', 'median', '95p', 'max')
                    table = tables.get_merged_table(labels,
                                                    table_datas,
                                                    key_column,
                                                    merged_columns_order)
                else:
                    # can't find any additional run for merged table
                    # fall back to the simple table
                    self.get_simple_table()
            except ValueError:
                # cannot parse additional runs parameter, fall back to simple table
                table = self.get_simple_table()
        else:
            table = self.get_simple_table()
        RequestConfig(self.request, paginate={"per_page": 10}).configure(table)
        return table


    def get_simple_table(self):
        component = apps_models.Component.objects.get(name=self.kwargs['app_name'])
        measurements_stat = self.model.analyzes.get_stat(run=self.kwargs['run_id'],metric__is_cse=False,
                                                         metric__component=component.id,return_code='OK')
        return tables.SimpleStatTable(measurements_stat)


class ClientMeasurementsResultsView(mixins.JSONResponseMixin, generic_views.ListView):
    model = models.ClientMetric
    results_model = models.ClientMeasurement

    def get_queryset(self):
        metric = self.model.objects.get(id=self.kwargs['metric_id'])
        return self.results_model.objects.filter(metric=metric,metric__is_cse=False,
                                                 run=self.kwargs['run_id'])

    def get_context_data(self, **kwargs):
        context = super(ClientMeasurementsResultsView, self).get_context_data(**kwargs)
        metric_id = self.model.objects.get(pk=self.kwargs['metric_id']).id
        return metric_id,list(context['object_list'].values_list('timestamp', 'value'))


class RunsOffendersStatView(mixins.JSONResponseMixin, generic_views.ListView):
    result_model = models.Run
    model = report_models.Project

    def get_queryset(self):
        return self.result_model.objects.filter(flags__name__contains='Combined')

    def get_context_data(self, **kwargs):
        context = super(RunsOffendersStatView, self).get_context_data(**kwargs)
        stats = []
        for run in list(context['object_list']):
            stat_entry = []
            stat_entry.append(run.timestamp_start)
            stat_entry.append(len(run.get_sla_violations_cases()))
            stats.append(stat_entry)
        return 'sla_violations', stats


class MedianToSLAStatView(mixins.JSONResponseMixin, generic_views.ListView):
    result_model = models.Run
    model = report_models.Project

    def get_queryset(self):
        return self.result_model.objects.filter(flags__name__contains='Combined')

    def get_context_data(self, **kwargs):
        context = super(MedianToSLAStatView, self).get_context_data(**kwargs)
        stats = []
        for run in list(context['object_list']):
            stat_entry = []
            stat_entry.append(run.timestamp_start)
            stat_entry.append(len(run.get_sla_violations_cases()))
            stats.append(stat_entry)
        return 'sla_violations', stats


class ClientMeasurementsReleasesStatsView(mixins.JSONResponseMixin, generic_views.ListView):
    model = models.ClientMetric
    results_model = models.ClientMeasurement

    def get_queryset(self):

        return self.results_model.analyzes.get_stat(grouper='run__timestamp_start',
                                                                               metric_id=self.kwargs['metric_id'])
    def get_context_data(self, **kwargs):
        context = super(ClientMeasurementsReleasesStatsView, self).get_context_data(**kwargs)
        metric_id = self.model.objects.get(pk=self.kwargs['metric_id']).id
        return metric_id,list([x.get('entity'),x.get('median')] for x in context['object_list'])



class ServerMeasurementsResultsView(mixins.GraphDataResponseMixin, generic_views.ListView):
    model = models.ServerMetric
    results_model = models.ServerMeasurement

    def get_queryset(self):
        #metric = self.model.objects.get(pk=self.kwargs['metric_id']) # commented out to avoid SQL query at this step (per ListView scheme)
        return self.results_model.objects.filter(metric=self.kwargs['metric_id'],
                                                 run=self.kwargs['run_id'],
                                                 server__name=self.kwargs['server_name'])

    def get_context_data(self, **kwargs):
        context = super(ServerMeasurementsResultsView, self).get_context_data(**kwargs)
        metric_id = self.kwargs['metric_id']
        metric = self.model.objects.get(pk=metric_id)
        try:
            run_title = str( models.Run.objects.get(pk=self.kwargs['run_id']) )
        except models.Run.DoesNotExist:
            run_title = "Not-found run "+ self.kwargs['run_id']

        self.context = {'target_metric': metric.name}
        self.context['target_run'] = run_title

        cache_key = str(self.kwargs['run_id'])+'_'+metric_id+'_'+str(self.kwargs['server_name']) +'_results'
        cache = dj_cache.get_cache('default')
        values_list = cache.get(cache_key)
        if values_list is None:
            values_list = list(context['object_list'].values_list('timestamp', 'value'))
            cache.set(cache_key,values_list)
        return metric_id, values_list


class SLAViolationsStatTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'

    def get_object(self, queryset=None):
        run = self.model.objects.get(pk=self.kwargs['run_id'])
        data = run.get_decorated_sla_violations_table()
        table = tables.SLAViolationsTable(data)
        RequestConfig(self.request, paginate=False).configure(table)
        return table


def get_tc_table_row(x):
    return dict(tc_name=x.external_key, tc_title=x.title, component=x.component, priority=x.priority)

class SkippedTestCasesTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'
    cache = dj_cache.get_cache('default')


    def get_object(self, queryset=None):
        key = str(self.kwargs['run_id'])
        diff_list = cache.get(key+'_skipped')
        if diff_list is None:
            run = self.model.objects.get(pk=self.kwargs['run_id'])
            components = [x['component'] for x in run.get_load_profile()]
            executed_list   = list(get_tc_table_row(x) for x in list(run.get_executed_cases()))
            applicable_list = list(get_tc_table_row(x) for x in list(run.get_applicable_testcases(components=components)))
            diff_list = [x for x in applicable_list if x not in executed_list]
        cache.set(key+'_skipped',diff_list)
        table = tables.SkippedTestCasesTable(diff_list)
        RequestConfig(self.request, paginate={"per_page": 20}).configure(table)
        return table

class TestCaseCoverageTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'


    def calcultate_summary(self, data):
        data_list = [list(elem) for elem in data]
        table_data = []
        total_cases = 0
        executed_cases = 0
        applicable_cases = 0
        new_cases = 0
        deprecated_cases = 0
        for entry in data_list:
            total_cases += entry[1]
            executed_cases += entry[2]
            applicable_cases += entry[3]
            new_cases += entry[4]
            deprecated_cases += entry[5]
            entry = dict(name=entry[0], total_cases=entry[1], executed_cases=entry[2],
                         applicable_cases=entry[3], new_cases=entry[4], deprecated_cases=entry[5])
            table_data.append(entry)

        total_row = dict(name='~Total', total_cases=total_cases, executed_cases=executed_cases,
                         applicable_cases=applicable_cases, new_cases=new_cases, deprecated_cases=deprecated_cases)
        table_data.append(total_row)
        return table_data

    def get_object(self, queryset=None):

        run = self.model.objects.get(pk=self.kwargs['run_id'])
        components = [x['component'] for x in run.get_load_profile()]
        table_data = self.calcultate_summary(run.get_coverage_stat_by_app(components=components))
        table = tables.CoverageTable(table_data)
        RequestConfig(self.request, paginate=False).configure(table)
        return table


class CSETestCaseCoverageTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'

    def calcultate_summary(self, data):
        data_list = [list(elem) for elem in data]
        table_data = []
        high_priority_total = 0
        normal_priority_total = 0
        high_priority_tested = 0
        normal_priority_tested = 0
        p1_offenders = 0
        p2_offenders = 0
        applicable_cases = 0
        deprecated_cases = 0
        for entry in data_list:
            high_priority_total += entry[1]
            normal_priority_total += entry[2]
            high_priority_tested += entry[3]
            normal_priority_tested += entry[4]
            p1_offenders += entry[5]
            p2_offenders += entry[6]
            applicable_cases += entry[7]
            deprecated_cases += entry[8]
            entry = dict(name=entry[0], high_priority_total=entry[1], normal_priority_total=entry[2],
                         high_priority_tested=entry[3], normal_priority_tested=entry[4], p1_offenders=entry[5],
                         p2_offenders=entry[6], applicable_cases=entry[7], deprecated_cases=entry[8])
            table_data.append(entry)

        total_row = dict(name='~Total', high_priority_total=high_priority_total,
                         normal_priority_total=normal_priority_total,
                         high_priority_tested=high_priority_tested, normal_priority_tested=normal_priority_tested,
                         p1_offenders=p1_offenders, p2_offenders=p2_offenders,
                         applicable_cases=applicable_cases, deprecated_cases=deprecated_cases)
        table_data.append(total_row)
        return table_data


    def get_object(self, queryset=None):
        run = self.model.objects.get(pk=self.kwargs['run_id'])
        table_data = self.calcultate_summary(run.get_coverage_stat_by_app(True))
        table = tables.CSECoverageTable(table_data)
        RequestConfig(self.request, paginate=False).configure(table)
        return table


class CSENewTestCasesTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'

    def get_object(self, queryset=None):
        run = self.model.objects.get(pk=self.kwargs['run_id'])
        table = tables.CSENewTestCasesTable(run.get_cse_new_cases())
        RequestConfig(self.request, paginate=False).configure(table)
        return table


class LoadProfileTableView(generic_views.DetailView):
    model = models.Run
    template_name = '_table.html'
    context_object_name = 'table'

    def get_object(self, queryset=None):
        run = self.model.objects.get(pk=self.kwargs['run_id'])
        table = tables.LoadTable(run.get_load_profile())
        RequestConfig(self.request, paginate=False).configure(table)
        return table


class ServerMetricBaseChartSettings(mixins.JSONResponseMixin, generic_views.DetailView):
    model = models.ServerMetric
    results_model = models.ServerMeasurement

    @staticmethod
    def get_graph_settings(metric):
        return {'title': metric.name,
                'match_key': metric.id}

    def get_context_data(self, **kwargs):
        context = super(ServerMetricBaseChartSettings, self).get_context_data(**kwargs)
        return context['object']

    def get_metric_units(self, metric):
        if 'page_id' in self.request.GET:
            metric_cfg = metric.get_metric_transform(self.request.GET['page_id'])
            if metric_cfg.unit:
                return metric_cfg.unit
        return str(metric.unit)


class ServerMetricSimpleChartSettings(ServerMetricBaseChartSettings):

    def get_object(self, queryset=None):
        if 'server_id' in self.request.GET:
            server_name = ' ('+env_models.Server.objects.get(id=self.request.GET['server_id']).name+') '
        else:
            server_name = ' '
        metric = self.model.objects.get(pk=self.kwargs['metric_id'])
        units = self.get_metric_units(metric)
        settings = {'title':  metric.name + ', ' + units + server_name + str(metric.description),
                    'y_label': units,
                    'x_label': 'Timeline',
                    'category_timestamp': True,
                    'graphs': [self.get_graph_settings(metric)]}
        if metric.unit == '%':
            settings['maximum'] = 100
        return settings




class ClientMetricSimpleChartSettings(mixins.JSONResponseMixin, generic_views.DetailView):

    model = models.ClientMetric
    results_model = models.ClientMeasurement

    @staticmethod
    def get_graph_settings(metric):
        return {'title': metric.name,
                'match_key': metric.id
                }

    def get_context_data(self, **kwargs):
        context = super(ClientMetricSimpleChartSettings, self).get_context_data(**kwargs)
        return context['object']

    def get_object(self, queryset=None):
        metric = self.model.objects.get(pk=self.kwargs['metric_id'])
        component = apps_models.Component.objects.get(clientmetric=metric)
        settings = {'title': metric.name  + ', ' +str(metric.unit) + ' ('+component.name+') ' + str(metric.description),
                    'y_label': metric.unit,
                    'x_label': 'Timeline',
                    'category_timestamp': True,
                    'graphs': [self.get_graph_settings(metric)],
                    'bullet': 'round',
                    'lineThickness': 0,
                    'bulletSize': 4}
        if metric.unit == '%':
            settings['maximum'] = 100
        return settings


class ServerMetricFamilySimpleChartSettings(ServerMetricBaseChartSettings):

    def get_object(self, queryset=None):
        family = models.MetricFamily.objects.get(name=self.kwargs['family'])
        # filter metrics in family available only for specific server
        if 'server_id' in self.request.GET and 'run_id' in self.request.GET:
            run = models.Run.objects.get(pk=self.request.GET['run_id'])
            available_metrics = run._get_available_metrics_for_server(self.request.GET['server_id'])
            metrics = available_metrics.filter(family_ref=family)
            server_name = ' ('+env_models.Server.objects.get(id=self.request.GET['server_id']).name+') '
        # get all metrics for selected family. Useless most of the times
        else:
            metrics = self.model.objects.filter(family_ref=family)
            server_name = ' '
        graphs = []
        units = self.get_metric_units(metrics[0])
        for metric in metrics:
            graphs.append(self.get_graph_settings(metric))
        settings = {'title': family.name + ', ' +units + server_name + str(family.description),
                    'y_label': units,
                    'x_label': 'Timeline',
                    'category_timestamp': True,
                    'graphs': graphs}
        if metrics[0].unit == '%':
            settings['maximum'] = 100
        return settings


class RunsStatBaseChartSettings(mixins.JSONResponseMixin, generic_views.DetailView):
    model = models.Run


    @staticmethod
    def get_graph_settings(title,match_key):
        return {'title': title,
                'match_key': match_key}

    def get_context_data(self, **kwargs):
        context = super(RunsStatBaseChartSettings, self).get_context_data(**kwargs)
        return context['object']


class RunsIssuesChartSettings(RunsStatBaseChartSettings):


    def get_object(self, queryset=None):
        settings = {'title': 'Offenders per release',
                    'y_label': 'Offenders',
                    'x_label': 'Timeline',
                    'category_timestamp': True,
                    'graphs': [self.get_graph_settings('Offenders per release','sla_violations')]}
        return settings



class FillTestCasesForRunView(generic_views.FormView):
    template_name = 'upload.html'
    form_class = forms.RunSelectionForm

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        run = form.cleaned_data['run']
        task = tasks.fill_cases_for_measurements.delay(run)
        return render_to_response(self.template_name,
                                  {'task': task},
                                  context_instance=RequestContext(self.request))

class LinkMetricsWithTestCases(generic_views.FormView):
    template_name = 'upload.html'
    form_class = forms.RunSelectionForm

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        run = form.cleaned_data['run']
        task = tasks.link_cases_with_metrics.delay(run)
        return render_to_response(self.template_name,
                                  {'task': task},
                                  context_instance=RequestContext(self.request))

class PerfFieldReportUploadView(BaseUploadView):
    models_to_upload = report_models.Test
    form_class = forms.PerfFieldTestsUploadForm
    uploaders = (('PerfFieldReportBasedUploader', tasks.PerfFieldReportBasedUploader.__doc__),)



def reset_cache(request):
    cache.clear()
    html = "<html><body>Cache cleaned.</body></html>"
    return HttpResponse(html)
