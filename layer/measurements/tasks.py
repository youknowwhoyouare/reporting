# Standard library
import csv
import sys
csv.field_size_limit(sys.maxsize)   # Set this for the big CSV files
from datetime import datetime
from io import TextIOWrapper
from os import remove
import tempfile


# Django modules
from django.conf import settings
from django.db import transaction, IntegrityError, DatabaseError
from django.db.models.related import RelatedObject
import django.core.exceptions as django_exceptions
from django.contrib.auth.models import User as usr_model
from django.core.cache import cache

# 3rd party modules
from pytz import timezone
from celery.utils.log import get_task_logger
from celery import group
logger = get_task_logger(__name__)

# my modules
from . import models, utils
from apps import models as apps_models
from environments import models as env_models
from reports import models as reports_models
from external_data.servermonitoring import accessor
from external_data.testcases import accessor as tcaccessor
from . import celery_app


class BaseUploader(object):
    """Base class for upload something into db"""

    upload_bulk_size = settings.UPLOAD_BULK_SIZE
    task_wait_timeout = 3600    # 1 hour
    defult_timestamp_format = settings.DEFAULT_TIMSTAMP_FORMAT
    parsed_fields = []

    def __init__(self, models_to_create, cleaned_data):

        self.cleaned_data = cleaned_data
        self.models_to_create = models_to_create
        self.all_models_fields = models_to_create._meta.get_all_field_names()
        if 'timezone' in cleaned_data:
            self.data_timezone = timezone(cleaned_data['timezone'])
        else:
            self.data_timezone = timezone('UTC')
        self.validate_fields()

    def validate_fields(self):
        """This method check if all required fields was presented in form

            Each child parser should present list of fields, which is extracted from the input file,
            rest should be presented in the web form"""
        for field_name in self.all_models_fields:
            # check that field is in form or will be parsed from file
            if field_name not in self.cleaned_data and field_name not in self.parsed_fields:
                # check if it is required
                the_field = self.models_to_create._meta.get_field_by_name(field_name)[0]
                # don't need primary keys or related objects
                if isinstance(the_field, RelatedObject) or the_field.primary_key:
                    continue
                elif the_field.blank:
                    continue
                elif the_field.null:
                    continue
                else:
                    raise django_exceptions.ValidationError("Field %s is required"
                                                            " for this uploader"
                                                            % field_name)

    def get_result_info(self):
        try:
            return {'task': self.task}
        except AttributeError:
            return {}

    @celery_app.task
    def upload(self, dict_models_list):
        """This method uploads dictionary for specified model into db

            Validation of values from dictionary perfmormed here."""
        upload_stat = {'failed_num': 0,
                       'uploaded_num': 0,
                       'error_lines': []}
        for parsed_model in dict_models_list:
            try:
                with transaction.atomic():
                    pre_saved_obj = self.models_to_create(**parsed_model)
                    pre_saved_obj.clean()
                    pre_saved_obj.save()
                upload_stat['uploaded_num'] += 1
            except (IntegrityError, DatabaseError, django_exceptions.ValidationError) as e:
                upload_stat['failed_num'] += 1
                logger.error(e)
                err_msg = str(parsed_model) + ': ' + str(e)
                upload_stat['error_lines'].append(err_msg)
        return upload_stat

    def bulk_upload(self, models_list):
        upload_stat = {'uploaded_num': 0}
        with transaction.atomic():
            upload_stat['uploaded_num'] = len(self.models_to_create.objects.bulk_create(models_list))
        return upload_stat

    def fill_form_values(self, model_entity):
        """This method fills values from form into model entity dictionary"""
        for field_name in self.all_models_fields:
            if field_name not in model_entity and field_name in self.cleaned_data:
                model_entity[field_name] = self.cleaned_data[field_name]
        return model_entity

    @staticmethod
    def get_temp_file_path(data_file):
        _, temp_file_name = tempfile.mkstemp(dir=settings.TMP_UPLOAD_LOCATION)
        with open(temp_file_name, 'wb') as destination:
            for chunk in data_file.chunks():
                destination.write(chunk)
        return temp_file_name


class RCLogsAbstractUploader(BaseUploader):
    """This class provides ability to parse output of RCLogs parser

    This logic separated into the class for handling
    """
    measurement_model = None    # Should be set in child classes
    metric_model = None
    parsed_fields = ['timestamp', 'value', 'metric']

    def __init__(self, cleaned_data, *args, **kwargs):
        BaseUploader.__init__(self, self.measurement_model, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = RCLogsAbstractUploader.parse.delay(self, temp_file_name)
        # self.task = self.parse.delay(temp_file_name)

    @celery_app.task
    def parse(self, csv_file_name, datetime_format=None, delimiter=';'):
        logger.info('Start parsing data from {filename}'.format(filename=csv_file_name))
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0}
        upload_tasks = []
        dict_models_list = []
        if datetime_format is None:
            datetime_format = self.defult_timestamp_format
        component_metrics = self.metric_model.objects.filter(component=self.cleaned_data['component'])
        metrics = {}
        for component_metric in component_metrics:
            metrics[component_metric.name] = component_metric
        with open(csv_file_name, 'rb') as csv_file:
            csv_reader = csv.reader(TextIOWrapper(csv_file), delimiter=delimiter)
            for i, line in enumerate(csv_reader):
                try:
                    metric_name = line[1]
                    try:
                        metric = metrics[metric_name]
                    except KeyError:
                        parse_stat['failed_num'] += 1
                        parse_stat['error_lines'].append((i + 1,
                                                          'Metric {name} doesn\'t exists '
                                                          'for this component '.format(name=metric_name)
                                                          + ': ' + str(line)))
                        continue
                    try:
                        timestamp = datetime.strptime(line[0], datetime_format).replace(tzinfo=self.data_timezone)
                    except ValueError:
                        # Milliseconds probably missed in round timestamps
                        line[0] += '.0'
                        timestamp = datetime.strptime(line[0], datetime_format).replace(tzinfo=self.data_timezone)
                    value = float(line[2])
                    try:
                        return_code = line[3]
                    except IndexError:
                        return_code = ''
                    try:
                        account = line[4]
                    except IndexError:
                        account = ''
                    measure = {'metric': metric,
                               'timestamp': timestamp,
                               'value': value,
                               'return_code': return_code,
                               'account': account}
                    dict_models_list.append(self.fill_form_values(measure))
                    if not i % self.upload_bulk_size and i != 0:
                        logger.info('UPload, but not everything parsed')
                        upload_task = RCLogsAbstractUploader.upload.s(self, dict_models_list)
                        dict_models_list = []
                        upload_tasks.append(upload_task)
                        #self.upload(measurements_dicts)
                except (ValueError, IndexError):
                    parse_stat['failed_num'] += 1
                    parse_stat['error_lines'].append((i + 1, line))
        upload_task = RCLogsAbstractUploader.upload.s(self, dict_models_list)
        upload_tasks.append(upload_task)
        g = group(*upload_tasks)
        remove(csv_file_name)


        return g()


class MeasurementsSimpleCsv(BaseUploader):
    """Measurements - one metric per file"""

    parsed_fields = ['timestamp', 'value']

    def __init__(self, cleaned_data, models_to_create, delimiter=';',
                 datetime_format=None):
        BaseUploader.__init__(self, models_to_create, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = MeasurementsSimpleCsv.parse.delay(self, temp_file_name)

    @celery_app.task(name='parse-measurements-csv-simple')
    def parse(self, csv_file_name, delimiter=';'):
        datetime_format = self.defult_timestamp_format
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0}
        upload_tasks = []
        dict_models_list = []

        with open(csv_file_name, 'rb') as csv_file:
            csv_reader = csv.reader(TextIOWrapper(csv_file), delimiter=delimiter)
            for i, line in enumerate(csv_reader):
                try:
                    if '.' not in line[0]:
                        line[0] += '.0'
                    timestamp = datetime.strptime(line[0], datetime_format).replace(tzinfo=self.data_timezone)
                    value = float(line[1])
                    measure = {'timestamp': timestamp,
                               'value': value}
                    # put the rest to the current model object
                    measure = self.fill_form_values(measure)
                    dict_models_list.append(measure)
                except (ValueError, IndexError):
                    parse_stat['failed_num'] = 1
                    parse_stat['error_lines'].append((i + 1, line))
                if not i % self.upload_bulk_size and i != 0:
                    upload_task = MeasurementsSimpleCsv.upload.s(self, dict_models_list)
                    upload_tasks.append(upload_task)
        upload_task = MeasurementsSimpleCsv.upload.s(self, dict_models_list)
        upload_tasks.append(upload_task)
        g = group(*upload_tasks)
        remove(csv_file_name)


        return g()


class ClientMeasurementsBulkCsv(RCLogsAbstractUploader):
    """Multiple client metrics"""
    measurement_model = models.ClientMeasurement
    metric_model = models.ClientMetric


class ServerMeasurementsBulkCsv(RCLogsAbstractUploader):
    """Server: - CSV multiple server measurements"""
    measurement_model = models.ServerMeasurement
    metric_model = models.ServerMetric


class MetricsSimpleCsv(BaseUploader):
    """Both: Upload simple client metric"""
    parsed_fields = ['name']

    def __init__(self, models_to_create, cleaned_data, delimiter=';'):
        BaseUploader.__init__(self, models_to_create, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = MetricsSimpleCsv.parse.delay(self, temp_file_name)

    @celery_app.task(name='parse-metric-simple-csv')   # names for avoiding intersection with other parse methods
    def parse(self, csv_file_name, delimiter=';'):
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0}
        upload_tasks = []
        dict_models_list = []

        with open(csv_file_name, 'rb') as csv_file:
            csv_reader = csv.reader(TextIOWrapper(csv_file), delimiter=delimiter)
            for i, line in enumerate(csv_reader):
                try:
                    metric_name = line[0]
                    # Todo: add unit fetching
                    metric = {'name': metric_name}
                    self.fill_form_values(metric)
                    dict_models_list.append(metric)
                except (ValueError, IndexError):
                    parse_stat['failed_num'] += 1
                    parse_stat['error_lines'].append((i + 1, line))
                    continue
                if i % self.upload_bulk_size == 0 and i != 0:
                    upload_task = MetricsSimpleCsv.upload.s(self, dict_models_list)
                    upload_tasks.append(upload_task)
                    dict_models_list = []
        upload_task = MetricsSimpleCsv.upload.s(self, dict_models_list)
        upload_tasks.append(upload_task)
        g = group(*upload_tasks)
        remove(csv_file_name)
        return g()


class CSEMetricsReportBasedUploader(BaseUploader):
    """Uploads CSE metrics from CSE Measurements Report"""
    parsed_fields = ['name', 'item', 'component', 'description', 'is_cse']

    def __init__(self, cleaned_data, *args, **kwargs):
        BaseUploader.__init__(self, models.ClientMetric, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = CSEMetricsReportBasedUploader.parse.delay(self, temp_file_name)

    @celery_app.task(name='parse-metrics-cse')
    def parse(self, csv_file_name, delimiter=','):
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0}
        upload_tasks = []
        dict_models_list = []

        with open(csv_file_name, 'rb') as csv_file:
            report_reader = utils.CSEReportReader(TextIOWrapper(csv_file), delimiter=delimiter)
            for i, entry in report_reader.data:
                try:
                    #measure_type = models.ClientMeasurementType.objects.get(tool=entry['tool_name'])
                    #component = apps_models.Component.objects.get(name=entry['application_name'])
                    metric = {'name': entry['metric_name'],
                              'description': entry['metric_description'],
                              'is_cse': True,
                             }

                    dict_models_list.append(self.fill_form_values(metric))
                    if i % self.upload_bulk_size == 0 and i != 0:
                        upload_task = CSEMetricsReportBasedUploader.upload.s(self, dict_models_list)
                        upload_tasks.append(upload_task)
                        dict_models_list = []
                except django_exceptions.ObjectDoesNotExist as err:
                    print (str(err))
                    parse_stat['failed_num'] += 1
                    parse_stat['error_lines'].append((i + 1, str(err) + ': ' + str(entry)))
                    continue
        upload_task = CSEMetricsReportBasedUploader.upload.s(self, dict_models_list)
        upload_tasks.append(upload_task)
        g = group(*upload_tasks)
        remove(csv_file_name)
        return g()


class CSEMeasurementsReportBasedUploader(BaseUploader):
    """Uploads CSE measurements results from standard report"""
    parsed_fields = ['test_case','metric', 'timestamp', 'value', 'component']

    def __init__(self, cleaned_data, *args, **kwargs):
        BaseUploader.__init__(self, models.ClientMeasurement, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = CSEMeasurementsReportBasedUploader.parse.delay(self, temp_file_name)

    @celery_app.task(name='parse-measuremetnts-cse')
    def parse(self, csv_file_name):
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0,
                      'parsed_num': 0}
        upload_tasks = []
        dict_models_list = []

        # TODO: add error translation from reader to importer error list
        data_timezone = timezone(self.cleaned_data['timezone'])
        environments = {}
        with open(csv_file_name, 'rb') as csv_file:
            report_reader = utils.CSEReportReader(TextIOWrapper(csv_file))
            for i, entry in report_reader.data:
                try:
                    #component = apps_models.Component.objects.get(name=entry['application_name'])

                    measure_type = models.ClientMeasurementType.objects.get(tool=entry['tool_name'])
                    metric = models.ClientMetric.objects.get(name=entry['metric_name'],
                                                             measure_type=measure_type,
                                                             is_cse=True)
                    try:
                        environment = environments[entry['environment']]
                    except KeyError:
                        environment, _ = env_models.ClientEnvironment.objects.get_or_create(name=entry['environment'])
                        environments[entry['environment']] = environment
                    cse_measurement = {'metric': metric,
                                       'timestamp': entry['timestamp'].replace(tzinfo=data_timezone),
                                       'value': entry['value'],
                                       'account': entry['account_number'],
                                       'environment': environment}
                    cse_measurement = self.fill_form_values(cse_measurement)
                    dict_models_list.append(cse_measurement)
                    parse_stat['parsed_num'] += 1
                    if i % self.upload_bulk_size == 0 and i != 0:
                        upload_task = CSEMeasurementsReportBasedUploader.upload.s(self, dict_models_list)
                        upload_tasks.append(upload_task)
                        dict_models_list = []
                except django_exceptions.ObjectDoesNotExist as err:
                    parse_stat['failed_num'] += 1
                    parse_stat['error_lines'].append((i + 1, str(err) + ': ' + str(entry)))
                    continue
        upload_task = CSEMeasurementsReportBasedUploader.upload.s(self, dict_models_list)
        upload_tasks.append(upload_task)
        g = group(*upload_tasks)
        remove(csv_file_name)


        return g()



@celery_app.task
def import_server_measurements_zabbix(run, metrics, servers):
    """Server: import measurements results from zabbix"""
    # Preparation
    upload_results = {'uploaded': 0,
                      'failed': 0,
                      'error_items': []}
    start_range = run.timestamp_start
    end_range = run.timestamp_end
    servers_names = [x.name for x in servers]
    item_names = [x.name for x in metrics]
    # Extract
    zbx_accessor = accessor.ZabbixAccessor()
    history = zbx_accessor.get_hosts_items_history(servers_names,
                                                   item_names,
                                                   start_range,
                                                   end_range)
    # Format and create
    for server in servers:
        try:
            for metric in metrics:
                for entry in history[server.name][metric.name]:
                    try:
                        with transaction.atomic():
                            models.ServerMeasurement.objects.create(metric=metric,
                                                                    server=server,
                                                                    timestamp=entry[0],
                                                                    value=entry[1],
                                                                    run=run)
                            upload_results['uploaded'] += 1
                    except (IntegrityError, DatabaseError) as db_exc:
                        upload_results['failed'] += 1
                        upload_results['error_items'].append(server.name + metric.name +
                                                             ': ' + str(db_exc))
        except KeyError as key_exc:
            upload_results['failed'] += 1
            upload_results['error_items'].append('No stat for item '
                                                 'on {server_name}: {exc}'.format(server_name=server.name,
                                                                                  exc=key_exc))
    return upload_results


@celery_app.task(name='fill_cases_for_measurements')
def fill_cases_for_measurements(run):
    update_results = []
    tc_accessor = tcaccessor.TestCaseAccessor()
    applicable_cases = tc_accessor.get_applicable_cases(run.tested_release.get_short_release_name(),
                                                        run.timestamp_start,
                                                        run.timestamp_end)
    # client side
    for case in applicable_cases:
        # currently supportet cases without general or account parameters
        try:
            tc, created = models.TestCase.objects.get_or_create(external_key=case.external_key,
                                                                version=case.version,
                                                                defaults={'title': case.title,
                                                                          'sla': case.sla,
                                                                          'priority': case.priority,
                                                                          'component': case.component})
            if case.parameters is None:
                matched_measurements = run.get_client_measurements().filter(metric__name=case.key_field,
                                                                            metric__component=tc.component,
                                                                            parameters='')
                with transaction.atomic():
                    res = matched_measurements.update(test_case=tc.id)
                    update_results.append((case.external_key, res))
                    logger.info('Case {tc} matched'.format(tc=' '.join((case.external_key, case.title))))
            else:
                logger.info('Case {tc} currently is not '
                            'supportedos it will be ignored'.format(tc=' '.join((case.external_key, case.title))))
        except Exception:
            continue
    return update_results


@celery_app.task(name='link_cases_with_metrics')
def link_cases_with_metrics(run):
    update_results = []
    tc_accessor = tcaccessor.TestCaseAccessor()
    applicable_cases = tc_accessor.get_applicable_cases(run.tested_release.get_short_release_name(),
                                                        run.timestamp_start,
                                                        run.timestamp_end)
    # client side
    for case in applicable_cases:
        try:
            # currently supportet cases without general or account parameters
            tc = models.TestCase.objects.get(external_key=case.external_key, version=case.version)
            if case.parameters is None:
                matched_metrics = run.get_tested_client_metrics(True).filter(name=case.key_field,
                                                                             component=tc.component)
                if matched_metrics:
                    with transaction.atomic():
                        res = matched_metrics[0].test_case.add(tc.id)
                        update_results.append((case.external_key, res))
            else:
                logger.info('Case {tc} currently is not '
                            'supportedos it will be ignored'.format(tc=' '.join((case.external_key, case.title))))
        except Exception:
            continue
    return update_results




class PerfFieldReportBasedUploader(BaseUploader):
    """Uploads tests from perf-field generated report"""
    parsed_fields = ['status', 'project', 'requester', 'branch', 'external_report_link','start_date','end_date','goal','jira_key','title','tags','section']

    def __init__(self, cleaned_data, *args, **kwargs):
        BaseUploader.__init__(self, reports_models.Test, cleaned_data)
        temp_file_name = self.get_temp_file_path(cleaned_data['data_file'])
        del self.cleaned_data['data_file']   # We need to delete, because file object cannot be serialized properly
        self.task = PerfFieldReportBasedUploader.parse(self, temp_file_name)

    def parse(self, csv_file_name):
        parse_stat = {'failed_num': 0,
                      'error_lines': [],
                      'uploaded_num': 0}
        with open(csv_file_name, 'rb') as csv_file:
            report_reader = utils.PerfFieldReportReader(TextIOWrapper(csv_file))
            for i, entry in report_reader.data:
                try:
                    status = reports_models.TestStatus.objects.get(name=entry['status'])
                    requester= usr_model.objects.get(username=entry['requester'].split("@")[0],email=entry['requester'])
                    branch = apps_models.Branch.objects.get(name=entry['branch'])
                    section = reports_models.Section.objects.get(name=entry['section'])
                    build = apps_models.Build.objects.get(version=entry['build'])
                    release = apps_models.Release.objects.get(main_build=build)
                    run, created = models.Run.objects.get_or_create(name=entry['title'], tested_release=release,
                                                                    timestamp_start=datetime.combine(
                                                                        entry['release_date'], datetime.min.time()),
                                                                    timestamp_end=datetime.combine(
                                                                        entry['release_date'], datetime.max.time()),
                                                                    environment=env_models.HardwareEnvironment.objects.get(
                                                                        name='Stress Stage'))
                    goal = 'The goal is to find performance bugs in the ' + release.name
                    report_link = settings.OLD_REPORTS_URL + '/' + entry['section'] + '/' + entry['id']
                    test, created, updated = reports_models.Test.extra_manager.update_or_create(jira_key= entry['id'],

                                                                                          defaults={
                                                                                                     'title': entry['title'],
                                                                                                     'goal': goal,
                                                                                                     'section': section,
                                                                                                     'main_run':run,
                                                                                                     'status': status,
                                                                                                     'requester': requester,
                                                                                                     'start_date': entry['release_date'],
                                                                                                     'end_date': entry['test_date'],
                                                                                                     'external_report_link': report_link,
                                                                                                     'test_plan_external_link':entry['idLink']})

                    for res in entry['tags'].split(','):
                        tag, created = reports_models.Tag.objects.get_or_create(name=res)
                        test.tags.add(tag)

                except django_exceptions.ObjectDoesNotExist as err:
                    parse_stat['failed_num'] += 1
                    parse_stat['error_lines'].append((i + 1, str(err) + ': ' + str(entry)))
                    continue
        return parse_stat

