from pytz import common_timezones

from django import forms

from . import models
from apps import models as app_models
from reports import models as report_models
from environments import models as env_models
from external_data.servermonitoring import accessor


class BaseMeasurementsUploadForm(forms.Form):
    data_file = forms.FileField(label='Data file', required=True)
    run = forms.ModelChoiceField(queryset=models.Run.objects.all())
    timezone = forms.ChoiceField(
        choices=[(x, x) for x in common_timezones],
        initial='UTC',
        required=True
    )

    def __init__(self, uploaders_choices, *args, **kwargs):
        super(BaseMeasurementsUploadForm, self).__init__(*args, **kwargs)
        self.fields['uploader'] = forms.ChoiceField(choices=uploaders_choices,
                                                    required=True)


class ClientMeasurementsUploadForm(BaseMeasurementsUploadForm):
    metric = forms.ModelChoiceField(queryset=models.ClientMetric.objects.all(),
                                    required=False)
    account = forms.IntegerField(required=False)

    def __init__(self, *args, **kwargs):
        super(ClientMeasurementsUploadForm, self).__init__(*args, **kwargs)
        self.fields['component'] = forms.ModelChoiceField(queryset=app_models.Component.objects.all(),
                                                          required=True)


class ServerMeasurementsUploadForm(BaseMeasurementsUploadForm):
    metric = forms.ModelChoiceField(queryset=models.ServerMetric.objects.all(),
                                    required=False)

    def __init__(self, *args, **kwargs):
        super(ServerMeasurementsUploadForm, self).__init__(*args, **kwargs)


class ClientMetricsSimpleUploadForm(forms.ModelForm):
    data_file = forms.FileField(label='Data file', required=True)

    class Meta:
        model = models.ClientMetric
        exclude = ['name',  'test_case']


class CSEMetricsUploadForm(ClientMetricsSimpleUploadForm):

    def __init__(self, uploader_choices, *args, **kwargs):
        super(CSEMetricsUploadForm, self).__init__(*args, **kwargs)
        self.fields['uploader'] = forms.ChoiceField(choices=uploader_choices,
                                                    required=True)

class CSEMeasurementsUploadForm(ClientMeasurementsUploadForm):

    def __init__(self, uploader_choices, *args, **kwargs):
        super(CSEMeasurementsUploadForm, self).__init__(*args, **kwargs)
        # TODO: move it to extra class
        self.fields['uploader'] = forms.ChoiceField(choices=uploader_choices,
                                                    required=True)


class ServerMetricsSimpleUploadForm(forms.ModelForm):
    data_file = forms.FileField(label='Data file', required=True)

    class Meta:
        model = models.ServerMetric
        exclude = ['name', 'unit' ,'test_case']


class ServerMetricsZabbixGetForm(forms.Form):
    component = forms.ModelChoiceField(app_models.Component.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(ServerMetricsZabbixGetForm, self).__init__(*args, **kwargs)
        servers = env_models.Server.objects.all().values_list('name', flat=True)
        # tuple() for json serialization
        servers = tuple(servers)
        imported_metrics = models.ServerMetric.objects.all().values_list('name',
                                                                         flat=True)
        zbx_accessor = accessor.ZabbixAccessor()
        all_possible_metrics = zbx_accessor.get_hosts_metric_names(servers)
        avail_metrics = list(set(all_possible_metrics) - set(imported_metrics))
        avail_metrics.sort()
        self.fields['metrics'] = forms.MultipleChoiceField(choices=zip(avail_metrics, avail_metrics))


class ServerMeasurementsZabbixGetForm(forms.Form):
    run = forms.ModelChoiceField(models.Run.objects.all())
    servers = forms.ModelMultipleChoiceField(env_models.Server.objects.all())
    metrics = forms.ModelMultipleChoiceField(models.ServerMetric.objects.all())


class RunSelectionForm(forms.Form):
    run = forms.ModelChoiceField(models.Run.objects.all())


class PerfFieldTestsUploadForm(forms.ModelForm):
    data_file = forms.FileField(label='Data file', required=True)

    def __init__(self, uploader_choices, *args, **kwargs):
        super(PerfFieldTestsUploadForm, self).__init__(*args, **kwargs)
        self.fields['uploader'] = forms.ChoiceField(choices=uploader_choices,
                                                    required=True)

    class Meta:
        model = report_models.Test
        exclude = ['jira_key', 'title', 'goal', 'conclusion', 'test_plan', 'test_plan_external_link',
                   'external_report_link', 'requester', 'template', 'status', 'related', 'main_run', 'runs',
                   'start_date', 'end_date','section']