import csv
from datetime import datetime


class CSEReportReader:

    def __init__(self, csv_file, delimiter=','):
        self.data = []
        self.error_lines = []
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        for i, line in enumerate(csv_reader):
            try:
                dict_line = {}
                date = line[0].strip()
                time = line[1].strip()
                timestamp = datetime.strptime(' '.join((date, time)),
                                              '%m/%d/%Y %H:%M:%S')
                dict_line['timestamp'] = timestamp
                dict_line['metric_name'] = line[2].strip()
                dict_line['metric_description'] = line[4].strip()
                dict_line['sla'] = line[11].strip()
                dict_line['tool_name'] = line[7].strip()
                dict_line['application_name'] = line[16].strip()
                dict_line['environment'] = line[14].strip()
                dict_line['account_number'] = line[15].strip()
                dict_line['priority'] = line[9].strip()
                dict_line['value'] = float(line[10])
                self.data.append((i, dict_line))
            except (ValueError, IndexError) as err:
                self.error_lines.append((i, str(err)))
                continue


class PerfFieldReportReader:

    def __init__(self, csv_file, delimiter=';'):
        self.data = []
        self.error_lines = []
        csv_reader = csv.reader(csv_file, delimiter=delimiter)
        for i, line in enumerate(csv_reader):
            try:
                dict_line = {}
                release_date = line[8].strip()
                test_date = line[11].strip()
                dict_line['section'] = line[0].strip()
                dict_line['id']= line[4].strip()
                dict_line['release_date'] = datetime.strptime(release_date, "%d %b %Y")
                dict_line['test_date'] = datetime.strptime(test_date, "%d %b %Y")
                dict_line['branch'] = line[1].strip()
                dict_line['idLink'] = line[5].strip()
                dict_line['tags'] = line[12].strip()
                dict_line['build'] = line[7].strip()
                dict_line['requester'] = line[2].strip()
                dict_line['status'] = line[10].strip()
                dict_line['title'] = line[14].strip()
                self.data.append((i, dict_line))
            except (ValueError, IndexError) as err:
                self.error_lines.append((i, str(err)))
                continue
