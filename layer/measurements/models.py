# Standard library
import datetime
import re
import time
import logging

logger = logging.getLogger(__name__)

# Django modules
from django.db import models, transaction
from django.db.models import Count
from django.core.urlresolvers import reverse
from django.utils.timezone import utc
from django.core import exceptions as django_exceptions
from django.core import cache as dj_cache
# 3rd party modules

from dbparti.models import Partitionable
# My modules
from . import managers
from apps import models as app_models
from environments import models as env_models
from external_data.testcases import accessor as tcaccessor
from utils import metric_filters


class TestCase(models.Model):
    """Represent Test Case from performance testing point of view"""
    external_key = models.CharField(max_length=120)
    version = models.CharField(max_length=120)
    title = models.CharField(max_length=500)
    sla = models.FloatField()
    priority = models.IntegerField()

    component = models.ForeignKey('apps.Component', blank=True, null=True)
    objects = models.Manager()
    cse_objects = managers.CSETestCasesManager()

    def __str__(self):
        return '{key}: {title} ({version})'.format(key=self.external_key,
                                                   title=self.title,
                                                   version=self.version)

    class Meta:
        unique_together = (('external_key', 'version'),)

    def get_testcase_issues(self):
        return Issue.objects.filter(test_case=self.id)

    def is_cse(self):
        return ClientMetric.objects.filter(test_case=self.id)[0].is_cse


class Flag(models.Model):
    name = models.CharField(max_length=60, unique=True)

    def __str__(self):
        return self.name


class Run(models.Model):
    """Run is logically uninterrupted set of load on particular system with particular load profile"""
    name = models.CharField(max_length=120)
    tested_release = models.ForeignKey('apps.Release')
    timestamp_start = models.DateTimeField()
    timestamp_end = models.DateTimeField()
    environment = models.ForeignKey('environments.HardwareEnvironment')
    flags = models.ManyToManyField(Flag, blank=True)

    def __unicode__(self):        # TODO - need this in Python 3
        return '%s %s' % (self.name, str(self.tested_release))

    def __str__(self):
        return "%s [%s]" % (self.name, str(self.tested_release))

    class Meta:
        ordering = ['-timestamp_end']

    def get_client_measurements(self):
        return ClientMeasurement.objects.filter(run=self.id)

    def get_server_measurements(self):
        return ServerMeasurement.objects.filter(run=self.id)

    # TODO: deprecated, need to delete
    def get_tested_environment(self):
        return self.environment

    def get_server_metrics(self):
        measurements = self.get_server_measurements()
        metrics = measurements.order_by('server__name', 'metric__name') \
            .distinct('server__name', 'metric__name')
        return metrics

    def get_tested_client_metrics(self, include_cse=False):
        measurements = self.get_client_measurements()
        metric_ids = measurements.order_by('metric__pk').distinct('metric__pk').values('metric__pk')
        tested_metrics = ClientMetric.objects.filter(pk__in=metric_ids)
        if include_cse:
            return tested_metrics
        else:
            return tested_metrics.filter(is_cse=False)


    def get_tested_high_priority_metrics(self, include_cse=False):
        tested_metrics = self.get_tested_client_metrics(include_cse)
        return tested_metrics.filter(test_case__priority=1)


    def get_run_issues(self):
        return Issue.objects.filter(run=self.id)

    def get_sla_violations_cases(self):
        testcases = []
        statistics = ClientMeasurement.analyzes.get_stat(run=self, grouper='test_case')
        for stat_entry in statistics:
            try:
                # FIXME: error with duplicate testcase titles
                testcase = TestCase.objects.get(pk=stat_entry['entity'])
                if stat_entry['median'] > testcase.sla:
                    testcases.append(testcase)
            except Exception:
                continue
        return testcases


    def get_sla_violations_stat(self):
        statistics = ClientMeasurement.analyzes.get_stat(run=self, grouper='test_case')
        violated_statistcs = []
        for stat_entry in statistics:
            testcase = TestCase.objects.get(id=stat_entry['entity'])
            if stat_entry['median'] > testcase.sla:
                stat_entry['entity'] = testcase
                violated_statistcs.append(stat_entry)
        return violated_statistcs


    def get_sla_violations_metrics(self):
        statistics = ClientMeasurement.analyzes.get_stat(run=self, grouper='metric__id', metric__is_cse=False)
        violated_statistcs = []
        for stat_entry in statistics:
            try:
                testcase = TestCase.objects.get(clientmetric__id=stat_entry['entity'])
                if testcase:
                    if stat_entry['95p'] > testcase.sla:
                        metric = ClientMetric.objects.get(id=stat_entry['entity'])
                        violated_statistcs.append(metric)
            except Exception:
                continue
        return violated_statistcs


    def get_decorated_sla_violations_table(self, **kwargs):
        statistics = ClientMeasurement.analyzes.get_stat(run=self, grouper='test_case', **kwargs)
        violated_statistics = []
        for stat_entry in statistics:
            try:
                testcase = TestCase.objects.get(id=stat_entry['entity'])
                if stat_entry['median'] > testcase.sla:
                    stat_entry['sla'] = testcase.sla
                    stat_entry['priority'] = testcase.priority
                    stat_entry['component'] = testcase.component.name
                    stat_entry['tc_name'] = testcase.external_key
                    stat_entry['tc_title'] = testcase.title
                    stat_entry['offender'] = stat_entry['median'] - testcase.sla
                    violated_statistics.append(stat_entry)
            except Exception:
                continue
        return violated_statistics

    def get_executed_cases(self, cse_only=False):
        # key = str(self.id)+ '_get_executed_testcases_' + str(cse_only)
        # cache = dj_cache.get_cache('default')
        # cached_result = cache.get(key)
        # if cached_result is None:
        executed_cases_ids = ClientMeasurement.objects.filter(run=self.id).values('test_case').distinct()
        objects = TestCase.objects.filter(pk__in=executed_cases_ids)
        # cache.set(key,objects)
        return objects
        # else:
        # return cached_result

    def get_applicable_testcases(self, cse_only=False, components=None):
        # key = str(self.id)+ '_get_applicable_testcases_' + str(cse_only) + re.sub(r'[^a-zA-Z0-9]','', str(components))
        # cache = dj_cache.get_cache('default')
        # cached_result = cache.get(key)
        # if cached_result is None:


        tc_accessor = tcaccessor.TestCaseAccessor()
        applicable_cases_list = tc_accessor.get_applicable_cases(self.tested_release.get_short_release_name(),
                                                                     self.timestamp_start,
                                                                     self.timestamp_end, components)
        return applicable_cases_list
        # else:
        # return cached_result

    def get_coverage_stat_by_app(self, cse_only=False, components=None):
        # http://stackoverflow.com/questions/20686105/django-join-multiple-annotated-querysets
        # https://docs.djangoproject.com/en/dev/ref/models/querysets/#django.db.models.query.QuerySet.extra
        key = str(self.id)
        cache = dj_cache.get_cache('default')
        cached_result = cache.get(key + '_coverage_stat_by_app_' + str(cse_only))
        if cached_result is None:
            start_time = time.time()
            tc_accessor = tcaccessor.TestCaseAccessor()
            count_cases_sql = """select count(distinct measurements_testcase.id)
                                       from measurements_testcase
                                       where measurements_testcase.id in ({tc_ids})
                                       and apps_component.id = measurements_testcase.component_id
                                       """

            count_cse_cases_sql = """select count(distinct measurements_testcase.id)
                                       from measurements_testcase
                                       join measurements_clientmetric_test_case
                                       on measurements_clientmetric_test_case.testcase_id = measurements_testcase.id
                                       join measurements_clientmetric
                                       on measurements_clientmetric.id = measurements_clientmetric_test_case.clientmetric_id
                                       where measurements_testcase.id in ({tc_ids})
                                       and measurements_clientmetric.is_cse = true
                                       and apps_component.id = measurements_testcase.component_id
                                       """

            def get_cases_count_sql_from_tc_objects(tc_objects_list):
                cases_ids = [TestCase.objects.get_or_create(external_key=x.external_key,
                                                            version=x.version,
                                                            defaults={'title': x.title,
                                                                      'sla': x.sla,
                                                                      'priority': x.priority,
                                                                      'component': x.component})[0].id
                             for x in tc_objects_list]
                return count_cases_sql.format(tc_ids=','.join(str(x) for x in cases_ids)) if cases_ids else 0

            # applicable cases selecting
            applicable_cases_list = tc_accessor.get_applicable_cases(self.tested_release.get_short_release_name(),
                                                                                                         self.timestamp_start,
                                                                                                         self.timestamp_end, components)

            applicable_cases_sql = get_cases_count_sql_from_tc_objects(applicable_cases_list)
            # new cases selecting
            new_cases_list = tc_accessor.get_new_cases(self.tested_release.get_short_release_name(),
                                                       self.timestamp_start,
                                                       self.timestamp_end)
            new_cases_sql = get_cases_count_sql_from_tc_objects(new_cases_list)
            # deprecated cases selection
            deprecated_cases_list = tc_accessor.get_deprecated_cases(self.tested_release.get_short_release_name(),
                                                                     self.timestamp_start,
                                                                     self.timestamp_end)
            deprecated_cases_sql = get_cases_count_sql_from_tc_objects(deprecated_cases_list)
            # stats compilation

            if cse_only is False:
                apps_extra = app_models.Component.objects.filter(pk__in=[x.id for x in components]).extra(
                    select_params=[str(self.id)], select={
                        'total_cases': """select count(measurements_testcase.id)
                                  from measurements_testcase
                                  where apps_component.id = measurements_testcase.component_id""",
                        'executed_cases': """select count(distinct measurements_testcase.id)
                                     from measurements_testcase
                                     join measurements_clientmeasurement
                                          on measurements_clientmeasurement.test_case_id = measurements_testcase.id
                                     where
                                     apps_component.id = measurements_testcase.component_id
                                     and
                                     measurements_clientmeasurement.run_id = %s""",
                        'applicable_cases': applicable_cases_sql,
                        'new_cases': new_cases_sql,
                        'deprecated_cases': deprecated_cases_sql}).values_list('name', 'total_cases', 'executed_cases',
                                                                               'applicable_cases', 'new_cases',
                                                                               'deprecated_cases')
            else:
                total_by_priority = """select count(measurements_testcase.id)
                                  from measurements_testcase
                                  join measurements_clientmetric_test_case
                                  on measurements_clientmetric_test_case.testcase_id = measurements_testcase.id
                                  join measurements_clientmetric
                                  on measurements_clientmetric.id = measurements_clientmetric_test_case.clientmetric_id
                                  where apps_component.id = measurements_testcase.component_id
                                  and measurements_clientmetric.is_cse=true and measurements_testcase.priority = %d """
                executed_by_priority = """select count(distinct measurements_testcase.id)
                                     from measurements_clientmeasurement
                                     join measurements_testcase
                                     on measurements_clientmeasurement.test_case_id = measurements_testcase.id
                                     join measurements_clientmetric
                                     on measurements_clientmetric.id = measurements_clientmeasurement.metric_id
                                     where
                                     apps_component.id = measurements_testcase.component_id
                                     and
                                     measurements_clientmetric.is_cse=true
                                     and
                                     measurements_clientmeasurement.run_id = %s
                                     and measurements_testcase.priority = %d """
                offenders_by_priority = """select count(*) from (
                                        select test_case_id,run_id,sla,priority,measurements_testcase.component_id,avg(value) as result
                                        from measurements_clientmeasurement
                                        join measurements_testcase
                                        on measurements_clientmeasurement.test_case_id = measurements_testcase.id
                                        join measurements_clientmetric
                                        on measurements_clientmetric.id = measurements_clientmeasurement.metric_id
                                        group by test_case_id,priority,run_id,sla,measurements_clientmetric.is_cse,measurements_testcase.component_id
                                        having run_id = %s  and (priority = %s or priority = %s)
                                        and apps_component.id = measurements_testcase.component_id
                                        and measurements_clientmetric.is_cse=true) as t1
                                        where t1.result/t1.sla > %s """
                apps_extra = app_models.Component.objects.filter(description__contains='(Client side)').extra(select={
                    'high_priority_total': total_by_priority % 2,
                    'normal_priority_total': total_by_priority % 3,
                    'high_priority_tested': executed_by_priority % (str(self.id), 2),
                    'normal_priority_tested': executed_by_priority % (str(self.id), 3),
                    'p1_offenders': offenders_by_priority % (str(self.id), '2', '2', '2.5'),
                    'p2_offenders': offenders_by_priority % (str(self.id), '2', '3', '1'),
                    'applicable_cases': applicable_cases_sql,
                    'deprecated_cases': deprecated_cases_sql}).values_list('name', 'high_priority_total',
                                                                           'normal_priority_total',
                                                                           'high_priority_tested',
                                                                           'normal_priority_tested', 'p1_offenders',
                                                                           'p2_offenders', 'applicable_cases',
                                                                           'deprecated_cases')
            logger.info('Coverage calculated for run: "{name}" in {n} seconds'.format(name=self.name,
                                                                                      n=time.time() - start_time))
            cache.set(key + '_coverage_stat_by_app_' + str(cse_only), apps_extra)
            return apps_extra
        else:
            return cached_result

    def get_cse_new_cases(self):
        tc_accessor = tcaccessor.TestCaseAccessor()
        new_cases_list = tc_accessor.get_new_cases(self.tested_release.get_short_release_name(),
                                                   self.timestamp_start,
                                                   self.timestamp_end)

        def get_new_cases_with_results(tc_objects_list):
            result_sql = """select avg(value) from measurements_clientmeasurement
            where measurements_clientmeasurement.test_case_id = %d
             and measurements_clientmeasurement.run_id = %d """
            cases_ids = [TestCase.objects.get_or_create(external_key=x.external_key,
                                                        version=x.version,
                                                        defaults={'title': x.title,
                                                                  'sla': x.sla,
                                                                  'priority': x.priority,
                                                                  'component': x.component})[0].id
                         for x in tc_objects_list]
            if cases_ids:
                return [TestCase.cse_objects.filter(id=str(case_id)).extra(select_params=[str(case_id)], select={
                    'description': """select measurements_clientmetric.description
                              from measurements_clientmetric
                              join measurements_clientmetric_test_case
                              on measurements_clientmetric.id = measurements_clientmetric_test_case.clientmetric_id
                              join measurements_testcase
                              on measurements_clientmetric_test_case.testcase_id = measurements_testcase.id
                              where measurements_testcase.id = %s """,
                    'result': result_sql % (case_id, self.id)})
                        for case_id in cases_ids]

        cases_extra = get_new_cases_with_results(new_cases_list)
        return cases_extra

    def get_load_profile(self):
        cache = dj_cache.get_cache('default')
        key = str(self.id) + 'load_profile'
        cached_result = cache.get(key)
        if cached_result is None:
            profile_queryset = self.get_client_measurements().filter(metric__is_cse=False).values('metric__component') \
                .annotate(meas_count=Count('metric__component'))
            seconds = (self.timestamp_end - self.timestamp_start).total_seconds()
            profile = []
            for component in profile_queryset:
                app_load = {'component': app_models.Component.objects.get(pk=component['metric__component']),
                            'rate': component['meas_count'] / seconds}
                profile.append(app_load)
            cache.set(key, profile)
            return profile
        else:
            return cached_result


    def get_loaded_servers(self, server_model=env_models.Server, metric_model=None):
        if metric_model == None:
            metric_model = ServerMetric
        servers_ids = self.get_server_measurements().values('server').distinct()
        servers = server_model.objects.filter(id__in=servers_ids)
        for server in servers:
            metric_ids = self.get_server_measurements().filter(server=server).values('metric').distinct()
            metrics = metric_model.objects.filter(id__in=metric_ids)
            server.available_metrics = metrics
        return servers

    def get_participated_servers(self):
        servers = self.environment.get_actual_servers(self.timestamp_end)
        for server in servers:
            metric_ids = self.get_server_measurements().filter(server=server).values('metric')
            metrics = ServerMetric.objects.filter(id__in=metric_ids)
            server.available_metrics = metrics
        return servers

    def _get_available_metrics_for_server(self, server_id):
        measurements_for_server = self.get_server_measurements().filter(server_id=server_id)
        available_metrics = ServerMetric.objects.filter(pk__in=measurements_for_server.values('metric').distinct())
        return available_metrics


class MetricFamily(models.Model):
    name = models.CharField(max_length=120, unique=True)
    unit = models.CharField(max_length=120, blank=True, default='')
    description = models.CharField(max_length=120, blank=True, default='')

    def __str__(self):
        return self.name


class CommonMetric(models.Model):
    """Abstract metric class, which contains logic, which is similar for client and server metrics"""
    name = models.CharField(max_length=120, unique=True)
    unit = models.CharField(max_length=120, blank=True, default='')
    family_ref = models.ForeignKey(MetricFamily, null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=120, blank=True, default='')
    test_case = models.ManyToManyField(TestCase, blank=True, default='')

    class Meta:
        abstract = True

    @property
    def family(self):
        return self.family_ref.name if self.family_ref else ''

    def get_family(self):
        return self.family_ref.name if self.family_ref else self.name

    def get_unit(self):
        #fam_unit = self.family_ref.unit if self.family_ref else ''
        return self.unit if self.unit \
            else (self.family_ref.unit if self.family_ref else '')

    def get_chart_settings_url(self):
        if self.family_ref:
            return reverse('measurements:server_metric_family_chart_settings',
                           kwargs={'family': self.family_ref.name})
        else:
            return reverse('measurements:server_metric_chart_settings',
                           kwargs={'metric_id': self.id})

    def is_client(self):
        return True

    def get_metric_transform(self, page_id):
        kw_criteria = {}
        if self.family_ref:
            kw_criteria['metric_family'] = self.family_ref
        elif not self.is_client():
            kw_criteria['server_metric'] = self.id
        else:
            raise Exception('No transform for client metric '+ self.name)
        for page_key in [page_id, '_default_']:
            for metric_trf in MetricTransform.objects.filter(page_id=page_key, **kw_criteria): # 0 or 1 row
                return metric_trf
        return MetricTransform.dummy


class CommonMeasurement(models.Model):
    """Measurement is """
    timestamp = models.DateTimeField()
    value = models.FloatField()
    run = models.ForeignKey(Run)
    return_code = models.CharField(max_length=120, blank=True, default='')
    parameters = models.CharField(max_length=500, blank=True, default='')
    account = models.CharField(max_length=120, blank=True, default='')
    test_case = models.ForeignKey(TestCase, null=True, blank=True, on_delete=models.SET_NULL)

    objects = models.Manager()
    real_measurements = managers.RealRunsManager()
    analyzes = managers.MeasureStatManager()

    def clean(self):
        super(CommonMeasurement, self).clean()
        if self.timestamp < self.run.timestamp_start or self.timestamp > self.run.timestamp_end:
            raise django_exceptions.ValidationError('Measurement is outside the run')

    class Meta:
        abstract = True
        ordering = ['-timestamp']

    def get_parameters_dict(self):
        if self.parameters:
            return dict(x.split('=') for x in self.parameters.split('::'))
        else:
            return {}

    def get_account_parameters_dict(self):
        # TODO: to be implemented
        return None


class ClientMeasurementType(models.Model):
    """Measurement type is a way how measurement performed"""
    name = models.CharField(max_length=120, unique=True)
    description = models.CharField(max_length=300, blank=True, null=True)
    tool = models.CharField(max_length=120, blank=True, null=True)

    def __str__(self):
        return self.name


class ClientMetric(CommonMetric):
    component = models.ForeignKey('apps.Component')
    measure_type = models.ForeignKey(ClientMeasurementType)
    is_cse = models.BooleanField(default=False)

    class Meta:
        unique_together = (('component', 'name', 'measure_type'),)

    def __str__(self):
        return '%s - %s' % (self.component.name, self.name)

    def get_metric_class(self):
        if self.name.split('.')[0] is not '':
            a = self.name.split('.')[0]
            return a
        else:
            return 'some_class'


class ClientMeasurement(CommonMeasurement, Partitionable):
    """ClientMeasurement is a measurement performed on the client side.

    Usually it includes"""
    metric = models.ForeignKey(ClientMetric)
    environment = models.ForeignKey('environments.ClientEnvironment', blank=True, null=True)

    class Meta:
        # It can't be on the same time, same client  metric on the same stage
        # We consider that as error
        unique_together = (("metric", "timestamp", "run", "account", "return_code", "parameters"),)
        partition_type = 'range'
        partition_subtype = 'date'
        partition_range = 'month'
        partition_column = 'timestamp'

    def __str__(self):
        return ' %s=%s' % (self.timestamp,
                           self.value)


# Server side measurements
class ServerMetric(CommonMetric):
    component = models.ForeignKey('apps.Component', null=True, blank=True)

    class Meta:
        unique_together = (('component', 'name'),)

    def __str__(self):
        return self.name

    def is_client(self):
        return False


class ServerMeasurement(CommonMeasurement):
    metric = models.ForeignKey(ServerMetric)
    server = models.ForeignKey('environments.Server')

    class Meta:
        unique_together = (("metric", "server", "timestamp", "run"),)

    def __str__(self):
        return '{metric}: {timestamp}={value} at {server}'.format(metric=self.metric,
                                                                  timestamp=self.timestamp,
                                                                  value=self.value,
                                                                  server=self.server)


class MetricTransform(models.Model):
    page_id = models.CharField(max_length=120, default='_default_')
    metric_family = models.ForeignKey(MetricFamily, blank=True, null=True)
    server_metric = models.ForeignKey(ServerMetric, blank=True, null=True)
#    client_metric = models.ForeignKey(ClientMetric, null=True)
    unit = models.CharField(max_length=60)
    filter = models.CharField(max_length=120, blank=True, choices=metric_filters.get_filter_choices())
    multiply = models.FloatField(blank=True, null=True)
    round_digits = models.IntegerField(default=0)

    dummy = None # keep dummy transform instance for processing non-transformed metrics; see CommonMetric.get_metric_transform()

    class Meta:
        unique_together = (('page_id', 'metric_family', 'server_metric'))

    @staticmethod
    def get_dummy_metric_transform(page_id='dummy'):
        row = MetricTransform()
        row.page_id = page_id
        row.metric_family = None
        row.server_metric = None
#        row.client_metric = None
        row.unit   = ''
        row.filter  = ''
        row.multiply     = 0.0
        row.round_digits = 0
        return row

    def get_filter_suffix(self):
        round_suffix = ("&round_digits=%s" % self.round_digits) if self.round_digits else ''
        filter_name = self.filter.strip()
        if filter_name:
            return "filter=%s%s" % (filter_name, round_suffix)
        if self.multiply:
            return "multiply_by=%s%s" % (self.multiply, round_suffix)
        return ""

MetricTransform.dummy = MetricTransform.get_dummy_metric_transform()

class Account(models.Model):
    user_id = models.BigIntegerField()
    phone_number = models.IntegerField()
    environment = models.ForeignKey('environments.HardwareEnvironment')
    deletion_timestamp = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = (('phone_number', 'environment', 'deletion_timestamp'),)

    def __str__(self):
        return self.phone_number

    def get_property(self, timestamp=None):
        if timestamp is None:
            timestamp = datetime.datetime.utcnow().replace(tzinfo=utc)
        # PostgreSQL only
        return AccountProperty.objects.filter(account=self.id,
                                              timestamp__lte=timestamp) \
            .order_by('name', '-timestamp').distinct('name')


class AccountProperty(models.Model):
    name = models.CharField(max_length=120)
    value = models.CharField(max_length=120)
    timestamp = models.DateTimeField()
    account = models.ForeignKey(Account)

    def __str__(self):
        return self.name


class Issue(models.Model):
    jira_key = models.CharField(max_length=60, unique=True)
    title = models.CharField(max_length=120)
    run = models.ManyToManyField(Run)
    test_case = models.ForeignKey(TestCase, blank=True, null=True)
    priority = models.CharField(max_length=12, blank=True)
    furps = models.CharField(max_length=60, blank=True)

    def __str__(self):
        return "%s: %s" % (self.jira_key, self.title)
