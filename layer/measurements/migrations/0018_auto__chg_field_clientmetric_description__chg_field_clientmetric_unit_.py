# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models, transaction


class Migration(SchemaMigration):

    def forwards(self, orm):

        with transaction.atomic():
            for cm in orm.ClientMetric.objects.filter(unit=None):
                cm.unit = ''
                cm.save()

            for cm in orm.ServerMeasurement.objects.filter(account=None):
                cm.account = ''
                cm.save()

            for cm in orm.ServerMeasurement.objects.filter(return_code=None):
                cm.return_code = ''
                cm.save()

            for cm in orm.ClientMeasurement.objects.filter(account=None):
                cm.account = ''
                cm.save()

            for cm in orm.ClientMeasurement.objects.filter(return_code=None):
                cm.return_code = ''
                cm.save()

        # Changing field 'ClientMetric.description'
        db.alter_column('measurements_clientmetric', 'description', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ClientMetric.unit'
        db.alter_column('measurements_clientmetric', 'unit', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ClientMetric.family'
        db.alter_column('measurements_clientmetric', 'family', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ServerMeasurement.account'
        db.alter_column('measurements_servermeasurement', 'account', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ServerMeasurement.return_code'
        db.alter_column('measurements_servermeasurement', 'return_code', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ServerMeasurement.parameters'
        db.alter_column('measurements_servermeasurement', 'parameters', self.gf('django.db.models.fields.CharField')(max_length=500))

        # Changing field 'ClientMeasurement.account'
        db.alter_column('measurements_clientmeasurement', 'account', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ClientMeasurement.return_code'
        db.alter_column('measurements_clientmeasurement', 'return_code', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ClientMeasurement.parameters'
        db.alter_column('measurements_clientmeasurement', 'parameters', self.gf('django.db.models.fields.CharField')(max_length=500))

        # Changing field 'ServerMetric.description'
        db.alter_column('measurements_servermetric', 'description', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ServerMetric.unit'
        db.alter_column('measurements_servermetric', 'unit', self.gf('django.db.models.fields.CharField')(max_length=120))

        # Changing field 'ServerMetric.family'
        db.alter_column('measurements_servermetric', 'family', self.gf('django.db.models.fields.CharField')(max_length=120))

    def backwards(self, orm):

        # Changing field 'ClientMetric.description'
        db.alter_column('measurements_clientmetric', 'description', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ClientMetric.unit'
        db.alter_column('measurements_clientmetric', 'unit', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ClientMetric.family'
        db.alter_column('measurements_clientmetric', 'family', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ServerMeasurement.account'
        db.alter_column('measurements_servermeasurement', 'account', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ServerMeasurement.return_code'
        db.alter_column('measurements_servermeasurement', 'return_code', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ServerMeasurement.parameters'
        db.alter_column('measurements_servermeasurement', 'parameters', self.gf('django.db.models.fields.CharField')(max_length=500, null=True))

        # Changing field 'ClientMeasurement.account'
        db.alter_column('measurements_clientmeasurement', 'account', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ClientMeasurement.return_code'
        db.alter_column('measurements_clientmeasurement', 'return_code', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ClientMeasurement.parameters'
        db.alter_column('measurements_clientmeasurement', 'parameters', self.gf('django.db.models.fields.CharField')(max_length=500, null=True))

        # Changing field 'ServerMetric.description'
        db.alter_column('measurements_servermetric', 'description', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ServerMetric.unit'
        db.alter_column('measurements_servermetric', 'unit', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        # Changing field 'ServerMetric.family'
        db.alter_column('measurements_servermetric', 'family', self.gf('django.db.models.fields.CharField')(max_length=120, null=True))

        with transaction.atomic():
            for cm in orm.ClientMetric.objects.filter(unit=''):
                cm.unit = None
                cm.save()

            for cm in orm.ServerMeasurement.objects.filter(account=''):
                cm.account = None
                cm.save()

            for cm in orm.ServerMeasurement.objects.filter(return_code=''):
                cm.return_code = None
                cm.save()

            for cm in orm.ClientMeasurement.objects.filter(account=''):
                cm.account = None
                cm.save()

            for cm in orm.ClientMeasurement.objects.filter(return_code=''):
                cm.return_code = None
                cm.save()

    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.AppGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['apps.Vendor']"})
        },
        'apps.branch': {
            'Meta': {'object_name': 'Branch', 'ordering': "['-date_created']"},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'object_name': 'Build', 'unique_together': "(('version', 'app_group'),)", 'ordering': "['-build_date', 'version']"},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.releasegroup': {
            'Meta': {'object_name': 'ReleaseGroup'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_build'", 'to': "orm['apps.Build']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'symmetrical': 'False', 'blank': 'True', 'to': "orm['apps.Build']"})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        'environments.server': {
            'Meta': {'object_name': 'Server', 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)", 'ordering': "['name']"},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'symmetrical': 'False', 'blank': 'True', 'to': "orm['apps.App']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        'measurements.account': {
            'Meta': {'object_name': 'Account', 'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)"},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'object_name': 'ClientMeasurement', 'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)"},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['environments.ClientEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'default': "''", 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'tool': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        'measurements.clientmetric': {
            'Meta': {'object_name': 'ClientMetric', 'unique_together': "(('component', 'name', 'measure_type'),)"},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'})
        },
        'measurements.run': {
            'Meta': {'object_name': 'Run', 'ordering': "['-timestamp_end']"},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'blank': 'True', 'to': "orm['measurements.Flag']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.ReleaseGroup']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'object_name': 'ServerMeasurement', 'unique_together': "(('metric', 'server', 'timestamp', 'run'),)"},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'default': "''", 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'object_name': 'ServerMetric', 'unique_together': "(('component', 'name'),)"},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'})
        },
        'measurements.testcase': {
            'Meta': {'object_name': 'TestCase', 'unique_together': "(('external_key', 'version'),)"},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['apps.App']"}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']