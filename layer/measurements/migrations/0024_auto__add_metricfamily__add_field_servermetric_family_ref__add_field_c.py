# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetricFamily'
        db.create_table('measurements_metricfamily', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('unit', self.gf('django.db.models.fields.CharField')(blank=True, default='', max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(blank=True, default='', max_length=120)),
        ))
        db.send_create_signal('measurements', ['MetricFamily'])

        # Adding field 'ServerMetric.family_ref'
        db.add_column('measurements_servermetric', 'family_ref',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.MetricFamily'], on_delete=models.SET_NULL, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ClientMetric.family_ref'
        db.add_column('measurements_clientmetric', 'family_ref',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.MetricFamily'], on_delete=models.SET_NULL, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'MetricFamily'
        db.delete_table('measurements_metricfamily')

        # Deleting field 'ServerMetric.family_ref'
        db.delete_column('measurements_servermetric', 'family_ref_id')

        # Deleting field 'ClientMetric.family_ref'
        db.delete_column('measurements_clientmetric', 'family_ref_id')


    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Vendor']", 'null': 'True'})
        },
        'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app'),)", 'object_name': 'Build'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['apps.Build']", 'null': 'True', 'symmetrical': 'False', 'blank': 'True'})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        'environments.server': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)", 'object_name': 'Server'},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_components': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['apps.Component']", 'null': 'True', 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'blank': 'True', 'null': 'True', 'max_length': '120'})
        },
        'measurements.account': {
            'Meta': {'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)", 'object_name': 'Account'},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)", 'object_name': 'ClientMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.ClientEnvironment']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '500'}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL', 'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'null': 'True', 'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'tool': ('django.db.models.fields.CharField', [], {'blank': 'True', 'null': 'True', 'max_length': '120'})
        },
        'measurements.clientmetric': {
            'Meta': {'unique_together': "(('component', 'name', 'measure_type'),)", 'object_name': 'ClientMetric'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.TestCase']", 'blank': 'True', 'default': "''", 'symmetrical': 'False'}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'})
        },
        'measurements.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'priority': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '12'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Run']", 'symmetrical': 'False'}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.metricfamily': {
            'Meta': {'object_name': 'MetricFamily'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'})
        },
        'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Flag']", 'blank': 'True', 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'unique_together': "(('metric', 'server', 'timestamp', 'run'),)", 'object_name': 'ServerMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '500'}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL', 'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'unique_together': "(('component', 'name'),)", 'object_name': 'ServerMetric'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.TestCase']", 'blank': 'True', 'default': "''", 'symmetrical': 'False'}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'default': "''", 'max_length': '120'})
        },
        'measurements.testcase': {
            'Meta': {'unique_together': "(('external_key', 'version'),)", 'object_name': 'TestCase'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']", 'null': 'True', 'blank': 'True'}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']