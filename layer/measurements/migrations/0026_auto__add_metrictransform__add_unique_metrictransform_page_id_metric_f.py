# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetricTransform'
        db.create_table('measurements_metrictransform', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page_id', self.gf('django.db.models.fields.CharField')(max_length=120, default='_default_')),
            ('metric_family', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['measurements.MetricFamily'])),
            ('server_metric', self.gf('django.db.models.fields.related.ForeignKey')(null=True, to=orm['measurements.ServerMetric'])),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('filter', self.gf('django.db.models.fields.CharField')(max_length=120, blank=True)),
            ('multiply', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('round_digits', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('measurements', ['MetricTransform'])

        # Adding unique constraint on 'MetricTransform', fields ['page_id', 'metric_family', 'server_metric']
        db.create_unique('measurements_metrictransform', ['page_id', 'metric_family_id', 'server_metric_id'])

        # Deleting field 'ClientMetric.family'
        db.delete_column('measurements_clientmetric', 'family')

        # Deleting field 'ServerMetric.family'
        db.delete_column('measurements_servermetric', 'family')


    def backwards(self, orm):
        # Removing unique constraint on 'MetricTransform', fields ['page_id', 'metric_family', 'server_metric']
        db.delete_unique('measurements_metrictransform', ['page_id', 'metric_family_id', 'server_metric_id'])

        # Deleting model 'MetricTransform'
        db.delete_table('measurements_metrictransform')

        # Adding field 'ClientMetric.family'
        db.add_column('measurements_clientmetric', 'family',
                      self.gf('django.db.models.fields.CharField')(max_length=120, blank=True, default=''),
                      keep_default=False)

        # Adding field 'ServerMetric.family'
        db.add_column('measurements_servermetric', 'family',
                      self.gf('django.db.models.fields.CharField')(max_length=120, blank=True, default=''),
                      keep_default=False)


    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['apps.Vendor']"})
        },
        'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'object_name': 'Build', 'unique_together': "(('version', 'app'),)"},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Build']", 'symmetrical': 'False'})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.server': {
            'Meta': {'ordering': "['name']", 'object_name': 'Server', 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)"},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_components': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '120'})
        },
        'measurements.account': {
            'Meta': {'object_name': 'Account', 'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)"},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'object_name': 'ClientMeasurement', 'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)"},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['environments.ClientEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'tool': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '120'})
        },
        'measurements.clientmetric': {
            'Meta': {'object_name': 'ClientMetric', 'unique_together': "(('component', 'name', 'measure_type'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.TestCase']", 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'})
        },
        'measurements.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Run']", 'symmetrical': 'False'}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.TestCase']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.metricfamily': {
            'Meta': {'object_name': 'MetricFamily'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"})
        },
        'measurements.metrictransform': {
            'Meta': {'object_name': 'MetricTransform', 'unique_together': "(('page_id', 'metric_family', 'server_metric'),)"},
            'filter': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric_family': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['measurements.MetricFamily']"}),
            'multiply': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'page_id': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "'_default_'"}),
            'round_digits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'server_metric': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['measurements.ServerMetric']"}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.Flag']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'object_name': 'ServerMeasurement', 'unique_together': "(('metric', 'server', 'timestamp', 'run'),)"},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'object_name': 'ServerMetric', 'unique_together': "(('component', 'name'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'to': "orm['measurements.TestCase']", 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True', 'default': "''"})
        },
        'measurements.testcase': {
            'Meta': {'object_name': 'TestCase', 'unique_together': "(('external_key', 'version'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'null': 'True', 'to': "orm['apps.Component']"}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']