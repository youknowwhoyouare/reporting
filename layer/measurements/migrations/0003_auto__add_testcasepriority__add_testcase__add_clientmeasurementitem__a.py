# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TestCasePriority'
        db.create_table(u'measurements_testcasepriority', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal(u'measurements', ['TestCasePriority'])

        # Adding model 'TestCase'
        db.create_table(u'measurements_testcase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('external_key', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('summary', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('SLA', self.gf('django.db.models.fields.IntegerField')()),
            ('priority', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.TestCasePriority'])),
        ))
        db.send_create_signal(u'measurements', ['TestCase'])

        # Adding model 'ClientMeasurementItem'
        db.create_table(u'measurements_clientmeasurementitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal(u'measurements', ['ClientMeasurementItem'])

        # Adding field 'ClientMeasurementType.tool'
        db.add_column(u'measurements_clientmeasurementtype', 'tool',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServerMeasurement.return_code'
        db.add_column(u'measurements_servermeasurement', 'return_code',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServerMeasurement.parameters'
        db.add_column(u'measurements_servermeasurement', 'parameters',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ClientMetric.description'
        db.add_column(u'measurements_clientmetric', 'description',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ClientMetric.item'
        db.add_column(u'measurements_clientmetric', 'item',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.ClientMeasurementItem'], null=True, blank=True),
                      keep_default=False)


        # Changing field 'Run.tested_build'
        # commented to fix full migration
        #db.alter_column(u'measurements_run', 'tested_build_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Release']))
        # Adding field 'ClientMeasurement.return_code'
        db.add_column(u'measurements_clientmeasurement', 'return_code',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ClientMeasurement.parameters'
        db.add_column(u'measurements_clientmeasurement', 'parameters',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ServerMetric.description'
        db.add_column(u'measurements_servermetric', 'description',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'TestCasePriority'
        db.delete_table(u'measurements_testcasepriority')

        # Deleting model 'TestCase'
        db.delete_table(u'measurements_testcase')

        # Deleting model 'ClientMeasurementItem'
        db.delete_table(u'measurements_clientmeasurementitem')

        # Deleting field 'ClientMeasurementType.tool'
        db.delete_column(u'measurements_clientmeasurementtype', 'tool')

        # Deleting field 'ServerMeasurement.return_code'
        db.delete_column(u'measurements_servermeasurement', 'return_code')

        # Deleting field 'ServerMeasurement.parameters'
        db.delete_column(u'measurements_servermeasurement', 'parameters')

        # Deleting field 'ClientMetric.description'
        db.delete_column(u'measurements_clientmetric', 'description')

        # Deleting field 'ClientMetric.item'
        db.delete_column(u'measurements_clientmetric', 'item_id')


        # Changing field 'Run.tested_build'
        #db.alter_column(u'measurements_run', 'tested_build_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Build']))
        # Deleting field 'ClientMeasurement.return_code'
        db.delete_column(u'measurements_clientmeasurement', 'return_code')

        # Deleting field 'ClientMeasurement.parameters'
        db.delete_column(u'measurements_clientmeasurement', 'parameters')

        # Deleting field 'ServerMetric.description'
        db.delete_column(u'measurements_servermetric', 'description')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'release_date': ('django.db.models.fields.DateField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.release': {
            'Meta': {'object_name': 'Release'},
            'builds': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.Build']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'release_date': ('django.db.models.fields.DateField', [], {})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.server': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('name', 'environment'),)", 'object_name': 'Server'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.clientmeasurement': {
            'Meta': {'unique_together': "(('metric', 'timestamp', 'run'),)", 'object_name': 'ClientMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.clientmeasurementitem': {
            'Meta': {'object_name': 'ClientMeasurementItem'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'tool': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.clientmetric': {
            'Meta': {'unique_together': "(('app', 'name'),)", 'object_name': 'ClientMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMeasurementItem']", 'null': 'True', 'blank': 'True'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_build': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'measurements.servermeasurement': {
            'Meta': {'unique_together': "(('metric', 'server', 'timestamp', 'run'),)", 'object_name': 'ServerMeasurement'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.Server']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.servermetric': {
            'Meta': {'unique_together': "(('app', 'name'),)", 'object_name': 'ServerMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.testcase': {
            'Meta': {'object_name': 'TestCase'},
            'SLA': ('django.db.models.fields.IntegerField', [], {}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.TestCasePriority']"}),
            'summary': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'measurements.testcasepriority': {
            'Meta': {'object_name': 'TestCasePriority'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']