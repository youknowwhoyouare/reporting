# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'TestCase.title'
        db.delete_column(u'measurements_testcase', 'title')

        # Deleting field 'TestCase.summary'
        db.delete_column(u'measurements_testcase', 'summary')

        # Deleting field 'TestCase.priority'
        db.delete_column(u'measurements_testcase', 'priority_id')

        # Deleting field 'TestCase.SLA'
        db.delete_column(u'measurements_testcase', 'SLA')

        # Adding field 'TestCase.version'
        db.add_column(u'measurements_testcase', 'version',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=120),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'TestCase.title'
        db.add_column(u'measurements_testcase', 'title',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=120),
                      keep_default=False)

        # Adding field 'TestCase.summary'
        db.add_column(u'measurements_testcase', 'summary',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'TestCase.priority'
        db.add_column(u'measurements_testcase', 'priority',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=2, to=orm['measurements.TestCasePriority']),
                      keep_default=False)

        # Adding field 'TestCase.SLA'
        db.add_column(u'measurements_testcase', 'SLA',
                      self.gf('django.db.models.fields.IntegerField')(default=3),
                      keep_default=False)

        # Deleting field 'TestCase.version'
        db.delete_column(u'measurements_testcase', 'version')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Vendor']", 'null': 'True'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.releasegroup': {
            'Meta': {'object_name': 'ReleaseGroup'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_build'", 'to': u"orm['apps.Build']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.Build']", 'null': 'True', 'blank': 'True'})
        },
        u'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.server': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('name', 'environment'),)", 'object_name': 'Server'},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.account': {
            'Meta': {'unique_together': "(('phone_number', 'environment'),)", 'object_name': 'Account'},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        u'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Account']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'measurements.clientmeasurement': {
            'Meta': {'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)", 'object_name': 'ClientMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.TestCase']", 'null': 'True', 'blank': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'tool': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.clientmetric': {
            'Meta': {'unique_together': "(('app', 'name', 'measure_type'),)", 'object_name': 'ClientMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.ReleaseGroup']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'measurements.servermeasurement': {
            'Meta': {'unique_together': "(('metric', 'server', 'timestamp', 'run'),)", 'object_name': 'ServerMeasurement'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.Server']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.servermetric': {
            'Meta': {'unique_together': "(('app', 'name'),)", 'object_name': 'ServerMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.testcase': {
            'Meta': {'object_name': 'TestCase'},
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'measurements.testcasepriority': {
            'Meta': {'object_name': 'TestCasePriority'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']