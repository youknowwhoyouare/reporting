# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models

class Migration(DataMigration):

    def forwards(self, orm):
        "Write your forwards methods here."
        # Note: Don't use "from appname.models import ModelName". 
        # Use orm.ModelName to refer to models in this application,
        # and orm['appname.ModelName'] for models in other applications.
        for metric_model in (orm.ServerMetric, orm.ClientMetric):
            for metric in metric_model.objects.all():
                fam_name = metric.family.strip()
                if not fam_name:
                    continue
                fam_ref = None
                for f in orm.MetricFamily.objects.filter(name=fam_name):
                    fam_ref = f
                if not fam_ref:
                    fam_ref = orm.MetricFamily()
                    fam_ref.name = fam_name
                    fam_ref.unit = metric.unit
                    fam_ref.save()

                metric.family_ref = fam_ref
                metric.save()

    def backwards(self, orm):
        "Write your backwards methods here."
        for metric_model in (orm.ServerMetric, orm.ClientMetric):
            for metric in metric_model.objects.all():
                if metric.family_ref:
                    metric.family = metric.family_ref.name

    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Vendor']", 'null': 'True'})
        },
        'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app'),)", 'object_name': 'Build'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['apps.Build']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.server': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)", 'object_name': 'Server'},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_components': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['apps.Component']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'null': 'True'})
        },
        'measurements.account': {
            'Meta': {'object_name': 'Account', 'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)"},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'blank': 'True', 'null': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'object_name': 'ClientMeasurement', 'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)"},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.ClientEnvironment']", 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '500', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL', 'blank': 'True', 'null': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '300', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'tool': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'null': 'True'})
        },
        'measurements.clientmetric': {
            'Meta': {'object_name': 'ClientMetric', 'unique_together': "(('component', 'name', 'measure_type'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.TestCase']", 'blank': 'True', 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'})
        },
        'measurements.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '60'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '12'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Run']", 'symmetrical': 'False'}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'blank': 'True', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.metricfamily': {
            'Meta': {'object_name': 'MetricFamily'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"})
        },
        'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.Flag']", 'blank': 'True', 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'object_name': 'ServerMeasurement', 'unique_together': "(('metric', 'server', 'timestamp', 'run'),)"},
            'account': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '500', 'default': "''"}),
            'return_code': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.TestCase']", 'on_delete': 'models.SET_NULL', 'blank': 'True', 'null': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'object_name': 'ServerMetric', 'unique_together': "(('component', 'name'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']", 'blank': 'True', 'null': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"}),
            'family_ref': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.MetricFamily']", 'on_delete': 'models.SET_NULL', 'blank': 'True', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['measurements.TestCase']", 'blank': 'True', 'symmetrical': 'False', 'default': "''"}),
            'unit': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '120', 'default': "''"})
        },
        'measurements.testcase': {
            'Meta': {'object_name': 'TestCase', 'unique_together': "(('external_key', 'version'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']", 'blank': 'True', 'null': 'True'}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']
    symmetrical = True
