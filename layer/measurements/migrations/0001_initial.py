# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ('apps', '0001_initial'),
        ('environments', '0001_initial')
    )

    def forwards(self, orm):
        # Adding model 'Run'
        db.create_table(u'measurements_run', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('tested_build', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Build'])),
            ('timestamp_start', self.gf('django.db.models.fields.DateTimeField')()),
            ('timestamp_end', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'measurements', ['Run'])

        # Adding model 'ClientMeasurementType'
        db.create_table(u'measurements_clientmeasurementtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'measurements', ['ClientMeasurementType'])

        # Adding model 'ClientMetric'
        db.create_table(u'measurements_clientmetric', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.App'])),
            ('measure_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.ClientMeasurementType'])),
        ))
        db.send_create_signal(u'measurements', ['ClientMetric'])

        # Adding unique constraint on 'ClientMetric', fields ['app', 'name']
        db.create_unique(u'measurements_clientmetric', ['app_id', 'name'])

        # Adding model 'ClientMeasurement'
        db.create_table(u'measurements_clientmeasurement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
            ('value', self.gf('django.db.models.fields.FloatField')()),
            ('run', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.Run'])),
            ('metric', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.ClientMetric'])),
            ('account', self.gf('django.db.models.fields.CharField')(max_length=120, blank=True)),
        ))
        db.send_create_signal(u'measurements', ['ClientMeasurement'])

        # Adding unique constraint on 'ClientMeasurement', fields ['metric', 'timestamp', 'run']
        db.create_unique(u'measurements_clientmeasurement', ['metric_id', 'timestamp', 'run_id'])

        # Adding model 'ServerMetric'
        db.create_table(u'measurements_servermetric', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('unit', self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True)),
            ('app', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.App'], null=True, blank=True)),
        ))
        db.send_create_signal(u'measurements', ['ServerMetric'])

        # Adding unique constraint on 'ServerMetric', fields ['app', 'name']
        db.create_unique(u'measurements_servermetric', ['app_id', 'name'])

        # Adding model 'ServerMeasurement'
        db.create_table(u'measurements_servermeasurement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
            ('value', self.gf('django.db.models.fields.FloatField')()),
            ('run', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.Run'])),
            ('metric', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.ServerMetric'])),
            ('server', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['environments.Server'])),
        ))
        db.send_create_signal(u'measurements', ['ServerMeasurement'])

        # Adding unique constraint on 'ServerMeasurement', fields ['metric', 'server', 'timestamp', 'run']
        db.create_unique(u'measurements_servermeasurement', ['metric_id', 'server_id', 'timestamp', 'run_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'ServerMeasurement', fields ['metric', 'server', 'timestamp', 'run']
        db.delete_unique(u'measurements_servermeasurement', ['metric_id', 'server_id', 'timestamp', 'run_id'])

        # Removing unique constraint on 'ServerMetric', fields ['app', 'name']
        db.delete_unique(u'measurements_servermetric', ['app_id', 'name'])

        # Removing unique constraint on 'ClientMeasurement', fields ['metric', 'timestamp', 'run']
        db.delete_unique(u'measurements_clientmeasurement', ['metric_id', 'timestamp', 'run_id'])

        # Removing unique constraint on 'ClientMetric', fields ['app', 'name']
        db.delete_unique(u'measurements_clientmetric', ['app_id', 'name'])

        # Deleting model 'Run'
        db.delete_table(u'measurements_run')

        # Deleting model 'ClientMeasurementType'
        db.delete_table(u'measurements_clientmeasurementtype')

        # Deleting model 'ClientMetric'
        db.delete_table(u'measurements_clientmetric')

        # Deleting model 'ClientMeasurement'
        db.delete_table(u'measurements_clientmeasurement')

        # Deleting model 'ServerMetric'
        db.delete_table(u'measurements_servermetric')

        # Deleting model 'ServerMeasurement'
        db.delete_table(u'measurements_servermeasurement')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'release_date': ('django.db.models.fields.DateField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.server': {
            'Meta': {'ordering': "['hostname']", 'unique_together': "(('hostname', 'environment'),)", 'object_name': 'Server'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.App']", 'null': 'True', 'symmetrical': 'False'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'measurements.clientmeasurement': {
            'Meta': {'unique_together': "(('metric', 'timestamp', 'run'),)", 'object_name': 'ClientMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMetric']"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'measurements.clientmetric': {
            'Meta': {'unique_together': "(('app', 'name'),)", 'object_name': 'ClientMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_build': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Build']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'measurements.servermeasurement': {
            'Meta': {'unique_together': "(('metric', 'server', 'timestamp', 'run'),)", 'object_name': 'ServerMeasurement'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.ServerMetric']"}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.Server']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        u'measurements.servermetric': {
            'Meta': {'unique_together': "(('app', 'name'),)", 'object_name': 'ServerMetric'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['measurements']