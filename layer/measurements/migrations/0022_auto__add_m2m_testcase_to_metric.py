# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding M2M table for field test_case on 'ServerMetric'
        m2m_table_name = db.shorten_name('measurements_servermetric_test_case')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('servermetric', models.ForeignKey(orm['measurements.servermetric'], null=False)),
            ('testcase', models.ForeignKey(orm['measurements.testcase'], null=False))
        ))
        db.create_unique(m2m_table_name, ['servermetric_id', 'testcase_id'])

        # Adding M2M table for field test_case on 'ClientMetric'
        m2m_table_name = db.shorten_name('measurements_clientmetric_test_case')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('clientmetric', models.ForeignKey(orm['measurements.clientmetric'], null=False)),
            ('testcase', models.ForeignKey(orm['measurements.testcase'], null=False))
        ))
        db.create_unique(m2m_table_name, ['clientmetric_id', 'testcase_id'])


    def backwards(self, orm):
        # Removing M2M table for field test_case on 'ServerMetric'
        db.delete_table(db.shorten_name('measurements_servermetric_test_case'))

        # Removing M2M table for field test_case on 'ClientMetric'
        db.delete_table(db.shorten_name('measurements_clientmetric_test_case'))


    models = {
        'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'to': "orm['apps.Vendor']"})
        },
        'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app'),)", 'object_name': 'Build'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'app': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.App']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'blank': 'True', 'symmetrical': 'False', 'to': "orm['apps.Build']"})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'environments.clientenvironment': {
            'Meta': {'object_name': 'ClientEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'})
        },
        'environments.server': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('name', 'environment', 'decommission_timestamp'),)", 'object_name': 'Server'},
            'decommission_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'hosted_components': ('django.db.models.fields.related.ManyToManyField', [], {'null': 'True', 'blank': 'True', 'symmetrical': 'False', 'to': "orm['apps.Component']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'role': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '120'})
        },
        'measurements.account': {
            'Meta': {'unique_together': "(('phone_number', 'environment', 'deletion_timestamp'),)", 'object_name': 'Account'},
            'deletion_timestamp': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone_number': ('django.db.models.fields.IntegerField', [], {}),
            'user_id': ('django.db.models.fields.BigIntegerField', [], {})
        },
        'measurements.accountproperty': {
            'Meta': {'object_name': 'AccountProperty'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Account']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.clientmeasurement': {
            'Meta': {'unique_together': "(('metric', 'timestamp', 'run', 'account', 'return_code', 'parameters'),)", 'object_name': 'ClientMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['environments.ClientEnvironment']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'default': "''", 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.clientmeasurementtype': {
            'Meta': {'object_name': 'ClientMeasurementType'},
            'description': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'tool': ('django.db.models.fields.CharField', [], {'null': 'True', 'blank': 'True', 'max_length': '120'})
        },
        'measurements.clientmetric': {
            'Meta': {'unique_together': "(('component', 'name', 'measure_type'),)", 'object_name': 'ClientMetric'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_cse': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'measure_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ClientMeasurementType']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['measurements.TestCase']"}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'})
        },
        'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'})
        },
        'measurements.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'max_length': '60', 'unique': 'True'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['measurements.Run']"}),
            'testcase': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['measurements.TestCase']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['measurements.Flag']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Release']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.servermeasurement': {
            'Meta': {'unique_together': "(('metric', 'server', 'timestamp', 'run'),)", 'object_name': 'ServerMeasurement'},
            'account': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.ServerMetric']"}),
            'parameters': ('django.db.models.fields.CharField', [], {'max_length': '500', 'default': "''", 'blank': 'True'}),
            'return_code': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['measurements.Run']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['environments.Server']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['measurements.TestCase']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'value': ('django.db.models.fields.FloatField', [], {})
        },
        'measurements.servermetric': {
            'Meta': {'unique_together': "(('component', 'name'),)", 'object_name': 'ServerMetric'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['apps.Component']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'family': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'unique': 'True'}),
            'test_case': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'symmetrical': 'False', 'to': "orm['measurements.TestCase']"}),
            'unit': ('django.db.models.fields.CharField', [], {'max_length': '120', 'default': "''", 'blank': 'True'})
        },
        'measurements.testcase': {
            'Meta': {'unique_together': "(('external_key', 'version'),)", 'object_name': 'TestCase'},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['apps.Component']"}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['measurements']