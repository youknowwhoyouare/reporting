from django_tables2.tables import TableData
from utils.celery import celery_app
import threading
from django_tables2.utils import Accessor


def _make_sorting_being_case_insensitive():
    TableData.order_by_case_sensitive = TableData.order_by
    Accessor.resolve_case_sensitive = Accessor.resolve
    local = threading.local()

    def order_by(self, aliases):
        local.sorting = True
        try:
            self.order_by_case_sensitive(aliases)
        finally:
            local.sorting = False

    def resolve(self, *args, **kwargs):
        val = self.resolve_case_sensitive(*args, **kwargs)
        return str(val).lower() if (hasattr(local, 'sorting') and local.sorting) and isinstance(val, str) else val

    TableData.order_by = order_by
    Accessor.resolve = resolve


_make_sorting_being_case_insensitive()