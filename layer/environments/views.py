# Create your views here.
import datetime
import xlwt
import django.views.generic as generic_views
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db import transaction, IntegrityError
from django.utils.timezone import utc
from django.template import defaultfilters

from . import models
from . import forms
from . import uploaders
from external_data.servermonitoring import accessor


class ServersUploadView(generic_views.FormView):
    form_class = forms.ServersUploadForm
    template_name = 'upload.html'

    def get_success_url(self):
        return self.request.get_full_path()

    def get_form(self, form_class):
        zbx_accessor = accessor.ZabbixAccessor()
        all_servers = zbx_accessor.get_all_hosts_names()
        imported_servers = models.Server.objects.all().values_list('name',
                                                                   flat=True)
        not_imported_servers = (list(set(all_servers) - set(imported_servers)))
        not_imported_servers.sort()
        # simple trick to make a choices structure from list
        servers_choices = zip(not_imported_servers, not_imported_servers)
        if self.request.POST:
            # get bound form
            return self.form_class(servers_choices, self.request.POST)
        else:
            # get unbound form
            return form_class(servers_choices)

    def form_valid(self, form):
        upload_results = uploaders.upload_servers(form.cleaned_data['environment'],
                                                  form.cleaned_data['servers'])
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results},
                                  context_instance=RequestContext(self.request))

    def form_invalid(self, form):
        return HttpResponse()


class ServerPropertiesFetchView(generic_views.FormView):
    # TODO: add checking of existed values
    # and don't add new if old didn't changed
    form_class = forms.ServerPropertiesFetchForm
    template_name = 'upload.html'

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        self.upload_results(form.cleaned_data)
        return HttpResponse()

    def upload_results(self, cleaned_data):
        upload_results = {'uploaded': 0,
                          'failed': 0,
                          'err_elements': []}
        now = datetime.datetime.utcnow().replace(tzinfo=utc)
        zbx_accessor = accessor.ZabbixAccessor()
        for server in cleaned_data['servers']:
            properties = {}
            properties['Memory bytes'] = zbx_accessor.get_item_last_value(server.name,
                                                                          cleaned_data['memory_bytes'])
            properties['CPU cores'] = zbx_accessor.get_item_last_value(server.name,
                                                                       cleaned_data['cpu_cores'])
            properties['OS installed'] = zbx_accessor.get_item_last_value(server.name,
                                                                          cleaned_data['os_installed'])
            os_installed = properties['OS installed']
            if os_installed and (server.name.upper() in os_installed or server.name in os_installed):
                properties['OS installed'] = properties['OS installed'] \
                    .replace(server.name.upper() + ' ', '') \
                    .replace(server.name + ' ', '')
            for key, value in properties.items():
                try:
                    with transaction.atomic():
                        models.ServerProperty.objects.create(name=key,
                                                             value=value,
                                                             server=server,
                                                             timestamp=now)
                        upload_results['uploaded'] += 1
                except IntegrityError as err:
                    upload_results['failed'] += 1
                    upload_results['err_elements'].append(server.name +
                                                          ' ' + str(err))
        return render_to_response(self.template_name,
                                  {'upload_results': upload_results},
                                  context_instance=RequestContext(self.request))


class EnvironmentLayoutView(generic_views.DetailView):
    model = models.HardwareEnvironment
    template_name = '_environment.html'

    def get_object(self, queryset=None):
        #object = super(EnvironmentLayoutView, self).get_object()
        if queryset is None:
            model_object = self.model.objects.get(name=self.kwargs['env_name'])
        else:
            model_object = queryset.get(name=self.kwargs['env_name'])
        return model_object

    def get_context_data(self, **kwargs):
        context = super(EnvironmentLayoutView, self).get_context_data(**kwargs)
        roles = {}
        servers = models.Server.objects.filter(environment=self.object)
        for server in servers:
            server_role = server.get_server_role()
            server_properties = []
            for s_property in server.get_server_properties():
                server_properties.append((s_property.name.strip(), s_property.value.strip()))
            server_properties = tuple(server_properties)
            if server_role not in roles:
                roles[server_role] = {}
                roles[server_role][server_properties] = 1
            else:
                if server_properties in roles[server_role]:
                    roles[server_role][server_properties] += 1
                else:
                    roles[server_role][server_properties] = 1
        context['roles'] = roles
        return context

    def render_to_excel(self,context):
        w = xlwt.Workbook()
        ws = w.add_sheet(context['hardwareenvironment'].name, cell_overwrite_ok=True)
        style = xlwt.XFStyle()
        font = xlwt.Font()
        font.bold = True
        style.font = font
        role_column = 0
        qty_column = 1
        config_column = 2
        roles = context['roles'].keys()
        current_row = 0
        ws.write(current_row, role_column, 'Server role', style=style)
        ws.write(current_row, qty_column, 'Quantity', style=style)
        ws.write(current_row, config_column, 'Configuration', style=style)
        current_row += 1
        for role_name in roles:
            role = context['roles'][role_name]
            # One role can have servers with different configuration,
            # so each uniq configuration should be processed separately
            for role_configurations, qty in role.items():
                config = '; '.join((': '.join((str(y) for y in x)) for x in role_configurations))
                ws.write(current_row, role_column, role_name)
                ws.write(current_row, qty_column, qty)
                ws.write(current_row, config_column, config)
                current_row += 1
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=' + context['hardwareenvironment'].name + '.xls'
        w.save(response)
        return response


    def render_to_response(self, context, **response_kwargs):
        if self.kwargs['format'] == 'excel':
            return self.render_to_excel(context)
        else:
            return super(EnvironmentLayoutView, self).render_to_response(context, **response_kwargs)

