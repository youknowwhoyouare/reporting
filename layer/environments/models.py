import datetime

from django.db import models
from django.db.models import Q
from django.utils.timezone import utc


class HardwareEnvironment(models.Model):
    """Environment is a group of all-sufficient servers"""
    name = models.CharField(max_length=120, unique=True)
    description = models.TextField(blank=True, null=True)

    def get_actual_servers(self, timestamp=None):
        if timestamp is None:
            return Server.objects.filter(environment=self.id,
                                         decommission_timestamp=None)
        else:
            return Server.objects.filter(environment=self.id) \
                .filter(Q(decommission_timestamp=None) | Q(decommission_timestamp__gte=timestamp))

    def __str__(self):
        return self.name


class Server(models.Model):
    name = models.CharField(max_length=120)
    environment = models.ForeignKey(HardwareEnvironment)
    hosted_components = models.ManyToManyField('apps.Component', null=True, blank=True)
    role = models.CharField(max_length=120, null=True, blank=True)
    decommission_timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = (("name", "environment", "decommission_timestamp"),)
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_server_properties(self, timestamp=None):
        if timestamp is None:
            timestamp = datetime.datetime.utcnow().replace(tzinfo=utc)
        # PostgreSQL only
        return ServerProperty.objects.filter(server=self.id,
                                             timestamp__lte=timestamp)\
            .order_by('name', '-timestamp').distinct('name')

    def get_server_role(self):
        if self.role:
            return self.role
        else:
            try:
                role = ''.join(i for i in self.name.split('-')[2] if not i.isdigit()).upper()
            except IndexError:
                role = 'ALL'
            return role


class ServerProperty(models.Model):
    name = models.CharField(max_length=240)
    value = models.CharField(max_length=240)
    timestamp = models.DateTimeField()
    server = models.ForeignKey(Server)

    class Meta:
        verbose_name_plural = 'server properties'
        unique_together = (('name', 'timestamp', 'server'),)
        get_latest_by = 'timestamp'

    def __str__(self):
        return "%s=%s" % (self.name, self.value)


class ClientEnvironment(models.Model):
    name = models.CharField(max_length=120, unique=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name
