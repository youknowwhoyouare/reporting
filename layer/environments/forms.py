from django import forms
from . import models


class ServersUploadForm(forms.Form):

    def __init__(self, servers_choices, *args, **kwargs):
        super(ServersUploadForm, self).__init__(*args, **kwargs)
        self.fields['environment'] = forms.ModelChoiceField(models.HardwareEnvironment.objects.all())
        self.fields['servers'] = forms.MultipleChoiceField(choices=servers_choices)


class ServerPropertiesFetchForm(forms.Form):
    cpu_cores = forms.CharField(max_length=120, initial='CPU number of cores')
    memory_bytes = forms.CharField(max_length=120, initial='Total memory')
    os_installed = forms.CharField(max_length=120, initial='Host information')
    servers = forms.ModelMultipleChoiceField(models.Server.objects.all())
