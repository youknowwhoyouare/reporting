from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required, permission_required

from . import views

urlpatterns = patterns('',
                       url(r'^upload_servers/$',
                           login_required(views.ServersUploadView.as_view()),
                           name='upload_new_servers'),
                       url(r'^fetch_servers_properties/$',
                           login_required(views.ServerPropertiesFetchView.as_view()),
                           name='fetch_server_properties'),
                       url(r'^layout/(?P<env_name>[A-Za-z0-9 ]+)/(?P<format>[A-Za-z0-9 ]+)/$',
                           views.EnvironmentLayoutView.as_view(),
                           name="html_layout"),
                       )

