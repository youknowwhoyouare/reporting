# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
        ('apps', '0001_initial'),
    )

    def forwards(self, orm):
        # Adding model 'HardwareEnvironment'
        db.create_table(u'environments_hardwareenvironment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'environments', ['HardwareEnvironment'])

        # Adding model 'Server'
        db.create_table(u'environments_server', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hostname', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('environment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['environments.HardwareEnvironment'])),
        ))
        db.send_create_signal(u'environments', ['Server'])

        # Adding unique constraint on 'Server', fields ['name', 'environment']
        db.create_unique(u'environments_server', ['hostname', 'environment_id'])

        # Adding M2M table for field hosted_apps on 'Server'
        m2m_table_name = db.shorten_name(u'environments_server_hosted_apps')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('server', models.ForeignKey(orm[u'environments.server'], null=False)),
            ('app', models.ForeignKey(orm[u'apps.app'], null=False))
        ))
        db.create_unique(m2m_table_name, ['server_id', 'app_id'])

        # Adding model 'ServerProperties'
        db.create_table(u'environments_serverproperties', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cpu_cores', self.gf('django.db.models.fields.IntegerField')()),
            ('total_memory_bytes', self.gf('django.db.models.fields.IntegerField')()),
            ('os_installed', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
            ('server', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['environments.Server'])),
        ))
        db.send_create_signal(u'environments', ['ServerProperties'])


    def backwards(self, orm):
        # Removing unique constraint on 'Server', fields ['name', 'environment']
        db.delete_unique(u'environments_server', ['hostname', 'environment_id'])

        # Deleting model 'HardwareEnvironment'
        db.delete_table(u'environments_hardwareenvironment')

        # Deleting model 'Server'
        db.delete_table(u'environments_server')

        # Removing M2M table for field hosted_apps on 'Server'
        db.delete_table(db.shorten_name(u'environments_server_hosted_apps'))

        # Deleting model 'ServerProperties'
        db.delete_table(u'environments_serverproperties')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.server': {
            'Meta': {'ordering': "['hostname']", 'unique_together': "(('hostname', 'environment'),)", 'object_name': 'Server'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.App']", 'null': 'True', 'symmetrical': 'False'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'environments.serverproperties': {
            'Meta': {'object_name': 'ServerProperties'},
            'cpu_cores': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'os_installed': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.Server']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {}),
            'total_memory_bytes': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['environments']