# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ServerProperties.cpu_cores'
        db.delete_column(u'environments_serverproperties', 'cpu_cores')

        # Deleting field 'ServerProperties.os_installed'
        db.delete_column(u'environments_serverproperties', 'os_installed')

        # Deleting field 'ServerProperties.total_memory_bytes'
        db.delete_column(u'environments_serverproperties', 'total_memory_bytes')

        # Adding field 'ServerProperties.name'
        db.add_column(u'environments_serverproperties', 'name',
                      self.gf('django.db.models.fields.CharField')(default='CPU cores', max_length=120),
                      keep_default=False)


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'ServerProperties.cpu_cores'
        raise RuntimeError("Cannot reverse this migration. 'ServerProperties.cpu_cores' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'ServerProperties.os_installed'
        raise RuntimeError("Cannot reverse this migration. 'ServerProperties.os_installed' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'ServerProperties.total_memory_bytes'
        raise RuntimeError("Cannot reverse this migration. 'ServerProperties.total_memory_bytes' and its values cannot be restored.")
        # Deleting field 'ServerProperties.name'
        db.delete_column(u'environments_serverproperties', 'name')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'environments.server': {
            'Meta': {'ordering': "['hostname']", 'unique_together': "(('hostname', 'environment'),)", 'object_name': 'Server'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'hosted_apps': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.App']", 'null': 'True', 'blank': 'True'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'})
        },
        u'environments.serverproperties': {
            'Meta': {'object_name': 'ServerProperties'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.Server']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['environments']