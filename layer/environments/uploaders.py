from django.db import IntegrityError

from . import models


def upload_servers(environment, server_names):
    upload_results = {'uploaded': 0,
                      'failed': 0,
                      'err_elements': []}
    for server_name in server_names:
        try:
            models.Server.objects.create(name=server_name,
                                         environment=environment)
            upload_results['uploaded'] += 1
        except IntegrityError:
            upload_results['failed'] += 1
            upload_results['err_elements'].append(server_name)
    return upload_results
