from django.contrib import admin

from . import models


class ServerPropertiesAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ['server', 'name', 'value', 'timestamp']
    list_filter = ['server__role', 'server', 'name', 'timestamp']
    search_fields = ['name', 'server__name', 'server__role']

    class Meta:
        model = models.ServerProperty


class ServerAdmin(admin.ModelAdmin):
    list_select_related = True
    search_fields = ['name', 'role']
    list_display = ['name', 'role', 'environment']
    list_filter = ['role', 'environment']

    class Meta:
        model = models.Server

admin.site.register(models.HardwareEnvironment)
admin.site.register(models.Server, ServerAdmin)
admin.site.register(models.ServerProperty, ServerPropertiesAdmin)
