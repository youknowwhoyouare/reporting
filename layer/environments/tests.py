from datetime import datetime, timedelta

import pytz
from django.test import TestCase
from django.db import transaction, IntegrityError
from django.core.urlresolvers import reverse
from django.utils.timezone import utc

from . import models


class BaseTest(TestCase):
    def setUp(self):
        with transaction.atomic():
            self.test_environment = models.HardwareEnvironment.objects.create(name='TestEnvironment')
            self.test_server = models.Server.objects.create(name='TestServer1',
                                                            environment=self.test_environment,
                                                            role='Role1')
            self.utc_timzone = pytz.timezone('UTC')


class ServerPropertiesModelTest(BaseTest):
    def setUp(self):
        super(ServerPropertiesModelTest, self).setUp()
        self.last_date = datetime.now().replace(tzinfo=self.utc_timzone) - timedelta(days=1)
        self.prev_date = self.last_date - timedelta(days=1)
        self.future_date = self.last_date + timedelta(days=3)
        self.valid_property_name = 'Property1'
        self.valid_property_value = 'Current'
        self.prev_property_value = 'Past'
        self.future_property_value = 'Future'
        with transaction.atomic():
            models.ServerProperty.objects.create(name=self.valid_property_name,
                                                 value=self.valid_property_value,
                                                 timestamp=self.last_date,
                                                 server=self.test_server)
            models.ServerProperty.objects.create(name=self.valid_property_name,
                                                 value=self.prev_property_value,
                                                 timestamp=self.prev_date,
                                                 server=self.test_server)
            models.ServerProperty.objects.create(name=self.valid_property_name,
                                                 value=self.future_property_value,
                                                 timestamp=self.future_date,
                                                 server=self.test_server)

    def test_get_property(self):
        server = models.Server.objects.get(name=self.test_server.name,
                                           environment=self.test_environment)
        property_queryset = server.get_server_properties().filter(name=self.valid_property_name)
        # Check that there's only one value for one property
        self.assertEqual(property_queryset.count(), 1)
        # Check current property value
        valid_property = property_queryset[0]
        self.assertEqual(valid_property.value, self.valid_property_value)

    def test_get_old_property(self):
        server = models.Server.objects.get(name=self.test_server.name,
                                           environment=self.test_environment)
        property_queryset = server.get_server_properties(self.prev_date + timedelta(minutes=3)) \
            .filter(name=self.valid_property_name)
        # Check that there's only one value for one property
        self.assertEqual(property_queryset.count(), 1)
        valid_prev_property = property_queryset[0]
        self.assertEqual(valid_prev_property.value, self.prev_property_value)

    def test_future_property(self):
        server = models.Server.objects.get(name=self.test_server.name,
                                           environment=self.test_environment)

        property_queryset = server.get_server_properties(self.future_date).filter(name=self.valid_property_name)
        # Check that there's only one value for one property
        self.assertEqual(property_queryset.count(), 1)
        future_property = property_queryset[0]
        self.assertEqual(future_property.value, self.future_property_value)

    def test_same_timestamp_same_server(self):
        with self.assertRaises(IntegrityError):
            models.ServerProperty.objects.create(name=self.valid_property_name,
                                                 value='Wrong',
                                                 timestamp=self.last_date,
                                                 server=self.test_server)
        transaction.rollback()

    def test_same_timestamp_different_server(self):
        additional_server = models.Server.objects.create(name='TestServer2',
                                                         environment=self.test_environment)
        check_value = 'Additional'
        with transaction.atomic():
            models.ServerProperty.objects.create(name=self.valid_property_name,
                                                 value=check_value,
                                                 timestamp=self.last_date,
                                                 server=additional_server)
        server = models.Server.objects.get(name=additional_server.name,
                                           environment=self.test_environment)
        property_queryset = server.get_server_properties().filter(name=self.valid_property_name)
        # Check that there's only one value for one property
        self.assertEqual(property_queryset.count(), 1)
        # Check current property value
        valid_property = property_queryset[0]
        self.assertEqual(valid_property.value, check_value)


class EnvironmentViewHTMLSimpleTest(BaseTest):

    def setUp(self):
        super(EnvironmentViewHTMLSimpleTest, self).setUp()

    def _generate_env_data(self):
        servers = []
        for role in ('ABC', 'XYZ', 'ZZZ'):
            for i in range(5):
                server = models.Server.objects.create(name='Server' + role + str(i),
                                                      environment=self.test_environment,
                                                      role=role)
                servers.append(server)
        for server in servers:
            for propery_name in ('CPU', 'Mem'):
                models.ServerProperty.objects.create(name=propery_name,
                                                     value='test',
                                                     timestamp=datetime(2013, 5, 5, 14, 35).replace(tzinfo=utc),
                                                     server=server)
        return servers

    def test_simple_html_layout(self):
        servers = self._generate_env_data()
        html_environment_url = reverse('environments:html_layout',
                                       kwargs={'env_name': self.test_environment.name,
                                               'format': 'web'})
        response = self.client.get(html_environment_url)
        self.assertTrue(servers[0].role.encode('utf-8') in response.content)
        self.assertEqual(response.status_code, 200)

    def test_simple_excel_layout(self):
        self._generate_env_data()
        html_environment_url = reverse('environments:html_layout',
                                       kwargs={'env_name': self.test_environment.name,
                                               'format': 'excel'})
        response = self.client.get(html_environment_url)
        self.assertEqual(response.status_code, 200)

