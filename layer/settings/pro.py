from .base import *

DEBUG = False

ADMINS = (
    ('Nikolay Golub', 'nikolay.golub@nordigy.ru'),
    ('Igor Leontiev', 'igor.leontiev@nordigy.ru'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'layer',
        'USER': 'layer',
        'PASSWORD': 'layer',
        'HOST': '127.0.0.1',
        'PORT': '9999',
    }
}


ALLOWED_HOSTS = ['perf-field',
                 'perf-field.stage.ringcentral.com',
                 'perf-field.int.nordigy.ru',
                 'qa-tools-dashboard.dins.ru',
                 'sjc07-c01-prf01.ops.ringcentral.com',
                 ]

TIME_ZONE = 'UTC'

INSTALLED_APPS = INSTALLED_APPS + ('south','dbparti')

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        # infinite:
        # 'TIMEOUT': '0',
        # 14 days
        'TIMEOUT': '1209600',
    }
}

TEMPLATE_VISIBLE_SETTINGS = ('MAIN_PAGE_NAME',
                             'JIRA_URL',
                             'COMPANY_NAME',
                             'COMPANY_WEB_SITE',
                             'CELERY_MON_URL')

TMP_UPLOAD_LOCATION = '/opt/layer/tmp'

MAIN_PAGE_NAME = 'Main Layer'

COMPANY_NAME = 'Dins'
COMPANY_WEB_SITE = 'http://dins.ru'

JIRA_URL = 'http://jira.ringcentral.com/'
JIRA_LOGIN = 'igor.leontiev'
JIRA_PASSWORD = '1qaz@WSX'

CELERY_MON_URL = 'http://sjc07-c01-prf01.ops.ringcentral.com:5555'

TESTCASEMGMT_URL = 'http://testlink.dins.ru/lib/api/xmlrpc/v1/xmlrpc.php'
TESTCASEMGMT_API_KEY = 'dummy_key'


ZABBIXES = (
    {'host': 'http://mon.stage.ringcentral.com',
     'login': 'perfteam_api',
     'password': 'perfteam_api'},
)

TESTLINK_SERVER = '192.168.12.11'
TESTLINK_DBUSER = 'testlinkuser'
TESTLINK_DBPASS = 'testlinkpw'
TESTLINK_DBNAME = 'testlink'

OLD_REPORTS_URL = 'http://qa-tools-dashboard/Amarosa'

MEDIA_ROOT = '/opt/layer/media/'


with open('/etc/layer.ini') as f:
    SECRET_KEY = f.read().strip()

