# Development Django settings for layer project
from .base import *

DEBUG = True

TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Nikolay Golub', 'nikolay.v.golub@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'layer',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'layer',
        'PASSWORD': 'layer',
        'HOST': '127.0.0.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}


# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Moscow'


INSTALLED_APPS = INSTALLED_APPS + ('south','django_tables2_reports','dbparti', 'debug_toolbar') # to find debug_toolbar templates

MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + ('debug_toolbar.middleware.DebugToolbarMiddleware',)

browser_host_ilyaneverov2 = '192.168.38.120'
INTERNAL_IPS = ('127.0.0.1', '192.168.0.101', browser_host_ilyaneverov2)

#
# def custom_show_toolbar(request):
#     return True  # Always show toolbar, for example purposes only.

# DEBUG_TOOLBAR_CONFIG = {
#     'SHOW_TOOLBAR_CALLBACK': 'settings.dev.custom_show_toolbar',
#     'ENABLE_STACKTRACES': True,
# }

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        # infinite:
        # 'TIMEOUT': '0',
        # 14 days
        'TIMEOUT': '1209600',
        }
}
# These environment variables are visible in template context
TEMPLATE_VISIBLE_SETTINGS = ('MAIN_PAGE_NAME',
                             'JIRA_URL',
                             'COMPANY_NAME',
                             'COMPANY_WEB_SITE',
                             'CELERY_MON_URL')

MAIN_PAGE_NAME = 'Main Layer'
MEDIA_ROOT = '/opt/layer/media'
COMPANY_NAME = 'Contoso Inc.'
COMPANY_WEB_SITE = 'http://example.com'
EXCEL_SUPPORT = 'xlwt'
CELERY_MON_URL = 'http:/127.0.0.1:5555/'

# JIRA configuration
JIRA_URL = 'http://jira.example.com/'

# Configuration of testcase management connector
TESTCASEMGMT_URL = 'http://testlink.dins.ru/lib/api/xmlrpc/v1/xmlrpc.php'
TESTCASEMGMT_API_KEY = 'dummy_key'

# Configuration of server monitoring connector
ZABBIXES = (
    {'host': 'http://mon.stage.ringcentral.com',
     'login': 'perfteam_api',
     'password': 'perfteam_api'},
)

# Secret key for developer base. Do not use it in production!
SECRET_KEY = '#7b0f$#@^3(ha)!4c5$inl9m$f=zzhjg^@7=@4pq+)p7vx2l=r'


TESTLINK_SERVER = '192.168.12.11'
TESTLINK_DBUSER = 'testlinkuser'
TESTLINK_DBPASS = 'testlinkpw'
TESTLINK_DBNAME = 'testlink'

OLD_REPORTS_URL = 'http://qa-tools-dashboard/Amarosa'

SOUTH_TESTS_MIGRATE = False

