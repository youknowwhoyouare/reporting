from .base import *

DEBUG = False

ADMINS = (
    ('Nikolay Golub', 'nikolay.v.golub@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'layer',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'layer',
        'PASSWORD': 'layer',
        'HOST': '127.0.0.1',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

ALLOWED_HOSTS = []


TIME_ZONE = 'Europe/Moscow'


INSTALLED_APPS += ('django_jenkins',
                  #  'external_data', # some strange thing with tests inside django tests
                   )

JENKINS_TASKS = (
    'django_jenkins.tasks.with_coverage',
    'django_jenkins.tasks.django_tests',   # select one django or
    #'django_jenkins.tasks.dir_tests',      # directory tests discovery
    'django_jenkins.tasks.run_pep8',
    'django_jenkins.tasks.run_pyflakes',
    'django_jenkins.tasks.run_pylint',
    #'django_jenkins.tasks.run_jslint',
    #'django_jenkins.tasks.run_csslint',
    #'django_jenkins.tasks.run_sloccount',
    #'django_jenkins.tasks.lettuce_tests',
)

PROJECT_APPS = ('apps',
                'environments',
                'measurements',
                'reports',
                'external_data',
                )
TEMPLATE_VISIBLE_SETTINGS = ('MAIN_PAGE_NAME',
                             'JIRA_URL',
                             'COMPANY_NAME',
                             'COMPANY_WEB_SITE')
MAIN_PAGE_NAME = 'Main Layer'
# Add trailing slash
JIRA_URL = 'http://jira.example.com/'
COMPANY_NAME = 'Contoso Inc.'
COMPANY_WEB_SITE = 'http://example.com'


TESTCASEMGMT_URL = 'http://testlink.dins.ru/lib/api/xmlrpc/v1/xmlrpc.php'
TESTCASEMGMT_API_KEY = 'dummy_key'

ZABBIXES = (
    {'host': 'http://example.com',
     'login': 'jenkins',
     'password': 'password'},
)

SECRET_KEY = get_env_variable('DJANGO_SECRET_KEY')


PEP8_RCFILE = 'pep8.rc'
PYLINT_RCFILE = 'pylint.rc'

CELERY_ALWAYS_EAGER = True