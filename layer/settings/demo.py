from .pro import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'layer_demo',
        'USER': 'layer',
        'PASSWORD': 'layer',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}