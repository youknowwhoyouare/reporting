from __future__ import unicode_literals

import logging

from datetime import datetime
from collections import defaultdict
import re
import threading

from pytz import timezone
from pyzabbix import ZabbixAPI
from django.conf import settings

logger = logging.getLogger(__name__)


class ZabbixAccessor(object):

    def __init__(self):
        self.history_limit = 20000
        self.client = ZabbixAPI(settings.ZABBIXES[0]['host'])
        self.client.login(settings.ZABBIXES[0]['login'],
                          settings.ZABBIXES[0]['password'])
        zbx_timezone = settings.ZABBIXES[0].get('timezone', 'UTC')
        self.zabbix_timezone = timezone(zbx_timezone)
        self.local_timezone = timezone(settings.TIME_ZONE)
        self.errors_list = []
        logger.info('Zabbix accessor is initialized')

    @staticmethod
    def _to_zabbix_timestamp(timestamp):
        """Converts datetime object to unix timestamp

        Note: it should has a time
        """
        if timestamp.tzinfo is None:
            logger.warning('Timezone isn\'t set for timestamp, ')
        return timestamp.timestamp()

    @staticmethod
    def _from_zabbix_timestamp(timestring):
        return datetime.utcfromtimestamp(int(timestring)).replace(tzinfo=timezone('UTC'))

    @staticmethod
    def _restore_item_name(original_name, key_value, regex=re.compile('^.*\[(.*)\]$')):
        if '$' in original_name:
            keys = regex.match(key_value).group(1).split(',')
            restored_name = original_name
            for i, key in enumerate(keys):
                restored_name = restored_name.replace('$'+str(i+1), key)
            return restored_name
        else:
            return original_name

    def get_all_hosts_names(self):
        raw_hosts = self.client.host.get(output=('name', 'hostid'),
                                         sortfield='name')
        hosts = [x['name'] for x in raw_hosts]
        return hosts

    def get_hosts_metric_names(self, host_names):
        raw_items = self.client.item.get(output=('name', 'key_'),
                                         filter={'host': host_names})
        items = [self._restore_item_name(x['name'], x['key_']) for x in raw_items]
        return list(set(items))

    def get_item_last_value(self, host_name, item_name):
        raw_items = self.client.item.get(output='extend',
                                         host=host_name,
                                         filter={'name': item_name})
        if len(raw_items) != 1:
            self.errors_list.append('Number of items with name {name}'
                                    ' for host {host_name} is not equal 1'.format(name=item_name,
                                                                                  host_name=host_name))
            return
        return raw_items[0]['lastvalue']

    def get_hosts_items_history(self, hosts_names, items_names, start_range, end_range):
        """ Returns dictionary with host names as a keys and dictionary as a values,
        which contains item names and history data.

        Note: start and range should be timezone awared
        """
        logger.info('Requesting history from Zabbix is started')
        start_range = self._to_zabbix_timestamp(start_range)
        end_range = self._to_zabbix_timestamp(end_range)

        lock = threading.Lock()
        results = defaultdict(lambda: defaultdict(list))
        hosts = self.client.host.get(filter={'host': hosts_names}, output='extend')
        hosts_tuples = [(x['name'], x['hostid']) for x in hosts]

        def get_host_items_history(host_name, host_id):
            available_items = self.client.item.get(hostids=host_id, output='extend')
            available_items_by_name = {}
            target_item_ids = []
            for available_item in available_items:
                restored_name = self._restore_item_name(available_item['name'],
                                                        available_item['key_'])
                available_items_by_name[restored_name] = available_item['itemid']
            with lock:
                for item_name in items_names:
                    if item_name in available_items_by_name:
                        target_item_ids.append(available_items_by_name[item_name])
                    #else:
                        #print 'Item %s is not available for %s' % (item_name, host_name)
            # script iterates here, beacuse data can be not fetched for some items(see if inside loop)
            # Need to investigate zabbix api for that
            history = []
            for target_item_id in target_item_ids:
                item_history = self.client.history.get(itemids=[target_item_id],
                                                       time_from=start_range,
                                                       time_till=end_range,
                                                       output='extend',
                                                       limit=str(self.history_limit))
                if not item_history:
                    item_history = self.client.history.get(itemids=[target_item_id],
                                                           time_from=start_range,
                                                           time_till=end_range,
                                                           output='extend',
                                                           limit=str(self.history_limit),
                                                           history=0)
                history.extend(item_history)
            items_by_id = {v: k for k, v in available_items_by_name.items()}
            for entry in history:
                entry_value = self._from_zabbix_timestamp(entry['clock']), float(entry['value'])
                with lock:
                    results[host_name][items_by_id[entry['itemid']]].append(entry_value)
        threads = []
        for hosts_name, host_id in hosts_tuples:
            thread = threading.Thread(target=get_host_items_history, args=(hosts_name, host_id))
            threads.append(thread)
            thread.start()

        for t in threads:
            t.join()
        logger.info('Requesting history from Zabbix completed')
        return results
