# Standard library
import logging

# Django modules
from django.conf import settings
from django.db.models.loading import get_model
from django.contrib.auth.models import User as usr_model

# 3rd party modules
from jira.client import JIRA

# Project modules
from reports import models as rep_models

logger = logging.getLogger(__name__)


class JiraAccessor(object):
    def __init__(self, *args, **kwargs):
        self.test_model = get_model('reports', 'Test')
        self.default_components = {
            'testing_perf': "Testing - Performance"
        }

        self.default_project = 'Performance Analysis'
        options = {
            'server': settings.JIRA_URL
        }
        self.custom_fields = {
            'userstory': "customfield_10460",
            'exist_on_pro': "customfield_10570",
            'reproducible': "customfield_10220",
            'build_found': "customfield_10270",
            'furps': "customfield_11253",
            'project': "customfield_12052",
            'found_by_auto_tests': "customfield_11152",
            'test_case_link': "customfield_10630"
        }
        self.jira = JIRA(options=options,
                         basic_auth=(settings.JIRA_LOGIN, settings.JIRA_PASSWORD))

    def _get_all_performance_tests(self):
        tickets = self.jira.search_issues('project = "'+self.default_project+'" and component = "'
                                          + self.default_components['testing_perf']+'"')
        return tickets

    def _get_fix_version_performance_tests(self, fix_version):
        tickets = self.jira.search_issues('project = "'+self.default_project+'" and component = "'
                                          + self.default_components['testing_perf']+'"'+'" and fix_version = "'
                                          + fix_version)
        return tickets

    def _get_custom_search_results(self, query):
        tickets = self.jira.search_issues(query)
        return tickets

    def get_jira_test(self, key):
        issue = self.jira.issue(key, fields='summary,reporter,status')
        user, created = usr_model.objects.get_or_create(username=issue.fields.reporter.name)
        status, created = rep_models.TestStatus.objects.get_or_create(name=issue.fields.status.name)
        test = self.test_model(
            jira_key=key,
            title=issue.fields.summary,
            requester=user,
            status=status,
        )
        return test

    def get_prepared_tests(self):
        tests = []
        issues = self._get_all_performance_tests()
        for issue in issues:
            test = self.get_jira_test(issue.key)
            tests.append(test)
        return tests
