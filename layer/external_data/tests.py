from datetime import date, datetime, timedelta
# import json
# from urllib.parse import urljoin

from django.test import TestCase
# from django.conf import settings

from mock import MagicMock
import pytz
# import httpretty

from .testcases import testlink_accessor
# from .servermonitoring import zabbix_accessor

# from measurements import models as meas_models
# from apps import models as app_models
# from environments import models as env_models

"""
class ZabbixAccessorTests(TestCase):

    @httpretty.activate
    def test_simple_login(self):
        httpretty.register_uri(
            httpretty.POST,
            urljoin(settings.ZABBIXES[0]['host'], '/api_jsonrpc.php'),
            body=json.dumps({
                "jsonrpc": "2.0",
                "result": "0424bd59b807674191e7d77572075f33",
                "id": 0
            })
        )
        client = zabbix_accessor.ZabbixAccessor()
        self.assertIsNotNone(client)

    @httpretty.activate
    def test_get_history(self):

        def zabbix_callbacks(method, uri, headers):
            body = json.loads(method.body.decode('utf-8'))
            response_body = ''
            if body['method'] == 'user.login':
                response_body = json.dumps({
                    "jsonrpc": "2.0",
                    "result": "0424bd59b807674191e7d77572075f33",
                    "id": 0
                })
            elif body['method'] == 'host.get':
                response_body = json.dumps({'result': [
                    {'available': '1', 'maintenance_type': '0', 'maintenances': [], 'ipmi_username': '',
                     'snmp_disable_until': '0', 'ipmi_authtype': '-1', 'ipmi_disable_until': '0',
                     'lastaccess': '0', 'snmp_error': '', 'ipmi_privilege': '2', 'jmx_error': '',
                     'jmx_available': '0', 'ipmi_errors_from': '0', 'maintenanceid': '0', 'snmp_available': '0',
                     'status': '0', 'host': 'sjc02-p02-adb01', 'disable_until': '1324893686',
                     'ipmi_password': '', 'ipmi_available': '0', 'maintenance_status': '0',
                     'snmp_errors_from': '0', 'ipmi_error': '', 'proxy_hostid': '100100000011335',
                     'hostid': '100100000011339', 'name': 'sjc02-p02-adb01', 'jmx_errors_from': '0',
                     'jmx_disable_until': '0', 'error': '', 'maintenance_from': '0',
                     'errors_from': '1324644563'},
                    {'available': '1', 'maintenance_type': '0', 'maintenances': [], 'ipmi_username': '',
                     'snmp_disable_until': '1324893984', 'ipmi_authtype': '-1', 'ipmi_disable_until': '0',
                     'lastaccess': '0', 'snmp_error': '', 'ipmi_privilege': '2', 'jmx_error': '',
                     'jmx_available': '0', 'ipmi_errors_from': '0', 'maintenanceid': '0', 'snmp_available': '1',
                     'status': '0', 'host': 'sjc02-p02-tas01', 'disable_until': '1324893978',
                     'ipmi_password': '', 'ipmi_available': '0', 'maintenance_status': '0',
                     'snmp_errors_from': '1324644607', 'ipmi_error': '', 'proxy_hostid': '100100000011335',
                     'hostid': '100100000011369', 'name': 'sjc02-p02-tas01', 'jmx_errors_from': '0',
                     'jmx_disable_until': '0', 'error': '', 'maintenance_from': '0',
                     'errors_from': '1324644571'}]
                }
                )
            elif body['method'] == 'item.get':
                host_id = body['params']['hostids'][0]
                response_body = json.dumps({'result':
                    [{'itemid': '100100000143080', 'username': '', 'inventory_link': '0',
                      'lastclock': '1387446210', 'lastlogsize': '0', 'trends': '365',
                      'snmpv3_authpassphrase': '', 'snmp_oid': '', 'templateid': '100100000188641',
                      'snmpv3_securitylevel': '0', 'port': '', 'multiplier': '0', 'lastns': '647013882',
                      'authtype': '0', 'password': '', 'logtimefmt': '', 'mtime': '0', 'delay': '90',
                      'publickey': '', 'params': '', 'snmpv3_securityname': '', 'formula': '1', 'type': '0',
                      'prevvalue': '0.090896', 'status': '0', 'lastvalue': '0.093708', 'snmp_community': '',
                      'description': '', 'data_type': '0', 'trapper_hosts': '', 'units': '',
                      'value_type': '0', 'prevorgvalue': '0', 'delta': '0', 'delay_flex': '',
                      'lifetime': '30', 'interfaceid': '100100000034017', 'snmpv3_privpassphrase': '',
                      'hostid': host_id, 'key_': 'system.cpu.util[,system,avg1]',
                      'name': 'CPU $2 time ($3)', 'privatekey': '', 'filter': '', 'valuemapid': '0',
                      'flags': '0', 'error': '', 'ipmi_sensor': '', 'history': '60'},
                     {'itemid': '100100000143081', 'username': '', 'inventory_link': '0',
                      'lastclock': '1387446211', 'lastlogsize': '0', 'trends': '365',
                      'snmpv3_authpassphrase': '', 'snmp_oid': '', 'templateid': '100100000188642',
                      'snmpv3_securitylevel': '0', 'port': '', 'multiplier': '0', 'lastns': '654745919',
                      'authtype': '0', 'password': '', 'logtimefmt': '', 'mtime': '0', 'delay': '90',
                      'publickey': '', 'params': '', 'snmpv3_securityname': '', 'formula': '1', 'type': '0',
                      'prevvalue': '1.609109', 'status': '0', 'lastvalue': '1.529897', 'snmp_community': '',
                      'description': '', 'data_type': '0', 'trapper_hosts': '', 'units': '',
                      'value_type': '0', 'prevorgvalue': '0', 'delta': '0', 'delay_flex': '',
                      'lifetime': '30', 'interfaceid': '100100000034017', 'snmpv3_privpassphrase': '',
                      'hostid': host_id, 'key_': 'system.cpu.util[,user,avg1]',
                      'name': 'CPU user time (avg1)', 'privatekey': '', 'filter': '', 'valuemapid': '0',
                      'flags': '0', 'error': '', 'ipmi_sensor': '', 'history': '60'},
                     {'itemid': '100100000195826', 'username': '', 'inventory_link': '0',
                      'lastclock': '1387446126', 'lastlogsize': '0', 'trends': '180',
                      'snmpv3_authpassphrase': '', 'snmp_oid': '', 'templateid': '0',
                      'snmpv3_securitylevel': '0', 'port': '', 'multiplier': '0', 'lastns': '22677989',
                      'authtype': '0', 'password': '', 'logtimefmt': '', 'mtime': '0', 'delay': '60',
                      'publickey': '', 'params': '', 'snmpv3_securityname': '', 'formula': '1', 'type': '0',
                      'prevvalue': '0', 'status': '3', 'lastvalue': '0', 'snmp_community': '',
                      'description': '', 'data_type': '0', 'trapper_hosts': '', 'units': '%',
                      'value_type': '3', 'prevorgvalue': '0', 'delta': '0', 'delay_flex': '',
                      'lifetime': '30', 'interfaceid': '100100000034017', 'snmpv3_privpassphrase': '',
                      'hostid': host_id, 'key_': 'vfs.fs.size[/,pfree]',
                      'name': 'Free disk space on $1 %', 'privatekey': '', 'filter': '', 'valuemapid': '0',
                      'flags': '4',
                      'error': 'Received value [73.052245] is not suitable for value type Numeric',
                      'ipmi_sensor': '', 'history': '30'},
                     {'itemid': '100100000143100', 'username': '', 'inventory_link': '0',
                      'lastclock': '1387446140', 'lastlogsize': '0', 'trends': '365',
                      'snmpv3_authpassphrase': '', 'snmp_oid': 'interfaces.ifTable.ifEntry.ifInOctets.1',
                      'templateid': '100100000188658', 'snmpv3_securitylevel': '0', 'port': '',
                      'multiplier': '0', 'lastns': '443711000', 'authtype': '0', 'password': '',
                      'logtimefmt': '', 'mtime': '0', 'delay': '90', 'publickey': '',
                      'params': '100-((last(vm.memory.size[free])+last(vm.memory.size[cached]))/last(vm.memory.size[total])*100)',
                      'snmpv3_securityname': '', 'formula': '1', 'type': '15', 'prevvalue': '49.274093',
                      'status': '0', 'lastvalue': '49.276876', 'snmp_community': 'public', 'description': '',
                      'data_type': '0', 'trapper_hosts': '', 'units': '%', 'value_type': '0',
                      'prevorgvalue': '0', 'delta': '0', 'delay_flex': '', 'lifetime': '30',
                      'interfaceid': '0', 'snmpv3_privpassphrase': '', 'hostid': host_id,
                      'key_': 'vm.mem.used', 'name': 'Memory used %', 'privatekey': '', 'filter': '',
                      'valuemapid': '0', 'flags': '0', 'error': '', 'ipmi_sensor': '', 'history': '60'},
                     ]}
                )
            elif body['method'] == 'history.get':
                history_response = {'result': []}
                for item_id in body['params']['itemids']:
                    i_history = [
                        {'itemid': item_id, 'ns': '90179836', 'value': '28485746688',
                         'clock': '1387445333'},
                        {'itemid': item_id, 'ns': '334999448', 'value': '28480778240',
                         'clock': '1387445423'},
                        {'itemid': item_id, 'ns': '285634663', 'value': '28499050496',
                         'clock': '1387445513'},
                        {'itemid': item_id, 'ns': '88505744', 'value': '28491354112',
                         'clock': '1387445605'},
                        {'itemid': item_id, 'ns': '181360815', 'value': '28493279232',
                         'clock': '1387445693'},
                        {'itemid': item_id, 'ns': '508476670', 'value': '28485025792',
                         'clock': '1387445783'},
                        {'itemid': item_id, 'ns': '190081888', 'value': '28484001792',
                         'clock': '1387445873'},
                        {'itemid': item_id, 'ns': '344109164', 'value': '28475236352',
                         'clock': '1387445963'},
                        {'itemid': item_id, 'ns': '83870315', 'value': '28470145024',
                         'clock': '1387446053'},
                        {'itemid': item_id, 'ns': '64177382', 'value': '28460265472',
                         'clock': '1387446143'}]
                    history_response['result'].extend(i_history)
                response_body = json.dumps(history_response)

            return 200, headers, response_body

        httpretty.register_uri(httpretty.POST,
                               urljoin(settings.ZABBIXES[0]['host'], '/api_jsonrpc.php'),
                               body=zabbix_callbacks)
        host_names = ['sjc02-p02-adb01', 'sjc02-p02-tas01']
        items_names = ['CPU user time (avg1)', 'Memory used %', 'Free disk space on / %', 'CPU system time (avg)']
        end_range = datetime.now()
        start_range = end_range - timedelta(hours=3)
        client = zabbix_accessor.ZabbixAccessor()
        history = client.get_hosts_items_history(host_names,
                                                 items_names,
                                                 start_range,
                                                 end_range)
        self.assertTrue(all([host_name in history for host_name in host_names]))
        self.assertGreater(len(history['sjc02-p02-adb01']['CPU user time (avg1)']), 0)
        self.assertGreater(len(history['sjc02-p02-adb01']['Memory used %']), 0)
        self.assertGreater(len(history['sjc02-p02-adb01']['Free disk space on / %']), 0)
        self.assertGreater(len(history['sjc02-p02-tas01']['Free disk space on / %']), 0)

    @httpretty.activate
    def test_get_all_hosts_names(self):

        def zabbix_callbacks(method, uri, headers):
            body = json.loads(method.body.decode('utf-8'))
            response_body = ''
            if body['method'] == 'user.login':
                response_body = json.dumps({
                    "jsonrpc": "2.0",
                    "result": "0424bd59b807674191e7d77572075f33",
                    "id": 0
                })
            elif body['method'] == 'host.get':
                response_body = json.dumps(
                    {'result': [{u'hostid': u'100100000011503', u'name': u'amz01-west-vdv01'},
                                {u'hostid': u'100100000011906', u'name': u'iad01-c01-cws03'},
                                {u'hostid': u'100100000011740', u'name': u'sjc02-p01-eps01'},
                                {u'hostid': u'100100000011741', u'name': u'sjc02-p01-mon01'}]}

                )
            return 200, headers, response_body
        httpretty.register_uri(httpretty.POST,
                               urljoin(settings.ZABBIXES[0]['host'], '/api_jsonrpc.php'),
                               body=zabbix_callbacks)


        client = zabbix_accessor.ZabbixAccessor()
        hosts = client.get_all_hosts_names()

        self.assertEqual(len(hosts), 4)
"""


class TestLinkDBAccessorTests(TestCase):

    def setUp(self):
        self.prefix = 'PRF'
        # Patching a part of connector
        testlink_accessor.TestLinkDBAccessor()

    def test_get_minor_and_major(self):
        sample = (5, 17)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        result = tc_connector._get_major_and_minor('.'.join(str(x) for x in sample))
        self.assertEqual(sample, result)

    def test_get_all_cases(self):
        # Preparation
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList',
             None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
            (330, 'Get Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage',
             None, datetime(2013, 3, 7, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            (331, 'Get SessionId', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID',
             None, datetime(2013, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            (332, 'Delete Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'DeleteMessage',
             None, datetime(2013, 3, 7, 15, 17, 15), datetime(2013, 10, 23, 19, 57, 56)),
            (333, 'Update Message Status', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync',
             'UpdateMessageStatus', None, datetime(2013, 3, 7, 16, 7, 18), datetime(2013, 10, 23, 19, 47, 6)),
            (334, 'Send fax', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'SendFax',
             None, datetime(2013, 3, 7, 16, 22, 18), datetime(2013, 10, 23, 19, 58, 2)),
            (335, 'Update Phone List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'UpdatePhoneList',
             None, datetime(2013, 3, 7, 16, 24, 42), datetime(2013, 10, 23, 19, 48, 21)),
        )
        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        all_cases = tc_connector.get_all_cases()
        self.assertEqual(len(db_cases), len(all_cases))

    def test_object_filler_case(self):
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList',
             None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
        )

        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        all_cases = tc_connector.get_all_cases()
        src_case = db_cases[0]
        case = all_cases[0]
        self.assertEqual(case.external_key, self.prefix + '-' + str(src_case[0]))
        self.assertEqual(case.title, src_case[1])
        self.assertEqual(case.version, src_case[2])
        self.assertEqual(case.priority, 2)

    def test_get_applicable_for_run(self):
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList',
             None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
            (330, 'Get Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage',
             None, datetime(2013, 3, 7, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            (331, 'Get SessionId', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID',
             None, datetime(2013, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 1 future case
            (331, 'Get SessionId', 2, '2 - High (Acceptance)', '5.11', None, '18', 'MessageSync', 'GetSessionID',
             None, datetime(2015, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 2 deprecated major less
            (332, 'Delete Message', 1, '2 - High (Acceptance)', '5.11', '4.32', '3', 'MessageSync', 'DeleteMessage',
             None, datetime(2013, 3, 7, 15, 17, 15), datetime(2013, 10, 23, 19, 57, 56)),
            # 3 deprecated minor less
            (333, 'Update Message Status', 1, '2 - High (Acceptance)', '5.11', '5.16', '3', 'MessageSync',
             'UpdateMessageStatus', None, datetime(2013, 3, 7, 16, 7, 18), datetime(2013, 10, 23, 19, 47, 6)),
            # 4 deprecated equal
            (334, 'Send fax', 1, '2 - High (Acceptance)', '5.11', '5.17', '3', 'MessageSync', 'SendFax',
             None, datetime(2013, 3, 7, 16, 22, 18), datetime(2013, 10, 23, 19, 58, 2)),
            # 5 applicable since future
            (335, 'Update Phone List', 1, '2 - High (Acceptance)', '6.0', None, '3', 'MessageSync', 'UpdatePhoneList',
             None, datetime(2013, 3, 7, 16, 24, 42), datetime(2013, 10, 23, 19, 48, 21)),
        )

        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        utc_timezone = pytz.timezone('UTC')
        applicable_cases = tc_connector.get_applicable_cases('5.17.000.12',
                                                             datetime(2013, 12, 11).replace(tzinfo=utc_timezone),
                                                             datetime(2013, 12, 12).replace(tzinfo=utc_timezone))
        self.assertEqual(len(db_cases) - 5, len(applicable_cases))

    def test_get_new_cases(self):
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList',
             None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
            (330, 'Get Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage',
             None, datetime(2013, 3, 7, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            (331, 'Get SessionId', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID',
             None, datetime(2013, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 1
            (332, 'Delete Message', 1, '2 - High (Acceptance)', '5.17', None, '3', 'MessageSync', 'DeleteMessage',
             None, datetime(2013, 3, 7, 15, 17, 15), datetime(2013, 10, 23, 19, 57, 56)),
            # Future case
            (333, 'Update Message Status', 1, '2 - High (Acceptance)', '5.17', None, '3', 'MessageSync',
             'UpdateMessageStatus', None, datetime(2015, 3, 7, 16, 7, 18), datetime(2013, 10, 23, 19, 47, 6)),
            # 2
            (334, 'Send fax', 1, '2 - High (Acceptance)', '5.17', None, '3', 'MessageSync', 'SendFax',
             None, datetime(2013, 3, 7, 16, 22, 18), datetime(2013, 10, 23, 19, 58, 2)),
            (335, 'Update Phone List', 1, '2 - High (Acceptance)', '6.0', None, '3', 'MessageSync', 'UpdatePhoneList',
             None, datetime(2013, 3, 7, 16, 24, 42), datetime(2013, 10, 23, 19, 48, 21)),
        )

        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        new_cases = tc_connector.get_new_cases('5.17.000.12',
                                               datetime(2013, 12, 11).replace(tzinfo=pytz.timezone('UTC')),
                                               datetime(2013, 12, 12).replace(tzinfo=pytz.timezone('UTC')))
        self.assertEqual(2, len(new_cases))

    def test_get_deprecated_cases(self):
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList',
             None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
            (330, 'Get Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage',
             None, datetime(2013, 3, 7, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            (331, 'Get SessionId', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID',
             None, datetime(2013, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 1 deprecated major less
            (332, 'Delete Message', 1, '2 - High (Acceptance)', '5.11', '4.32', '3', 'MessageSync', 'DeleteMessage',
             None, datetime(2013, 3, 7, 15, 17, 15), datetime(2013, 10, 23, 19, 57, 56)),
            # 2 deprecated minor less
            (333, 'Update Message Status', 1, '2 - High (Acceptance)', '5.11', '5.16', '3', 'MessageSync',
             'UpdateMessageStatus', None, datetime(2013, 3, 7, 16, 7, 18), datetime(2013, 10, 23, 19, 47, 6)),
            # 3 deprecated equal
            (334, 'Send fax', 1, '2 - High (Acceptance)', '5.11', '5.17', '3', 'MessageSync', 'SendFax',
             None, datetime(2013, 3, 7, 16, 22, 18), datetime(2013, 10, 23, 19, 58, 2)),
            # future case
            (335, 'Update Phone List', 1, '2 - High (Acceptance)', '5.11', '5.16', '3', 'MessageSync',
             'UpdatePhoneList', None, datetime(2015, 3, 7, 16, 24, 42), datetime(2013, 10, 23, 19, 48, 21)),
        )

        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        utc_timezone = pytz.timezone('UTC')
        deprecated_cases = tc_connector.get_deprecated_cases('5.17.000.12',
                                                             datetime(2013, 12, 11).replace(tzinfo=utc_timezone),
                                                             datetime(2013, 12, 12).replace(tzinfo=utc_timezone))
        self.assertEqual(3, len(deprecated_cases))

    def test_latest_version(self):
        db_cases = (
            (329, 'Get Message List', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessagesList', None, datetime(2013, 3, 6, 12, 30, 47), datetime(2013, 10, 23, 19, 58, 50)),
            # 1
            (330, 'Get Message', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage', None, datetime(2013, 3, 7, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            (330, 'Get Message', 2, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetMessage', None, datetime(2013, 3, 8, 13, 2, 34), datetime(2013, 10, 23, 19, 45, 19)),
            # 2
            (331, 'Get SessionId', 1, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 7, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 3
            (331, 'Get SessionId', 2, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 8, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 4
            (331, 'Get SessionId', 3, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 9, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 5
            (331, 'Get SessionId', 4, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 10, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 6
            (331, 'Get SessionId', 5, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 11, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            (331, 'Get SessionId', 6, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2013, 3, 12, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
            # 7 - Future version
            (331, 'Get SessionId', 7, '2 - High (Acceptance)', '5.11', None, '3', 'MessageSync', 'GetSessionID', None, datetime(2015, 3, 12, 15, 7, 5), datetime(2013, 10, 23, 19, 58, 24)),
        )
        testlink_accessor.TestLinkDBAccessor._get_absolutely_all_cases = MagicMock(return_value=db_cases)
        tc_connector = testlink_accessor.TestLinkDBAccessor()
        tc_connector.proj_prefix = self.prefix
        all_cases = tc_connector.get_all_cases()
        self.assertEqual(len(db_cases), len(all_cases))
        utc_tz = pytz.timezone('UTC')
        applicable_cases = tc_connector.get_applicable_cases('5.17.000.12',
                                                             datetime(2013, 12, 11).replace(tzinfo=utc_tz),
                                                             datetime(2013, 12, 12).replace(tzinfo=utc_tz))
        self.assertEqual(len(db_cases) - 7, len(applicable_cases))
        for case in applicable_cases:
            if case.title == 'Get SessionId':
                self.assertEqual(case.version, 6)
