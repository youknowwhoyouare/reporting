import time
import logging
logger = logging.getLogger(__name__)

from django.conf import settings
from django.db.models.loading import get_model
from django.core.exceptions import ObjectDoesNotExist

from pytz import timezone

#import testlink
#from testlink import testlinkerrors
import pymysql
import re
#
# class TestLinkAccessor(object):
#     """Accessor class implements all test case management tool related logic
#     for matching cases for measurements"""
#
#     def __init__(self, *args, **kwargs):
#         server_url = settings.TESTCASEMGMT_URL
#         api_key = settings.TESTCASEMGMT_API_KEY
#         self.client = testlink.TestlinkAPIClient(server_url, api_key)
#         self.project_name = 'Performance Test Cases'
#         self.project_id = self.client.getProjectIDByName(self.project_name)
#         self.errors_list = []
#s
#     def _fill_testcase_dict_custom_fields(self, testcase_dict):
#         priority = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=testcase_dict['external_key'],
#                                                                  version=testcase_dict['version'],
#                                                                  customfieldname='Priority',
#                                                                  testprojectid=self.project_id)
#         sla = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=testcase_dict['external_key'],
#                                                             version=testcase_dict['version'],
#                                                             customfieldname='SLA',
#                                                             testprojectid=self.project_id)
#         testcase_dict['priority'] = int(priority.split(' ')[0])
#         testcase_dict['sla'] = float(sla)
#         return testcase_dict
#
#     def _get_all_cases(self):
#         suites = self.client.getFirstLevelTestSuitesForTestProject(self.project_id)
#         suite_ids = [x['id'] for x in suites]
#         raw_cases = []
#         for suite_id in suite_ids:
#             cases = self.client.getTestCasesForTestSuite(testsuiteid=suite_id,
#                                                          details='full')
#             raw_cases.extend(cases)
#         return raw_cases
#
#     def _format_raw_cases(self, raw_cases):
#         formatted_cases = []
#         TestCase = get_model('measurements', 'TestCase')
#         Component = get_model('apps', 'Component')
#         for case in raw_cases:
#             testcase_dict = {'external_key': case['external_id'],
#                              'version': int(case['version']),
#                              'title': case['name']}
#             try:
#                 testcase_dict = self._fill_testcase_dict_custom_fields(testcase_dict)
#             except testlinkerrors.TestLinkError as err:
#                 self.errors_list.append(str(err))
#                 continue
#             testcase_obj = TestCase(**testcase_dict)
#             formatted_cases.append(testcase_obj)
#         return formatted_cases
#
#     @staticmethod
#     def _get_major_and_minor(release_short_name):
#         return int(release_short_name.split('.')[0]), int(release_short_name.split('.')[1])
#
#     def get_applicable_cases(self, release_short_name, start_range, end_range):
#         applicable_cases = self._get_applicable_cases(release_short_name)
#         for case in applicable_cases:
#             case.key_field = self._get_case_custom_field(case, 'Key')
#             parameters = self._get_case_custom_field(case, 'Case_Parameters')
#             # TODO: add deserializtion to dict
#             case.parameters = parameters
#         return applicable_cases
#
#     def _get_case_custom_field(self, case, field_name):
#         """Gets custom field value for formatted case dict"""
#         try:
#             value = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=case.external_key,
#                                                                   version=case.version,
#                                                                   customfieldname=field_name,
#                                                                   testprojectid=self.project_id)
#             return value
#         except testlinkerrors.TLResponseError as err:
#             self.errors_list.append(str(err))
#             return None
#
#     def _get_applicable_cases(self, release_short_name):
#         release_major, release_minor = self._get_major_and_minor(release_short_name)
#         raw_cases = self._get_all_cases()
#         applicable_cases = []
#         # 'Available since (version)'
#         for case in raw_cases:
#             try:
#                 deprecated_since = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=case['external_id'],
#                                                                                  version=int(case['version']),
#                                                                                  customfieldname='Deprecated since version',
#                                                                                  testprojectid=self.project_id)
#                 deprecated_major, deprecated_minor = self._get_major_and_minor(deprecated_since)
#                 if release_major > deprecated_major or (release_major == deprecated_major
#                                                         and release_minor > deprecated_minor):
#                     continue
#             except testlinkerrors.TLResponseError as err:
#                 if err.message != 'Empty Response!':
#                     self.errors_list.append(str(err))
#             try:
#                 available_since = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=case['external_id'],
#                                                                                 version=int(case['version']),
#                                                                                 customfieldname='Available since version',
#                                                                                 testprojectid=self.project_id)
#                 available_major, available_minor = self._get_major_and_minor(available_since)
#                 if release_major < available_major or (release_major == available_major
#                                                        and release_minor < available_minor):
#                     continue
#                 applicable_cases.append(case)
#             except testlinkerrors.TLResponseError as err:
#                 self.errors_list.append(str(err))
#                 continue
#         return self._format_raw_cases(applicable_cases)
#
#     def get_new_cases(self, release_short_name):
#         raw_cases = self._get_all_cases()
#         new_cases = []
#         release_major, release_minor = self._get_major_and_minor(release_short_name)
#         for case in raw_cases:
#             try:
#                 available_since = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=case['external_id'],
#                                                                                 version=int(case['version']),
#                                                                                 customfieldname='Available since version',
#                                                                                 testprojectid=self.project_id)
#                 available_major, available_minor = self._get_major_and_minor(available_since)
#                 if available_major == release_major and available_minor == release_minor:
#                     new_cases.append(case)
#             except testlinkerrors.TLResponseError as err:
#                 self.errors_list.append(str(err))
#                 continue
#         return self._format_raw_cases(new_cases)
#
#     def get_deprecated_cases(self, release_short_name):
#         raw_cases = self._get_all_cases()
#         deprecated_cases = []
#         release_major, release_minor = self._get_major_and_minor(release_short_name)
#         for case in raw_cases:
#             try:
#                 deprecated_since = self.client.getTestCaseCustomFieldDesignValue(testcaseexternalid=case['external_id'],
#                                                                                  version=int(case['version']),
#                                                                                  customfieldname='Deprecated since version',
#                                                                                  testprojectid=self.project_id)
#                 derprecated_major, depreacted_minor = self._get_major_and_minor(deprecated_since)
#                 if derprecated_major <= release_major or ( derprecated_major == release_major
#                                                            and depreacted_minor <= release_minor):
#                     deprecated_cases.append(case)
#             except testlinkerrors.TLResponseError as err:
#                 self.errors_list.append(str(err))
#                 continue
#         return self._format_raw_cases(deprecated_cases)
#
#     def get_all_cases(self):
#         raw_cases = self._get_all_cases()
#         return self._format_raw_cases(raw_cases)


class TestLinkDBAccessor(object):

    def __init__(self, *args, **kwargs):
        self.timezone = timezone('UTC')
        self.project_name = 'Performance Test Cases'
        self.testcase_model = get_model('measurements', 'TestCase')
        self.app_model = get_model('apps', 'Component')
        self._get_connection()
        self._fill_proj_prefix()

    def _get_connection(self):
        self.conn = pymysql.connect(host=settings.TESTLINK_SERVER,
                                    port=3306,
                                    user=settings.TESTLINK_DBUSER,
                                    passwd=settings.TESTLINK_DBPASS,
                                    db=settings.TESTLINK_DBNAME)

    def _fill_proj_prefix(self):
        cursor = self.conn.cursor()
        cursor.execute("""SELECT testlink.testprojects.prefix
FROM testlink.testprojects, testlink.nodes_hierarchy
WHERE
testlink.testprojects.id = testlink.nodes_hierarchy.id
and testlink.nodes_hierarchy.name = %s """, self.project_name)
        self.proj_prefix = cursor.fetchall()[0][0]

    @staticmethod
    def _get_major_and_minor(release_short_name):
        release_short_name = re.sub("[!^a-zA-Z\(\)]","", release_short_name)
        return int(release_short_name.split('.')[0]), int(release_short_name.split('.')[1])

    @staticmethod
    def _is_deprecated(current_major, current_minor, deprecated_major, deprecated_minor):
        if current_major > deprecated_major \
            or (current_major == deprecated_major and current_minor > deprecated_minor) \
                or (current_major == deprecated_major and current_minor == deprecated_minor):
            return True
        else:
            return False

    def _fetch_all_cases(self):
        get_all_cases_sql = """
                select tc_external_id,
               base_case.name,
               version,
               priority,
               available_since,
               deprecated_since,
               sla as sla_sec,
               component,
               key_field,
               case_parameters summary,
               creation_ts,
               modification_ts
        from testlink.nodes_hierarchy
        join testlink.tcversions on testlink.nodes_hierarchy.id = testlink.tcversions.id
        join
            (select t.node_id,
                    group_concat(t.priority) priority,
                    group_concat(t.available_since) available_since,
                    group_concat(t.deprecated_since) deprecated_since,
                    group_concat(t.sla) sla,
                    group_concat(t.key_field) key_field,
                    group_concat(t.case_parameters) case_parameters,
                    group_concat(t.component) component
             from
                 (select node_id,
                     case field_id when 16 then value end as priority,
                     case field_id when 39 then value end as available_since,
                     case field_id when 43 then value end as deprecated_since,
                     case field_id when 40 then value end as sla,
                     case field_id when 48 then value end as key_field,
        case field_id when 47 then value end as case_parameters,
                                                case field_id when 44 then value end as component
                  from testlink.cfield_design_values) t
             group by t.node_id) b on b.node_id = testlink.nodes_hierarchy.id
        join testlink.nodes_hierarchy as base_case on base_case.id = testlink.nodes_hierarchy.parent_id
        where testlink.nodes_hierarchy.parent_id in
        (select id
         from testlink.nodes_hierarchy
         where testlink.nodes_hierarchy.node_type_id = 3
             and testlink.nodes_hierarchy.parent_id in
                 (select testlink.nodes_hierarchy.id
                  from testlink.testsuites,
                       testlink.nodes_hierarchy
                  where testlink.testsuites.id = testlink.nodes_hierarchy.id
                      and testlink.nodes_hierarchy.parent_id in
                          (select testlink.nodes_hierarchy.id
                           from testlink.testprojects,
                                testlink.nodes_hierarchy
                           where testlink.testprojects.id = testlink.nodes_hierarchy.id
                               and testlink.nodes_hierarchy.name = %s )))
        """
        cursor = self.conn.cursor()
        cursor.execute(get_all_cases_sql, self.project_name)
        return cursor.fetchall()

    def _get_absolutely_all_cases(self):
        try:
            return self._raw_rows
        except AttributeError:
            raw_rows = self._fetch_all_cases()
            self._raw_rows = raw_rows
            return self._raw_rows

    def _get_case_obj_from_row(self, row):

        external_key = '-'.join((self.proj_prefix, str(row[0])))
        version = row[2]
        title = row[1]
        try:
            sla = float(row[6]) * 1000
        except TypeError:
            logger.info('Invalid SLA field value in TC: {testcase}. Using default value'.format(testcase=external_key))
            sla = 3000.0
        try:
            priority = int(row[3][0])
        except TypeError:
            logger.info('Invalid priority field value in TC: {testcase}. Using default value'.format(testcase=external_key))
            priority = 2
        component_name = row[7]
        try:
            component = self.app_model.objects.get(description__icontains=component_name)
            case = self.testcase_model(external_key=external_key,
                                       version=version,
                                       title=title,
                                       sla=sla,
                                       priority=priority,
                                       component=component)
        except (ObjectDoesNotExist, ValueError):
            case = self.testcase_model(external_key=external_key,
                                       version=version,
                                       title=title,
                                       sla=sla,
                                       priority=priority)
        return case

    def get_all_cases(self):
        """ Return list of TestCase objects which could be saved in database
        """
        raw_rows = self._get_absolutely_all_cases()
        return [self._get_case_obj_from_row(row) for row in raw_rows]

    def get_applicable_cases(self, release_short_name, start_range, end_range, components = None):
        """Return all cases, which can be tested in given release

        It means that
        * cases become available in this or previous releases
        * cases doesn't become deprecated in this or previous releases
        * case or case version wasn't create after given time range
        """
        start_time = time.time()
        logger.info('Fetching applicable cases for {release} on {timestamp}'.format(release=release_short_name,
                                                                                    timestamp=start_range))
        applicable_cases = []
        release_major, release_minor = self._get_major_and_minor(release_short_name)
        for row in self._get_absolutely_all_cases():
            available_since = row[4]
            available_major, available_minor = self._get_major_and_minor(available_since)
            # release version is not lower than available since
            if release_major < available_major or (release_major == available_major
                                                   and release_minor < available_minor):
                continue
            deprecated_since = row[5]
            # release version is not greater or equal than deprecated since
            if deprecated_since:
                deprecated_major, deprecated_minor = self._get_major_and_minor(deprecated_since)
                if self._is_deprecated(release_major, release_minor, deprecated_major, deprecated_minor):
                    continue
            created = row[10].replace(tzinfo=self.timezone)
            # Case is not from future
            if created > start_range or created > end_range:
                continue
            case = self._get_case_obj_from_row(row)
            case.key_field = row[8]
            # TODO: add conversion to dictionary
            case.parameters = row[9]
            applicable_cases.append(case)
        logger.info('Applicable cases fetched for {n} seconds'.format(n = time.time() - start_time))
        return self._filter_latest_versions(applicable_cases,components)

    def get_new_cases(self, release_short_name, start_range, end_range):
        """Return cases, which become available in this version is equal and applicable for the release"""
        release_major, release_minor = self._get_major_and_minor(release_short_name)
        raw_rows = self._get_absolutely_all_cases()
        new_cases = []
        for row in raw_rows:
            available_since = row[4]
            available_major, available_minor = self._get_major_and_minor(available_since)
            if release_major == available_major and release_minor == available_minor:
                # Check that version isn't from future
                created = row[10].replace(tzinfo=self.timezone)
                if created < start_range or created < end_range:
                    case = self._get_case_obj_from_row(row)
                    new_cases.append(case)
        return self._filter_latest_versions(new_cases)

    def get_deprecated_cases(self, release_short_name, start_range, end_range):
        """ Return all cases, that is deprecated at given release"""
        release_major, release_minor = self._get_major_and_minor(release_short_name)
        raw_rows = self._get_absolutely_all_cases()
        deprecated_cases = []
        for row in raw_rows:
            created = row[10].replace(tzinfo=self.timezone)
            # Check that version isn't from future
            if created < start_range or created < end_range:
                deprecated_since = row[5]
                if deprecated_since:
                    deprecated_major, deprecated_minor = self._get_major_and_minor(deprecated_since)
                    if self._is_deprecated(release_major, release_minor, deprecated_major, deprecated_minor):
                        case = self._get_case_obj_from_row(row)
                        deprecated_cases.append(case)
        return self._filter_latest_versions(deprecated_cases)

    @staticmethod
    def _filter_latest_versions(cases,components = None):
        latest_cases = {}
        for case in cases:
            if components:
                if case.component in components:
                    if case.external_key in latest_cases:
                        if case.version > latest_cases[case.external_key].version:
                            latest_cases[case.external_key] = case
                    else:
                        latest_cases[case.external_key] = case
            else:
                if case.external_key in latest_cases:
                    if case.version > latest_cases[case.external_key].version:
                        latest_cases[case.external_key] = case
                else:
                    latest_cases[case.external_key] = case
        return latest_cases.values()

