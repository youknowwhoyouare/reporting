from django.db import models


class Vendor(models.Model):
    name = models.CharField(max_length=120)
    description = models.CharField(max_length=120)

    def __str__(self):
        return self.name


class App(models.Model):
    """ Application group is a group of application from one vendor
    and under same versioning
    """
    name = models.CharField(max_length=120, unique=True)
    description = models.CharField(max_length=1000, blank=True)
    vendor = models.ForeignKey(Vendor, null=True)

    def __str__(self):
        return self.name


class Component(models.Model):
    """Component is a logical part of application group"""
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=120)
    app = models.ForeignKey(App)

    def __str__(self):
        return self.name


class Branch(models.Model):
    name = models.CharField(max_length=60)
    date_created = models.DateField()

    class Meta:
        verbose_name_plural = 'branches'
        ordering = ['-date_created']

    def __str__(self):
        return self.name


class Build(models.Model):
    version = models.CharField(max_length=60)
    build_date = models.DateField()
    branch = models.ForeignKey(Branch)
    app = models.ForeignKey(App)

    class Meta:
        ordering = ['-build_date', 'version']
        # There are can't be two identical versions in one application groups
        unique_together = ('version', 'app')

    def __str__(self):
        return self.version


class Release(models.Model):
    """Release is a set of builds of different apps groups

    Main build shows as release build
    """
    name = models.CharField(max_length=60)
    create_date = models.DateField()
    main_build = models.ForeignKey(Build, related_name='main_build')
    other_builds = models.ManyToManyField(Build, blank=True, null=True)

    def get_short_release_name(self):
        return '.'.join(self.main_build.version.split('.')[:2])

    def __str__(self):
        return self.name
