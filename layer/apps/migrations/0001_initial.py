# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AppGroup'
        db.create_table(u'apps_appgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, blank=True)),
        ))
        db.send_create_signal(u'apps', ['AppGroup'])

        # Adding model 'App'
        db.create_table(u'apps_app', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.AppGroup'])),
        ))
        db.send_create_signal(u'apps', ['App'])

        # Adding model 'Branch'
        db.create_table(u'apps_branch', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('date_created', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'apps', ['Branch'])

        # Adding model 'Build'
        db.create_table(u'apps_build', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('release_date', self.gf('django.db.models.fields.DateField')()),
            ('branch', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Branch'])),
            ('app_group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.AppGroup'])),
        ))
        db.send_create_signal(u'apps', ['Build'])

        # Adding unique constraint on 'Build', fields ['version', 'app_group']
        db.create_unique(u'apps_build', ['version', 'app_group_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Build', fields ['version', 'app_group']
        db.delete_unique(u'apps_build', ['version', 'app_group_id'])

        # Deleting model 'AppGroup'
        db.delete_table(u'apps_appgroup')

        # Deleting model 'App'
        db.delete_table(u'apps_app')

        # Deleting model 'Branch'
        db.delete_table(u'apps_branch')

        # Deleting model 'Build'
        db.delete_table(u'apps_build')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'release_date': ('django.db.models.fields.DateField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['apps']