# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        db.delete_foreign_key('apps_releasegroup_other_builds', 'releasegroup_id')
        db.delete_index('apps_releasegroup_other_builds', ['releasegroup_id'])
        db.delete_unique('apps_releasegroup_other_builds', ['releasegroup_id', 'build_id'])
        db.delete_primary_key('apps_releasegroup_other_builds')

        db.delete_foreign_key('measurements_run', 'tested_release_id')

        db.delete_primary_key('apps_releasegroup')
        db.delete_index('apps_releasegroup', ['main_build_id'])

        db.rename_table('apps_releasegroup', 'apps_release')

        db.create_index('apps_release', ['main_build_id'])
        db.create_primary_key('apps_release', ['id'])

        db.rename_table('apps_releasegroup_other_builds', 'apps_release_other_builds')
        db.rename_column('apps_release_other_builds', 'releasegroup_id', 'release_id')

        db.create_primary_key('apps_release_other_builds', ['id'])
        db.create_unique('apps_release_other_builds', ['release_id', 'build_id'])
        db.create_index('apps_release_other_builds', ['release_id'])
        db.alter_column('apps_release_other_builds', 'release_id', models.ForeignKey(to=orm['apps.Release']))
        db.alter_column('measurements_run', 'tested_release_id', models.ForeignKey(to=orm['apps.Release']))

    def backwards(self, orm):
        db.delete_foreign_key('apps_release_other_builds', 'release_id')
        db.delete_index('apps_release_other_builds', ['release_id'])
        db.delete_unique('apps_release_other_builds', ['release_id', 'build_id'])
        db.delete_primary_key('apps_release_other_builds')

        db.delete_foreign_key('measurements_run', 'tested_release_id')

        db.delete_primary_key('apps_release')
        db.delete_index('apps_release', ['main_build_id'])

        db.rename_table('apps_release', 'apps_releasegroup')

        db.create_index('apps_releasegroup', ['main_build_id'])
        db.create_primary_key('apps_releasegroup', ['id'])

        db.rename_table('apps_release_other_builds', 'apps_releasegroup_other_builds')
        db.rename_column('apps_releasegroup_other_builds', 'release_id', 'releasegroup_id')

        db.create_primary_key('apps_releasegroup_other_builds', ['id'])
        db.create_unique('apps_releasegroup_other_builds', ['releasegroup_id', 'build_id'])
        db.create_index('apps_releasegroup_other_builds', ['releasegroup_id'])
        db.alter_column('apps_releasegroup_other_builds', 'releasegroup_id', models.ForeignKey(to=orm['apps.ReleaseGroup']))
        db.alter_column('measurements_run', 'tested_release_id', models.ForeignKey(to=orm['apps.ReleaseGroup']))

    models = {
        'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'blank': 'True', 'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Vendor']", 'null': 'True'})
        },
        'apps.branch': {
            'Meta': {'object_name': 'Branch', 'ordering': "['-date_created']"},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.build': {
            'Meta': {'object_name': 'Build', 'unique_together': "(('version', 'app_group'),)", 'ordering': "['-build_date', 'version']"},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.AppGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        'apps.release': {
            'Meta': {'object_name': 'Release'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.Build']", 'related_name': "'main_build'"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['apps.Build']", 'symmetrical': 'False', 'blank': 'True', 'null': 'True'})
        },
        'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['apps']