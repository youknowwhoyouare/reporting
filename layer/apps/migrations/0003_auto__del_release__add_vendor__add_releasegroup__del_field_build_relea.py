# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Release'
        db.delete_table(u'apps_release')

        # Removing M2M table for field builds on 'Release'
        db.delete_table(db.shorten_name(u'apps_release_builds'))

        # Adding model 'Vendor'
        db.create_table(u'apps_vendor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=120)),
        ))
        db.send_create_signal(u'apps', ['Vendor'])

        # Adding model 'ReleaseGroup'
        db.create_table(u'apps_releasegroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('create_date', self.gf('django.db.models.fields.DateField')()),
            ('main_build', self.gf('django.db.models.fields.related.ForeignKey')(related_name='main_build', to=orm['apps.Build'])),
        ))
        db.send_create_signal(u'apps', ['ReleaseGroup'])

        # Adding M2M table for field other_builds on 'ReleaseGroup'
        m2m_table_name = db.shorten_name(u'apps_releasegroup_other_builds')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('releasegroup', models.ForeignKey(orm[u'apps.releasegroup'], null=False)),
            ('build', models.ForeignKey(orm[u'apps.build'], null=False))
        ))
        db.create_unique(m2m_table_name, ['releasegroup_id', 'build_id'])

        # Deleting field 'Build.release_date'
        db.delete_column(u'apps_build', 'release_date')

        # Adding field 'Build.build_date'
        db.add_column(u'apps_build', 'build_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 2, 0, 0)),
                      keep_default=False)

        # Adding field 'AppGroup.vendor'
        db.add_column(u'apps_appgroup', 'vendor',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['apps.Vendor'], null=True),
                      keep_default=False)

    def backwards(self, orm):
        # Adding model 'Release'
        db.create_table(u'apps_release', (
            ('release_date', self.gf('django.db.models.fields.DateField')()),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'apps', ['Release'])

        # Adding M2M table for field builds on 'Release'
        m2m_table_name = db.shorten_name(u'apps_release_builds')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('release', models.ForeignKey(orm[u'apps.release'], null=False)),
            ('build', models.ForeignKey(orm[u'apps.build'], null=False))
        ))
        db.create_unique(m2m_table_name, ['release_id', 'build_id'])

        # Deleting model 'Vendor'
        db.delete_table(u'apps_vendor')

        # Deleting model 'ReleaseGroup'
        db.delete_table(u'apps_releasegroup')

        # Removing M2M table for field other_builds on 'ReleaseGroup'
        db.delete_table(db.shorten_name(u'apps_releasegroup_other_builds'))

        # Adding field 'Build.release_date'
        db.add_column(u'apps_build', 'release_date',
                      self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 11, 2, 0, 0)),
                      keep_default=False)

        # Deleting field 'Build.build_date'
        db.delete_column(u'apps_build', 'build_date')

        # Deleting field 'AppGroup.vendor'
        db.delete_column(u'apps_appgroup', 'vendor_id')


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Vendor']", 'null': 'True'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.releasegroup': {
            'Meta': {'object_name': 'ReleaseGroup'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_build'", 'to': u"orm['apps.Build']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.Build']", 'null': 'True', 'blank': 'True'})
        },
        u'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['apps']