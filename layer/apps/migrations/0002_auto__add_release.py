# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ('measurements', '0001_initial'),
    )

    def forwards(self, orm):
        # Adding model 'Release'
        db.create_table(u'apps_release', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('release_date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'apps', ['Release'])

        # Adding M2M table for field builds on 'Release'
        m2m_table_name = db.shorten_name(u'apps_release_builds')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('release', models.ForeignKey(orm[u'apps.release'], null=False)),
            ('build', models.ForeignKey(orm[u'apps.build'], null=False))
        ))
        db.create_unique(m2m_table_name, ['release_id', 'build_id'])


    def backwards(self, orm):
        # Deleting model 'Release'
        db.delete_table(u'apps_release')

        # Removing M2M table for field builds on 'Release'
        db.delete_table(db.shorten_name(u'apps_release_builds'))


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'release_date': ('django.db.models.fields.DateField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.release': {
            'Meta': {'object_name': 'Release'},
            'builds': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.Build']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'release_date': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['apps']