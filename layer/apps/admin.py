from django.contrib import admin

from . import models


class BuildAdmin(admin.ModelAdmin):
    list_display = ('version', 'branch', 'build_date')
    list_filter = ('branch', 'app', 'build_date')

    class Meta:
        model = models.Build


class BuildInline(admin.TabularInline):
    model = models.Build

class BranchAdmin(admin.ModelAdmin):
    inlines = [ BuildInline ]

    class Meta:
        model = models.Branch


admin.site.register(models.App)
admin.site.register(models.Component)
admin.site.register(models.Branch, BranchAdmin)
admin.site.register(models.Build, BuildAdmin)
admin.site.register(models.Release)
admin.site.register(models.Vendor)
