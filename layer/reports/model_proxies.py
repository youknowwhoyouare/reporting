from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from django.utils.datastructures import SortedDict
from django.core import serializers
from django.core.urlresolvers import reverse

from . import models
from apps import models as app_models
from environments import models as env_models
from measurements import models as meas_models


class TestProxy(models.Test):
    class Meta:
        proxy = True

    def get_test_plan_url_href(self):
        url = ""
        if self.test_plan:
            url = self.test_plan.url
            icon_class = url.split(".")[-1]
            url = reverse('reports:test_plan_redirect', kwargs={'jira_key':self.jira_key})
        else:
            url = self.test_plan_external_link
            icon_class = "jira"
        if not url:
            return "N/A"
        return "<a class=\"%s\" href=\"%s\"><i/></a>" % (icon_class, url)

    def get_servers_with_metrics(self):
        if not hasattr(self, 'page_id'):
            self.page_id = 'none'
        servers = self.main_run.get_loaded_servers(server_model=ServerProxy, metric_model=ServerMetricProxy)
        for server in servers:
            server.run_test = self
        return servers


class ServerProxy(env_models.Server):
    class Meta:
        proxy = True

    def get_available_metrics(self):
        for metric in self.available_metrics:
            metric.run_server = self
        return self.available_metrics


class ServerMetricProxy(meas_models.ServerMetric):
    class Meta:
        proxy = True

    def get_chart_settings_url(self):
        test = self.run_server.run_test
        url = super(ServerMetricProxy, self).get_chart_settings_url()
        return url + ("?page_id=%s&server_id=%s&run_id=%s"
                      % (test.page_id, self.run_server.id, test.main_run.id))

    def get_chart_data_url(self):
        test = self.run_server.run_test
        metric_cfg = self.get_metric_transform(test.page_id)
        filter = metric_cfg.get_filter_suffix()
        url = reverse('measurements:server_side_json_results',
                       kwargs={'server_name': self.run_server.name, 'metric_id': self.id, 'run_id': test.main_run.id })
        if filter:
            return url +'?' + filter
        return url
