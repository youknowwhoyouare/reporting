from django.contrib import admin
from django.views.decorators.cache import never_cache

from . import models


class TestLogEntryInline(admin.TabularInline):
    model = models.TestLogEntry


class TestParticipantInline(admin.TabularInline):
    model = models.TestParticipant


class TestAttachmentInline(admin.TabularInline):
    model = models.Attachment


class TestAdmin(admin.ModelAdmin):
    fieldsets = (
        ('General', {'fields': ('jira_key', 'title', 'goal', 'status')}),
        ('Detailed', {'fields': ('section', 'requester','start_date','end_date', 'main_run', 'runs', 'conclusion',
                                 'test_plan', 'test_plan_external_link')}),
        ('Additional', {'classes': ('collapse',),
                        'fields': ('related', 'tags', 'template', 'external_report_link')})
    )
    inlines = [TestLogEntryInline,
               TestParticipantInline,
               TestAttachmentInline]

    list_display = ('__str__', 'section', 'status', 'main_run')
    list_filter = ('status', 'section', 'requester')
    search_fields = ('title', 'goal', 'conclusion')

    class Meta:
        model = models.Test


admin.site.register(models.Tag)
admin.site.register(models.TestStatus)
admin.site.register(models.Project)
admin.site.register(models.Section)
admin.site.register(models.ReportTemplate)
admin.site.register(models.Test, TestAdmin)
