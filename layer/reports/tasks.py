# Standard library
import csv
import sys
csv.field_size_limit(sys.maxsize) # Set this for the big CSV files


# Django modules

# 3rd party modules
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

# my modules
from . import models
from external_data.tests import accessor as tests_accessor
from . import celery_app

@celery_app.task
def import_tests(test_section):
    update_results = []
    ts_accessor = tests_accessor.JiraAccessor()
    tests = ts_accessor.get_prepared_tests()
    for x in tests:
        test, created,updated = models.Test.extra_manager.update_or_create(jira_key=x.jira_key,
                                                             defaults={'title': x.title,
                                                             'requester':x.requester,
                                                             'status':x.status,
                                                             'section':test_section}
        )

        update_results.append(test.jira_key)
        # logger.info('Case {tc} currently is not '
        #                 'supportedos it will be ignored'.format(tc=' '.join((case.external_key, case.title))))
    return update_results

