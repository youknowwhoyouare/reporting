import json
import logging

import django.views.generic as generic_views
from django.db.models import Q
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from django.utils.datastructures import SortedDict
from django.core import serializers
from django.core.urlresolvers import reverse

from . import models
from . import model_proxies
from measurements import models as meas_models
from . import tasks
from . import forms
logger = logging.getLogger(__name__)


class ProjectsListView(generic_views.ListView):
    template_name = 'projects.html'
    model = models.Project


class ProjectOverview(generic_views.DetailView):
    model = models.Project
    template_name = 'project_overview.html'
    context_object_name = 'project'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, name=self.kwargs['project_name'])


class ProjectHelpView(ProjectOverview):
    template_name = 'help_page.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectHelpView, self).get_context_data(**kwargs)
        project = self.get_object()
        context['breadcrumbs'] = (project,)
        context['help_image'] = 'help-project-desc.png'
        return context

class ProjectStatsView(generic_views.ListView):
    model = meas_models.ClientMetric
    template_name = 'project_stats.html'

    def get_context_data(self, **kwargs):
        context = super(ProjectStatsView, self).get_context_data(**kwargs)
        context['project_name'] = self.kwargs['project_name']
        context['metrics_list'] = self.model.objects.all()
        return context



class SectionOverview(generic_views.DetailView):
    model = models.Section
    template_name = 'section.html'
    context_object_name = 'section'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model,
                                 name=self.kwargs['section_name'],
                                 project__name=self.kwargs['project_name'])

    def get_context_data(self, **kwargs):
        context = super(SectionOverview, self).get_context_data(**kwargs)
        section = self.get_object()
        years_set = models.Test.objects.filter(section=section).dates('start_date',
                                                                          'year', order='DESC')
        context['years_set'] = years_set
        context['test_status_list'] = models.TestStatus.objects.all()
        return context


class SectionHelpView(ProjectOverview):
    template_name = 'help_page.html'

    def get_context_data(self, **kwargs):
        context = super(SectionHelpView, self).get_context_data(**kwargs)
        section = self.get_object()
        project = get_object_or_404(models.Project, name=self.kwargs['project_name'])
        context['breadcrumbs'] = (project, section)
        context['help_image'] = 'help-app-desc.png'
        return context


class TestsListView(generic_views.ListView):
    template_name = '_tests_list.html'
    model = model_proxies.TestProxy # does it used when get_queryset(self) is overridden here
    context_object_name = 'tests'

    def get_queryset(self):
        section = models.Section.objects.filter(project__name=self.kwargs['project_name'],
                                                name=self.kwargs['section_name'])
        return model_proxies.TestProxy.objects.filter(section=section,
                                          start_date__year=self.kwargs['year'])


class FindTestsSuggestView():

    @staticmethod
    def create_item(kind, data, text=''):
        if kind == 'requester':
            label_prx = 'requested: '
        elif kind == 'tag':
            label_prx = 'tag: '
            text = data
        elif kind == 'test':
            label_prx = data +': '
        else:
            label_prx = 'bad-selector: '
        return {'value': kind +"="+ data, 'label': label_prx + text}

    @staticmethod
    def fetch_items(project_name, text):
        project = get_object_or_404(models.Project, name=project_name)
        data = []
        criteria_for_user = Q(requester__last_name__icontains=text) | Q(requester__username__icontains=text)
        for user in project.get_all_tests().filter(criteria_for_user).values('requester__username', 'requester__last_name').distinct()[0:3]:
            data.append(FindTestsSuggestView.create_item('requester', user['requester__username'], user['requester__last_name']))
        for tag in project.get_top_tags().filter(name__icontains=text)[0:3]:
            data.append(FindTestsSuggestView.create_item('tag', tag.name))
        for test in project.get_all_tests().filter(title__icontains=text)[0:3]:
            data.append(FindTestsSuggestView.create_item('test', test.jira_key, test.title))
        return data

    @staticmethod
    def suggest_list_json(request, project_name='none'):
        #misdatos = misdatos.objects.all()
        #data = serializers.serialize('json', misdatos)
        #return HttpResponse(data, mimetype='application/json')
        '''
        data = [FindTestsSuggestView.create_item('requester', 'ilya.neverov', 'Neverov'),
                FindTestsSuggestView.create_item('tag',       '6.1',          '6.1'),
                FindTestsSuggestView.create_item('test',      'PERF-732',     '5.14: RC1 Combined regression test')]
        '''
        url_params = request.GET.dict()
        data = FindTestsSuggestView.fetch_items(project_name, url_params['term'])
        return HttpResponse(json.dumps(data), content_type = "application/json")


class FoundTestsView(generic_views.DetailView):
    model = models.Section # creates fake section object to use features or section_base.html
    template_name = 'tests_found.html'
    context_object_name = 'section'

    def get_object(self, queryset=None):
        project = get_object_or_404(models.Project,
                                    name=self.kwargs['project_name'])
        section = models.Section()
        section.name = 'Search Results'
        section.project = project
        return section

    def get_context_data(self, **kwargs):
        context = super(FoundTestsView, self).get_context_data(**kwargs)
        url_params = self.request.GET.dict() # see possible params in models.Test.select_tests()
        found_tests = model_proxies.TestProxy.select_tests(self.kwargs['project_name'], url_params) # sorted by start_date desc
        year_tables = SortedDict()
        year = None
        year_tests = []
        def put_year_table(year, year_tests):
            if year_tests:
                http_response = render_to_response(TestsListView.template_name, {TestsListView.context_object_name: year_tests},
                                                  context_instance=RequestContext(self.request))
                year_tables[year] = http_response.content

        for test in found_tests:
            if test.get_test_start_date().year != year:
                put_year_table(year, year_tests)
                year = test.get_test_start_date().year
                year_tests = []
            year_tests.append(test)

        put_year_table(year, year_tests)

        for (kind, text) in url_params.items():
            break
        context['search_request'] = {'kind': kind, 'text': text}
        context['year_tables'] = year_tables
        return context


class TestDetailedView(generic_views.DetailView):
    template_name = 'report_default.html'
    model = model_proxies.TestProxy
    context_object_name = 'test'

    def get_object(self, queryset=None):
        test = get_object_or_404(self.model.objects.select_related('main_run',
                                                                   'section'
                                                                   'section__project'),
                                 #section__project__name=self.kwargs['project_name'],
                                 #section__name=self.kwargs['section_name'],
                                 jira_key=self.kwargs['jira_key'])
        test.page_id = self.__class__.__name__
        return test


class FillTestsView(generic_views.FormView):
    template_name = 'upload.html'
    form_class = forms.SectionSelectionForm

    def get_success_url(self):
        return self.request.get_full_path()

    def form_valid(self, form):
        section = form.cleaned_data['section']
        task = tasks.import_tests(section)
        return render_to_response(self.template_name,
                                  {'task': task},
                                  context_instance=RequestContext(self.request))


class TestShortURLRedirectView(generic_views.RedirectView):
    permanent = False
    query_string = True
    pattern_name = 'reports:test_report'

    def get_attach_url(self, test, kwargs):
        pass

    def get_redirect_url(self, *args, **kwargs):
        test = get_object_or_404(models.Test, jira_key=kwargs['jira_key'])
        url = self.get_attach_url(test, kwargs)
        if url:
            return url
        test.get_absolute_url_pattern(kwargs)
        #return super(TestShortURLRedirectView, self).get_redirect_url(*args, **kwargs)
        return generic_views.RedirectView.get_redirect_url(self, *args, **kwargs)


class TestPlanShortURLRedirectView(TestShortURLRedirectView):
    def get_attach_url(self, test, kwargs):
        return test.get_test_plan_url()


class TestAttachShortURLRedirectView(TestShortURLRedirectView):
    def get_attach_url(self, test, kwargs):
        url_name = kwargs['attach_name']
        for attach in test.get_attachments(): # do select here
            if attach.get_url_name() == url_name:
                return attach.attached_file.url
        del kwargs['attach_name']
        return None
