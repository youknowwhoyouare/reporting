# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models, transaction


class Migration(SchemaMigration):
    depends_on = (
        ('apps', '0004_rename_app_to_component'),
    )

    def forwards(self, orm):
        db.delete_primary_key('reports_section_apps')
        db.delete_index('reports_section_apps', ['app_id'])
        db.delete_unique('reports_section_apps', ['section_id', 'app_id'])

        db.rename_table('reports_section_apps', 'reports_section_components')
        db.rename_column('reports_section_components', 'app_id', 'component_id')

        db.create_unique('reports_section_components', ['section_id', 'component_id'])
        db.create_index('reports_section_components', ['component_id'])
        db.create_primary_key('reports_section_components', ['id'])

    def backwards(self, orm):
        db.delete_primary_key('reports_section_components')
        db.delete_index('reports_section_components', ['component_id'])
        db.delete_unique('reports_section_components', ['section_id', 'component_id'])

        db.rename_column('reports_section_components', 'component_id', 'app_id')
        db.rename_table('reports_section_components', 'reports_section_apps')

        db.create_unique('reports_section_apps', ['section_id', 'app_id'])
        db.create_index('reports_section_apps', ['app_id'])
        db.create_primary_key('reports_section_apps', ['id'])


    models = {
        'apps.component': {
            'Meta': {'object_name': 'Component'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['apps.AppGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'vendor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Vendor']", 'null': 'True'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-build_date', 'version']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            'build_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.releasegroup': {
            'Meta': {'object_name': 'ReleaseGroup'},
            'create_date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_build': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_build'", 'to': u"orm['apps.Build']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'}),
            'other_builds': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['apps.Build']", 'null': 'True', 'blank': 'True'})
        },
        u'apps.vendor': {
            'Meta': {'object_name': 'Vendor'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'environments.hardwareenvironment': {
            'Meta': {'object_name': 'HardwareEnvironment'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'measurements.flag': {
            'Meta': {'object_name': 'Flag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'})
        },
        u'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            'environment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['environments.HardwareEnvironment']"}),
            'flags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['measurements.Flag']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.ReleaseGroup']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        'measurements.testcase': {
            'Meta': {'object_name': 'TestCase', 'unique_together': "(('external_key', 'version'),)"},
            'component': ('django.db.models.fields.related.ForeignKey', [], {'null': 'True', 'blank': 'True', 'to': "orm['apps.Component']"}),
            'external_key': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {}),
            'sla': ('django.db.models.fields.FloatField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'attached_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']"})
        },
        u'reports.issue': {
            'Meta': {'object_name': 'Issue'},
            'furps': ('django.db.models.fields.CharField', [], {'max_length': '60', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'priority': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'run': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['measurements.Run']", 'symmetrical': 'False'}),
            'testcase': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.TestCase']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.project': {
            'Meta': {'object_name': 'Project'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.reporttemplate': {
            'Meta': {'object_name': 'ReportTemplate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'template_name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.section': {
            'Meta': {'object_name': 'Section'},
            'components': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.Component']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Project']"})
        },
        u'reports.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'reports.test': {
            'Meta': {'object_name': 'Test'},
            'conclusion': ('django.db.models.fields.TextField', [], {}),
            'goal': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'main_run': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_run'", 'unique': 'True', 'to': u"orm['measurements.Run']"}),
            'related': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']", 'null': 'True', 'blank': 'True'}),
            'requester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'runs': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['measurements.Run']", 'symmetrical': 'False', 'blank': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Section']"}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.TestStatus']"}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['reports.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.ReportTemplate']", 'null': 'True', 'blank': 'True'}),
            'test_plan': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'test_plan_external_link': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.testlogentry': {
            'Meta': {'object_name': 'TestLogEntry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.CharField', [], {'max_length': '360'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'reports.testparticipant': {
            'Meta': {'object_name': 'TestParticipant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']"}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False'})
        },
        u'reports.teststatus': {
            'Meta': {'object_name': 'TestStatus'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['reports']