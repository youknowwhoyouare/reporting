# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    depends_on = (
        ('apps', '0001_initial'),
    )

    def forwards(self, orm):
        # Adding model 'Tag'
        db.create_table(u'reports_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
        ))
        db.send_create_signal(u'reports', ['Tag'])

        # Adding model 'TestStatus'
        db.create_table(u'reports_teststatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=60)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True)),
        ))
        db.send_create_signal(u'reports', ['TestStatus'])

        # Adding model 'Project'
        db.create_table(u'reports_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
        ))
        db.send_create_signal(u'reports', ['Project'])

        # Adding model 'Section'
        db.create_table(u'reports_section', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Project'])),
        ))
        db.send_create_signal(u'reports', ['Section'])

        # Adding M2M table for field apps on 'Section'
        m2m_table_name = db.shorten_name(u'reports_section_apps')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('section', models.ForeignKey(orm[u'reports.section'], null=False)),
            ('app', models.ForeignKey(orm[u'apps.app'], null=False))
        ))
        db.create_unique(m2m_table_name, ['section_id', 'app_id'])

        # Adding model 'ReportTemplate'
        db.create_table(u'reports_reporttemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('template_name', self.gf('django.db.models.fields.CharField')(max_length=120)),
        ))
        db.send_create_signal(u'reports', ['ReportTemplate'])

        # Adding model 'Test'
        db.create_table(u'reports_test', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('jira_key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('goal', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('conclusion', self.gf('django.db.models.fields.TextField')()),
            ('related', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Test'], null=True, blank=True)),
            ('requester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.ReportTemplate'], null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.TestStatus'])),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Section'])),
            ('main_run', self.gf('django.db.models.fields.related.ForeignKey')(related_name='main_run', unique=True, to=orm['measurements.Run'])),
        ))
        db.send_create_signal(u'reports', ['Test'])

        # Adding M2M table for field runs on 'Test'
        m2m_table_name = db.shorten_name(u'reports_test_runs')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('test', models.ForeignKey(orm[u'reports.test'], null=False)),
            ('run', models.ForeignKey(orm[u'measurements.run'], null=False))
        ))
        db.create_unique(m2m_table_name, ['test_id', 'run_id'])

        # Adding M2M table for field tags on 'Test'
        m2m_table_name = db.shorten_name(u'reports_test_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('test', models.ForeignKey(orm[u'reports.test'], null=False)),
            ('tag', models.ForeignKey(orm[u'reports.tag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['test_id', 'tag_id'])

        # Adding model 'Issue'
        db.create_table(u'reports_issue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('jira_key', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('run', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['measurements.Run'])),
        ))
        db.send_create_signal(u'reports', ['Issue'])

        # Adding model 'TestLogEntry'
        db.create_table(u'reports_testlogentry', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
            ('log', self.gf('django.db.models.fields.CharField')(max_length=360)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Test'])),
        ))
        db.send_create_signal(u'reports', ['TestLogEntry'])

        # Adding model 'TestParticipant'
        db.create_table(u'reports_testparticipant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('test', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reports.Test'])),
        ))
        db.send_create_signal(u'reports', ['TestParticipant'])

        # Adding M2M table for field users on 'TestParticipant'
        m2m_table_name = db.shorten_name(u'reports_testparticipant_users')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('testparticipant', models.ForeignKey(orm[u'reports.testparticipant'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['testparticipant_id', 'user_id'])


    def backwards(self, orm):
        # Deleting model 'Tag'
        db.delete_table(u'reports_tag')

        # Deleting model 'TestStatus'
        db.delete_table(u'reports_teststatus')

        # Deleting model 'Project'
        db.delete_table(u'reports_project')

        # Deleting model 'Section'
        db.delete_table(u'reports_section')

        # Removing M2M table for field apps on 'Section'
        db.delete_table(db.shorten_name(u'reports_section_apps'))

        # Deleting model 'ReportTemplate'
        db.delete_table(u'reports_reporttemplate')

        # Deleting model 'Test'
        db.delete_table(u'reports_test')

        # Removing M2M table for field runs on 'Test'
        db.delete_table(db.shorten_name(u'reports_test_runs'))

        # Removing M2M table for field tags on 'Test'
        db.delete_table(db.shorten_name(u'reports_test_tags'))

        # Deleting model 'Issue'
        db.delete_table(u'reports_issue')

        # Deleting model 'TestLogEntry'
        db.delete_table(u'reports_testlogentry')

        # Deleting model 'TestParticipant'
        db.delete_table(u'reports_testparticipant')

        # Removing M2M table for field users on 'TestParticipant'
        db.delete_table(db.shorten_name(u'reports_testparticipant_users'))


    models = {
        u'apps.app': {
            'Meta': {'object_name': 'App'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.appgroup': {
            'Meta': {'object_name': 'AppGroup'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'})
        },
        u'apps.branch': {
            'Meta': {'ordering': "['-date_created']", 'object_name': 'Branch'},
            'date_created': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'apps.build': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('version', 'app_group'),)", 'object_name': 'Build'},
            'app_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.AppGroup']"}),
            'branch': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Branch']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'release_date': ('django.db.models.fields.DateField', [], {}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'measurements.run': {
            'Meta': {'ordering': "['-timestamp_end']", 'object_name': 'Run'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'tested_build': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['apps.Build']"}),
            'timestamp_end': ('django.db.models.fields.DateTimeField', [], {}),
            'timestamp_start': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'reports.issue': {
            'Meta': {'object_name': 'Issue'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['measurements.Run']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.project': {
            'Meta': {'object_name': 'Project'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.reporttemplate': {
            'Meta': {'object_name': 'ReportTemplate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'template_name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.section': {
            'Meta': {'object_name': 'Section'},
            'apps': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['apps.App']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Project']"})
        },
        u'reports.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        },
        u'reports.test': {
            'Meta': {'object_name': 'Test'},
            'conclusion': ('django.db.models.fields.TextField', [], {}),
            'goal': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jira_key': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'main_run': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'main_run'", 'unique': 'True', 'to': u"orm['measurements.Run']"}),
            'related': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']", 'null': 'True', 'blank': 'True'}),
            'requester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'runs': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['measurements.Run']", 'symmetrical': 'False', 'blank': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Section']"}),
            'status': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.TestStatus']"}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['reports.Tag']", 'symmetrical': 'False', 'blank': 'True'}),
            'template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.ReportTemplate']", 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'reports.testlogentry': {
            'Meta': {'object_name': 'TestLogEntry'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.CharField', [], {'max_length': '360'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']"}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'reports.testparticipant': {
            'Meta': {'object_name': 'TestParticipant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'test': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reports.Test']"}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'symmetrical': 'False'})
        },
        u'reports.teststatus': {
            'Meta': {'object_name': 'TestStatus'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '60'})
        }
    }

    complete_apps = ['reports']