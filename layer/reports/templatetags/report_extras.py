import datetime

from django import template

register = template.Library()


@register.simple_tag
def last_year():
    return datetime.datetime.now().year - 1


@register.filter
def multiply(value, arg):
    try:
        return value * int(arg)
    except TypeError:
        return


@register.filter
def custom_floatformat(value):
    try:
        value = float(value)
        return int(round(value, 0))
    except ValueError:
        return value
    except TypeError:
        return value.__str__()

@register.filter
def format_two_dates(value):
    if type(value) == tuple and len(value) == 2:
        month_format = '%b'
        if (type(value[0]) == datetime.datetime and type(value[1]) == datetime.datetime) or (type(value[0]) == datetime.date and type(value[1]) == datetime.date):
            if value[0].year == value[1].year:
                if value[0].month == value[1].month:
                    if value[0].day == value[1].day:
                        return "%d %s" % (value[0].day, value[0].strftime(month_format))
                    else:
                        return "%d-%d %s" % tuple([x.day for x in value] + [value[0].strftime(month_format)])
                else:
                    return ' - '.join(["%d %s" % (x.day, x.strftime(month_format)) for x in value])
            else:
                return ' - '.join(["%d %s %d" % (x.day, x.strftime(month_format), x.year) for x in value])
