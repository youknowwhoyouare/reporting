from datetime import datetime
from django.utils.timezone import utc
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count
from django.db.models import Q
from django.core.urlresolvers import reverse
from . import managers

class Tag(models.Model):
    name = models.CharField(max_length=60, unique=True)

    def __str__(self):
        return self.name


class TestStatus(models.Model):
    name = models.CharField(max_length=60)
    description = models.CharField(max_length=120, blank=True, null=True)

    class Meta:
        verbose_name_plural = "test statuses"

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField(max_length=120)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('reports:project_overview', kwargs={'project_name': self.name})

    def get_sections(self):
        return Section.objects.filter(project=self.id)

    def get_all_tests(self):
        sections = self.get_sections()
        tests = Test.objects.filter(section__in=sections)
        return tests

    def get_combined_tests(self):
        sections = self.get_sections()
        tests = Test.objects.filter(section__in=sections).filter(main_run__flags__name__contains='Combined')
        return tests

    def get_project_tested_metrics(self):
        raw_list = list(x.main_run.get_tested_metrics() for x in self.get_combined_tests())
        return [item for sublist in raw_list for item in sublist]

    def get_project_tested_high_priority_metrics(self):
        raw_list = list(x.main_run.get_tested_high_priority_metrics() for x in self.get_combined_tests())
        return [item for sublist in raw_list for item in sublist]

    def get_top_tags(self):
        tests = self.get_all_tests()
        tags = Tag.objects.filter(test__in=tests).distinct()
        return tags

    def get_tags_cloud(self):
        tests = self.get_all_tests()
        # TODO: now it's simple count,
        # but need to implement smarted size calculation
        tags = Tag.objects.filter(test__in=tests).annotate(number_of_entries=Count('name'))
        return tags


class Section(models.Model):
    name = models.CharField(max_length=120)
    project = models.ForeignKey(Project)
    components = models.ManyToManyField('apps.Component')

    def __str__(self):
        return "%s at %s" % (self.name, self.project)

    def get_absolute_url(self):
        return reverse('reports:section_overview', kwargs={'project_name': self.project.name,
                                                           'section_name': self.name})

    def get_section_tests(self):
        return Test.objects.filter(section=self.id)

    def get_tests_count(self):
        return self.get_section_tests().count()

    def get_scheduled_tests(self):
        scheduled_statuses = ('In Progress', 'New')
        return self.get_section_tests().filter(status__name__in=scheduled_statuses).exclude(main_run__id__isnull=True)

    def get_last_test(self):
        completed_statuses = ('Completed', 'Passed', 'Failed', 'Aborted')
        return Test.objects.filter(section=self.id,
                                   status__name__in=completed_statuses).exclude(main_run__id__isnull=True).latest(field_name='main_run__timestamp_end')

    def get_this_year_tests(self):
        this_year = datetime.now().year
        return Test.objects.filter(section=self.id,
                                   main_run__timestamp_end__year=this_year)

    def get_this_year_tests_count(self):
        return self.get_this_year_tests().count()

    def get_last_year_tests_count(self):
        last_year = datetime.now().year - 1
        return Test.objects.filter(section=self.id,
                                   main_run__timestamp_end__year=last_year).count()


class ReportTemplate(models.Model):
    name = models.CharField(max_length=120)
    template_name = models.CharField(max_length=120)

    def __str__(self):
        return self.name


def test_attach_filename(kind_dir, test_entity, short_fname):
    dirname = test_entity.jira_key if (test_entity and test_entity.jira_key) else '_unknown_test'
    return '/'.join([kind_dir, dirname, short_fname])

def test_plan_filename(test_entity, short_fname):
    return test_attach_filename('test_plans', test_entity, short_fname)

class Test(models.Model):
    jira_key = models.CharField(max_length=60, unique=True)
    title = models.CharField(max_length=120)
    goal = models.CharField(max_length=120)
    conclusion = models.TextField(blank=True)
    test_plan = models.FileField(upload_to=test_plan_filename, null=True, blank=True)
    test_plan_external_link = models.CharField(max_length=300, null=True, blank=True)
    external_report_link = models.URLField(blank=True)

    requester = models.ForeignKey(User)
    template = models.ForeignKey(ReportTemplate, blank=True, null=True)
    status = models.ForeignKey(TestStatus)
    section = models.ForeignKey(Section)
    related = models.ForeignKey('self', blank=True, null=True)
    main_run = models.ForeignKey('measurements.Run',
                                 related_name='main_run',
                                 blank=True, null=True)
    runs = models.ManyToManyField('measurements.Run', blank=True, null=True)
    tags = models.ManyToManyField(Tag, blank=True)

    objects = models.Manager()
    extra_manager = managers.CreateOrUpdateManager()

    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)


    def __str__(self):
        return '%s: %s' % (self.jira_key, self.title)

    def get_absolute_url_pattern(self, kwargs):
        kwargs['section_name']=self.section.name
        kwargs['project_name']=self.section.project.name
        return 'reports:test_report'

    def get_absolute_url(self):
        if self.external_report_link:
            return self.external_report_link
        else:
            kwargs={'jira_key': self.jira_key}
            return reverse(self.get_absolute_url_pattern(kwargs), kwargs=kwargs)

    def get_test_date(self):
        try:
            return self.start_date, self.end_date
        except AttributeError:
            try:
                return self.main_run.timestamp_start, self.main_run.timestamp_end
            except AttributeError:
                return datetime(1970, 1, 1, 00, 00, tzinfo=utc), datetime(1970, 1, 1, 00, 1, tzinfo=utc)

    def get_test_start_date(self):
        try:
            year = self.start_date.year
            return self.start_date
        except AttributeError:
            try:
                return self.main_run.timestamp_start
            except AttributeError:
                return datetime(1970, 1, 1, 00, 00, tzinfo=utc)


    def get_related_tests(self):
        return self.__class__.objects.filter(related=self.id,
                                   section=self.section)

    def get_participants(self):
        return TestParticipant.objects.filter(test=self.id)

    def get_test_log(self):
        return TestLogEntry.objects.filter(test=self.id)

    def get_attachments(self):
        return Attachment.objects.filter(test=self.id)

    def get_test_plan_url(self):
        url = self.test_plan.url if self.test_plan else self.test_plan_external_link
        return url

    def is_start_date_before(self, date):
        if not date:
            return True
        if not self.start_date:
            return False
        return self.start_date < date

    @classmethod # to allow inheritance in TestProxy
    def select_tests(cls, project_name, params={'text': ''}):
        """
        :param project_name: string
        :param params: dict, can be directly passed from url parameters in http-request - protected from SQL injection
        by Django ORM level, https://docs.djangoproject.com/en/1.6/topics/security/
        :return: list of Test entities, including parent Tests for related tests selected by params
        """
        tests = list(cls.get_select_queryset(project_name, params))
        def parent_in_list(parent):
            for i in range(0, len(tests)):
                if tests[i].is_start_date_before(parent.start_date):
                    tests.insert(i, parent)
                if parent.id == tests[i].id:
                    return True

        for test in tests[:]: # use full slice copy to insert parents if needs
            parent = test.related
            if not parent:
                continue
            if not parent_in_list(parent):
                tests.append(parent)
        return tests

    @classmethod # to allow inheritance in TestProxy
    def get_select_queryset(cls, project_name, params):
        field_selectors = {'requester' : 'requester__username',
                           'test' : 'jira_key',
                           'tag' : 'tags__name' }
        criteria = {'section__project__name__exact' : project_name}
        if 'text' in params :
            prj_criteria = Q(**criteria)
            text = params['text']
            or_criteria = None
            for field in ['title', 'tags__name', 'requester__last_name', 'requester__username']:
                criteria = { field +'__icontains' : text }
                or_criteria = (or_criteria | Q(**criteria)) if or_criteria else Q(**criteria)
            query = cls.objects.filter(prj_criteria, or_criteria).distinct() # LEFT JOIN with tags gives duplicates
        else:
            for k in field_selectors.keys():
                if k in params :
                    criteria[ field_selectors[k] ] = params[k] # '__exact' is implied if last '__' suffix is field-name (doesn't match any modifier)
            query = cls.objects.filter(**criteria)

        return query.order_by('-start_date')


class TestLogEntry(models.Model):
    timestamp = models.DateTimeField()
    log = models.CharField(max_length=360)
    test = models.ForeignKey(Test)


class TestParticipant(models.Model):
    role = models.CharField(max_length=120)
    users = models.ManyToManyField(User)
    test = models.ForeignKey(Test)


def attachment_filename(attachment_entity, short_fname):
    return test_attach_filename('reports_attachments', attachment_entity.test, short_fname)

class Attachment(models.Model):
    alias = models.CharField(max_length=120)
    attached_file = models.FileField(upload_to=attachment_filename)
    test = models.ForeignKey(Test)

    def get_url_name(self):
        return self.alias.replace(' ', '-').replace('(', '_').replace(')', '_')

    def get_direct_url(self):
        attach_urlname = self.get_url_name()
        params = {'jira_key'    :self.test.jira_key,
                  'attach_name' :attach_urlname}
        return reverse('reports:test_attach_redirect', kwargs=params)

    def get_file_extension(self):
        splitted_name = self.attached_file.name.split('.')
        if not len(splitted_name) < 2:
            return splitted_name[-1]
