from django.contrib.auth.models import User

from django.db import transaction
from django.core.urlresolvers import reverse

from . import models

from measurements.tests import BaseTest as MeasBaseTest


class BaseTest(MeasBaseTest):

    def setUp(self):
        super(BaseTest, self).setUp()
        with transaction.atomic():
            self.project = models.Project.objects.create(name='Sample Project')
            self.section = models.Section.objects.create(name='Sample Section',
                                                         project=self.project)
            self.section.components = [self.test_component]
            self.status_in_progress = models.TestStatus.objects.create(name='In Progress',
                                                                       description='Sample status')
            self.sample_user = User.objects.create(username='tester',
                                                   first_name='sample',
                                                   last_name='user',
                                                   email='user@example.com')
            self.test_report = models.Test.objects.create(jira_key='PERF-123',
                                                          title='Sample Performance test',
                                                          goal='Create sample test',
                                                          conclusion='In progress',
                                                          section=self.section,
                                                          status=self.status_in_progress,
                                                          requester=self.sample_user,
                                                          main_run=self.test_run)

    def test_simple_project_overview(self):
        project_url = reverse('reports:project_overview', kwargs={'project_name': self.project.name})
        response = self.client.get(project_url)

        self.assertEqual(response.status_code, 200)

    def test_simple_section_overview(self):
        section_url = reverse('reports:section_overview', kwargs={'project_name': self.project.name,
                                                                  'section_name': self.section.name})

        response = self.client.get(section_url)
        self.assertEqual(response.status_code, 200)
