from django import forms

from . import models


class SectionSelectionForm(forms.Form):
    section = forms.ModelChoiceField(models.Section.objects.all())



